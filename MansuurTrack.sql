-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 06, 2023 at 06:45 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `MansuurTrack`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` bigint(20) NOT NULL,
  `name` varchar(500) NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `modified_at` datetime(6) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `created_at`, `modified_at`, `user_id`) VALUES
(1, 'Salaama Bank', '2023-07-28 09:35:07.109482', '2023-07-28 09:35:07.109516', 2),
(2, '252-617-608-849', '2023-07-28 09:35:28.867044', '2023-07-28 09:35:28.867117', 2),
(3, '252-613-992-210', '2023-07-28 09:35:45.570361', '2023-07-28 09:35:45.570397', 2),
(4, '252-612-224-355', '2023-07-28 09:36:22.822721', '2023-07-28 09:36:22.822771', 2),
(5, 'Premier Wallet', '2023-07-28 22:15:09.814749', '2023-07-28 22:15:09.814785', 2);

-- --------------------------------------------------------

--
-- Table structure for table `audittrials`
--

CREATE TABLE `audittrials` (
  `id` bigint(20) NOT NULL,
  `path` varchar(60) DEFAULT NULL,
  `Actions` varchar(400) NOT NULL,
  `Module` varchar(400) NOT NULL,
  `date_of_action` datetime(6) NOT NULL,
  `operating_system` varchar(200) NOT NULL,
  `browser` varchar(200) NOT NULL,
  `ip_address` varchar(200) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `audittrials`
--

INSERT INTO `audittrials` (`id`, `path`, `Actions`, `Module`, `date_of_action`, `operating_system`, `browser`, `ip_address`, `user_id`) VALUES
(1, '/accounts/login_account', 'Logged in...', 'Users', '2023-07-28 09:31:01.636110', 'Macintosh', 'Chrome', '127.0.0.1', 1),
(2, '/manage_essentials/CreateAccount', 'wealth@support.com has created new account with this name Salaama Bank', 'Accounts', '2023-07-28 09:32:39.879994', 'Macintosh', 'Chrome', '127.0.0.1', 1),
(3, '/accounts/create_account', 'Creating new account/user with this email mansuurtech101@gmail.com', 'Users', '2023-07-28 09:34:08.207883', 'Macintosh', 'ChromiumEdge', '127.0.0.1', NULL),
(4, '/accounts/login_account', 'Logged in...', 'Users', '2023-07-28 09:34:17.723700', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(5, '/manage_essentials/CreateAccount', 'mansuurtech101@gmail.com has created new account with this name Salaama Bank', 'Accounts', '2023-07-28 09:35:07.111339', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(6, '/manage_essentials/CreateAccount', 'mansuurtech101@gmail.com has created new account with this name 252-617-608-849', 'Accounts', '2023-07-28 09:35:28.870292', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(7, '/manage_essentials/CreateAccount', 'mansuurtech101@gmail.com has created new account with this name 252-613-992-210', 'Accounts', '2023-07-28 09:35:45.571696', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(8, '/manage_essentials/CreateAccount', 'mansuurtech101@gmail.com has created new account with this name 252-612-224-355', 'Accounts', '2023-07-28 09:36:22.824664', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(9, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Salary', 'Categories', '2023-07-28 09:36:51.814119', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(10, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 1', 'New Transactions', '2023-07-28 10:39:39.729125', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(11, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 1', 'New Transactions', '2023-07-28 10:42:13.416037', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(12, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 1', 'New Transactions', '2023-07-28 18:30:11.987501', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(13, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Last month', 'Categories', '2023-07-28 18:31:31.059618', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(14, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Others', 'Categories', '2023-07-28 18:31:39.386733', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(15, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 2', 'New Transactions', '2023-07-28 18:32:23.257846', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(16, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 3', 'New Transactions', '2023-07-28 18:32:41.292693', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(17, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Authorised resellers (MBP 16)', 'Categories', '2023-07-28 18:35:05.067303', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(18, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Family Help', 'Categories', '2023-07-28 18:36:32.675936', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(19, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Father Net', 'Categories', '2023-07-28 18:36:41.237146', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(20, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Transportation', 'Categories', '2023-07-28 18:37:20.462277', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(21, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Medical && Health Care', 'Categories', '2023-07-28 18:37:39.485498', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(22, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Utilities', 'Categories', '2023-07-28 18:37:48.711219', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(23, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Food', 'Categories', '2023-07-28 18:38:02.820565', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(24, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Entertainment && Gaming', 'Categories', '2023-07-28 18:38:18.533321', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(25, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Untracked', 'Categories', '2023-07-28 18:38:29.119344', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(26, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Transfer To Next Month', 'Categories', '2023-07-28 18:38:50.807790', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(27, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Cahrity', 'Categories', '2023-07-28 18:39:00.456453', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(28, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name GYM', 'Categories', '2023-07-28 18:39:10.828720', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(29, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 4', 'New Transactions', '2023-07-28 19:02:13.793582', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(30, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 5', 'New Transactions', '2023-07-28 19:03:29.529935', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(31, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 6', 'New Transactions', '2023-07-28 19:03:44.306172', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(32, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Personal Spending', 'Categories', '2023-07-28 19:04:29.092708', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(33, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 7', 'New Transactions', '2023-07-28 19:05:02.170760', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(34, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 8', 'New Transactions', '2023-07-28 19:05:19.583466', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(35, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 9', 'New Transactions', '2023-07-28 19:05:48.385640', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(36, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 10', 'New Transactions', '2023-07-28 19:06:14.221261', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(37, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 11', 'New Transactions', '2023-07-28 19:06:28.333491', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(38, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 12', 'New Transactions', '2023-07-28 19:06:48.967278', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(39, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 13', 'New Transactions', '2023-07-28 19:07:18.158005', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(40, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 14', 'New Transactions', '2023-07-28 19:10:41.257175', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(41, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com has update an exisiting transaction with id 14', 'Edit Transactions', '2023-07-28 19:11:46.170471', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(42, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 15', 'New Transactions', '2023-07-28 19:13:29.431458', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(43, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 16', 'New Transactions', '2023-07-28 19:13:45.044270', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(44, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 17', 'New Transactions', '2023-07-28 19:14:45.016395', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(45, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 18', 'New Transactions', '2023-07-28 19:15:10.603204', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(46, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 19', 'New Transactions', '2023-07-28 19:15:28.906605', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(47, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 20', 'New Transactions', '2023-07-28 19:16:07.747584', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(48, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 21', 'New Transactions', '2023-07-28 19:16:22.263495', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(49, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 22', 'New Transactions', '2023-07-28 19:16:43.087986', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(50, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 23', 'New Transactions', '2023-07-28 19:17:07.843862', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(51, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 24', 'New Transactions', '2023-07-28 19:18:52.383902', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(52, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 25', 'New Transactions', '2023-07-28 19:19:35.703861', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(53, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 26', 'New Transactions', '2023-07-28 19:19:54.767485', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(54, '/manage_essentials/EditCategory', 'mansuurtech101@gmail.com has updated a category with this name Charity', 'Categories', '2023-07-28 19:20:08.883181', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(55, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 27', 'New Transactions', '2023-07-28 19:22:31.406548', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(56, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 28', 'New Transactions', '2023-07-28 19:22:50.408749', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(57, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 29', 'New Transactions', '2023-07-28 19:23:10.181019', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(58, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 30', 'New Transactions', '2023-07-28 19:23:21.286394', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(59, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 31', 'New Transactions', '2023-07-28 19:23:38.667877', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(60, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 32', 'New Transactions', '2023-07-28 19:24:06.674339', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(61, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 33', 'New Transactions', '2023-07-28 19:25:13.603126', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(62, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 34', 'New Transactions', '2023-07-28 19:25:26.577341', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(63, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 35', 'New Transactions', '2023-07-28 19:25:46.722935', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(64, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 36', 'New Transactions', '2023-07-28 19:26:02.679848', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(65, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 37', 'New Transactions', '2023-07-28 19:26:25.527612', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(66, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 38', 'New Transactions', '2023-07-28 19:30:22.447780', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(67, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 39', 'New Transactions', '2023-07-28 19:30:40.015195', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(68, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 40', 'New Transactions', '2023-07-28 19:30:59.865694', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(69, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 41', 'New Transactions', '2023-07-28 19:31:14.108886', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(70, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 42', 'New Transactions', '2023-07-28 19:31:31.381005', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(71, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 43', 'New Transactions', '2023-07-28 19:31:47.448469', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(72, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 44', 'New Transactions', '2023-07-28 19:32:04.441800', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(73, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 45', 'New Transactions', '2023-07-28 19:32:22.333462', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(74, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 46', 'New Transactions', '2023-07-28 19:32:50.258813', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(75, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 47', 'New Transactions', '2023-07-28 19:33:06.675039', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(76, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 48', 'New Transactions', '2023-07-28 19:33:47.613877', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(77, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 49', 'New Transactions', '2023-07-28 19:34:22.841915', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(78, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 50', 'New Transactions', '2023-07-28 19:34:38.495621', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(79, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 51', 'New Transactions', '2023-07-28 19:35:04.627917', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(80, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 52', 'New Transactions', '2023-07-28 19:35:18.445664', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(81, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 53', 'New Transactions', '2023-07-28 19:35:35.878314', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(82, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 54', 'New Transactions', '2023-07-28 19:35:48.145942', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(83, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com has update an exisiting transaction with id 51', 'Edit Transactions', '2023-07-28 19:36:50.114814', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(84, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com has update an exisiting transaction with id 53', 'Edit Transactions', '2023-07-28 19:37:32.988279', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(85, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com has update an exisiting transaction with id 52', 'Edit Transactions', '2023-07-28 19:38:13.867055', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(86, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 55', 'New Transactions', '2023-07-28 19:38:33.122926', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(87, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com has update an exisiting transaction with id 54', 'Edit Transactions', '2023-07-28 19:39:16.093665', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(88, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 56', 'New Transactions', '2023-07-28 19:39:51.872437', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(89, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 57', 'New Transactions', '2023-07-28 19:41:08.646913', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(90, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 58', 'New Transactions', '2023-07-28 19:43:12.642332', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(91, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 59', 'New Transactions', '2023-07-28 19:44:19.451723', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(92, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 60', 'New Transactions', '2023-07-28 19:46:51.660357', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(93, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com has update an exisiting transaction with id 59', 'Edit Transactions', '2023-07-28 19:47:13.641855', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(94, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 61', 'New Transactions', '2023-07-28 19:47:53.114215', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(95, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 62', 'New Transactions', '2023-07-28 19:48:11.308935', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(96, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 63', 'New Transactions', '2023-07-28 19:48:32.799928', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(97, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 64', 'New Transactions', '2023-07-28 19:48:48.687918', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(98, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 65', 'New Transactions', '2023-07-28 19:49:06.683026', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(99, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 66', 'New Transactions', '2023-07-28 19:53:24.020403', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(100, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 67', 'New Transactions', '2023-07-28 19:53:41.329843', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(101, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 68', 'New Transactions', '2023-07-28 19:54:57.992105', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(102, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 69', 'New Transactions', '2023-07-28 19:55:09.934231', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(103, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com has update an exisiting transaction with id 69', 'Edit Transactions', '2023-07-28 19:57:18.177624', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(104, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 70', 'New Transactions', '2023-07-28 19:57:56.159357', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(105, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 71', 'New Transactions', '2023-07-28 19:58:14.411482', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(106, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 72', 'New Transactions', '2023-07-28 20:00:58.606432', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(107, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com has update an exisiting transaction with id 72', 'Edit Transactions', '2023-07-28 20:05:27.371831', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(108, '/manage_essentials/CreateAccount', 'mansuurtech101@gmail.com has created new account with this name Premier Wallet', 'Accounts', '2023-07-28 22:15:09.817399', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(109, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-612-224-355 to Salaama Bank', 'Transfer Amount - Account to account', '2023-07-29 18:51:20.305661', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(110, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 75', 'New Transactions', '2023-07-29 18:52:13.274419', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(111, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-617-608-849 to Salaama Bank', 'Transfer Amount - Account to account', '2023-07-29 18:52:56.964970', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(112, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 78', 'New Transactions', '2023-07-29 18:53:16.310261', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(113, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-612-224-355 to Salaama Bank', 'Transfer Amount - Account to account', '2023-07-29 19:01:09.964255', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(114, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 81', 'New Transactions', '2023-07-29 19:01:36.606924', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(115, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-07-29 19:02:18.714268', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(116, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 84', 'New Transactions', '2023-07-29 19:18:49.626586', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(117, '/manage_essentials/CreateDeptor', 'mansuurtech101@gmail.com has created new deptor with this name Abdiqani HNSQ ( Friend )', 'Deptors', '2023-07-31 10:36:58.624826', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(118, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 85', 'New Transactions', '2023-07-31 10:37:22.884519', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(119, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 86', 'New Transactions', '2023-07-31 10:38:08.719232', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(120, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 87', 'New Transactions', '2023-07-31 10:38:26.306780', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(121, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-07-31 10:39:04.073017', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(122, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 90', 'New Transactions', '2023-07-31 10:39:29.313027', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(123, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 91', 'New Transactions', '2023-07-31 10:48:35.682973', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(124, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-612-224-355 to 252-617-608-849', 'Transfer Amount - Account to account', '2023-07-31 10:48:48.801862', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(125, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-07-31 19:45:07.455970', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(126, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 96', 'New Transactions', '2023-07-31 19:45:31.767844', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(127, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 97', 'New Transactions', '2023-07-31 19:46:43.916695', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(128, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-07-31 19:50:47.066586', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(129, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 100', 'New Transactions', '2023-07-31 19:51:14.339281', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(130, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-07-31 19:52:24.593898', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(131, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-07-31 19:52:43.382937', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(132, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 105', 'New Transactions', '2023-07-31 19:53:31.868564', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(133, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 106', 'New Transactions', '2023-07-31 19:53:56.001909', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(134, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 107', 'New Transactions', '2023-07-31 19:57:03.548114', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(135, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com has update an exisiting transaction with id 107', 'Edit Transactions', '2023-07-31 19:57:35.503532', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(136, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 108', 'New Transactions', '2023-07-31 19:57:59.220445', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(137, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 109', 'New Transactions', '2023-07-31 19:58:34.231968', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(138, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 110', 'New Transactions', '2023-08-02 16:00:05.154992', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(139, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 111', 'New Transactions', '2023-08-02 16:02:06.292768', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(140, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 112', 'New Transactions', '2023-08-02 16:05:37.369894', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(141, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com has update an exisiting transaction with id 111', 'Edit Transactions', '2023-08-02 16:05:54.569919', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(142, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-02 16:07:39.418686', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(143, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-02 16:07:51.864133', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(144, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 117', 'New Transactions', '2023-08-02 16:08:34.065990', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(145, '/manage_essentials/CreateDeptor', 'mansuurtech101@gmail.com has created new deptor with this name Abdisalaam ( Friend )', 'Deptors', '2023-08-02 16:09:06.538274', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(146, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 118', 'New Transactions', '2023-08-02 16:09:27.810312', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(147, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 119', 'New Transactions', '2023-08-02 16:10:34.498576', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(148, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from Premier Wallet to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-02 16:23:28.336289', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(149, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 122', 'New Transactions', '2023-08-02 17:21:55.423292', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(150, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 123', 'New Transactions', '2023-08-02 17:22:10.423424', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(151, '/accounts/login_account', 'Logged in...', 'Users', '2023-08-04 14:48:46.035058', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(152, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-04 14:49:28.421662', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(153, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-04 14:51:09.813741', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(154, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 128', 'New Transactions', '2023-08-04 14:51:47.424293', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(155, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 129', 'New Transactions', '2023-08-04 14:52:11.952188', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(156, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 130', 'New Transactions', '2023-08-04 14:52:23.345930', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(157, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 131', 'New Transactions', '2023-08-04 14:52:36.575036', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(158, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 132', 'New Transactions', '2023-08-04 14:54:20.439545', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(159, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 133', 'New Transactions', '2023-08-04 14:54:47.669932', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(160, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 134', 'New Transactions', '2023-08-04 14:57:33.349127', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(161, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 135', 'New Transactions', '2023-08-04 18:22:34.329138', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(162, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 136', 'New Transactions', '2023-08-04 18:22:48.522156', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(163, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transfer an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-05 16:43:09.316773', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(164, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 139', 'New Transactions', '2023-08-05 16:43:24.782877', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(165, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 140', 'New Transactions', '2023-08-05 16:43:46.810447', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(166, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 141', 'New Transactions', '2023-08-06 04:05:05.621685', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(167, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com has update an exisiting transaction with id 141', 'Edit Transactions', '2023-08-10 04:00:52.473041', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(168, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com has update an exisiting transaction with id 141', 'Edit Transactions', '2023-08-10 04:01:33.841578', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(169, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-10 04:02:45.167977', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(170, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 144', 'New Transactions', '2023-08-10 04:03:09.912021', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(171, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 145', 'New Transactions', '2023-08-10 04:03:34.609955', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(172, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com has update an exisiting transaction with id 145', 'Edit Transactions', '2023-08-10 04:03:59.428189', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(173, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 146', 'New Transactions', '2023-08-10 04:04:56.182070', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(174, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 147', 'New Transactions', '2023-08-10 04:05:23.383636', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(175, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 148', 'New Transactions', '2023-08-10 04:05:47.346110', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(176, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 149', 'New Transactions', '2023-08-11 16:24:39.261763', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(177, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-11 16:27:53.357077', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(178, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-612-224-355 to 252-613-992-210', 'Transfer Amount - Account to account', '2023-08-11 17:45:53.128106', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(179, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 154', 'New Transactions', '2023-08-11 17:49:53.228442', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(180, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 155', 'New Transactions', '2023-08-11 17:50:18.584927', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(181, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 156', 'New Transactions', '2023-08-11 17:50:40.379285', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(182, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 157', 'New Transactions', '2023-08-11 17:51:22.645049', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(183, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 158', 'New Transactions', '2023-08-11 17:56:04.238491', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(184, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 159', 'New Transactions', '2023-08-11 17:57:35.894208', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(185, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 160', 'New Transactions', '2023-08-11 17:59:55.708909', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(186, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-14 18:21:30.008672', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(187, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Dressing and Clothing', 'Categories', '2023-08-14 18:24:27.339633', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(188, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 163', 'New Transactions', '2023-08-14 18:25:09.767651', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(189, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 164', 'New Transactions', '2023-08-14 18:25:30.503586', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(190, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 165', 'New Transactions', '2023-08-14 18:27:26.609316', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(191, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 166', 'New Transactions', '2023-08-14 18:27:40.657210', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(192, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 167', 'New Transactions', '2023-08-14 18:27:59.114111', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(193, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-14 18:28:16.761225', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(194, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 170', 'New Transactions', '2023-08-14 18:28:34.904174', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(195, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-617-608-849 to 252-613-992-210', 'Transfer Amount - Account to account', '2023-08-14 18:28:50.899586', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(196, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 173', 'New Transactions', '2023-08-14 18:29:07.789429', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(197, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 174', 'New Transactions', '2023-08-14 18:29:28.570128', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(198, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 175', 'New Transactions', '2023-08-14 18:30:10.850558', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(199, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 176', 'New Transactions', '2023-08-14 18:30:40.246854', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(200, '/accounts/manage_account/UpdateProfile', 'Mansuur Abdullahi Abdirahman has updated their profile avatar', 'Users', '2023-08-14 18:42:51.807437', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(201, '/accounts/manage_account/UpdateInformation', 'Mansuur Abdullahi Abdirahman has updated their personal information', 'Users', '2023-08-14 18:43:18.913781', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(202, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 177', 'New Transactions', '2023-08-15 17:48:41.717998', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(203, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-15 17:49:03.108593', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(204, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 180', 'New Transactions', '2023-08-15 17:49:40.748220', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(205, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 181', 'New Transactions', '2023-08-15 17:49:57.509285', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(206, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 182', 'New Transactions', '2023-08-15 17:50:15.770358', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(207, '/accounts/login_account', 'Logged in...', 'Users', '2023-08-19 03:05:19.970862', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(208, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 183', 'New Transactions', '2023-08-19 03:06:49.646091', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(209, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 184', 'New Transactions', '2023-08-19 03:07:41.707347', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(210, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com updated a transaction with id 184', 'Edit Transactions', '2023-08-19 03:08:06.045371', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(211, '/manage_essentials/EditTransaction', 'mansuurtech101@gmail.com updated a transaction with id 184', 'Edit Transactions', '2023-08-19 03:08:34.908335', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(212, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 185', 'New Transactions', '2023-08-19 03:09:15.486349', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(213, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-612-224-355 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-19 03:09:51.048889', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(214, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 188', 'New Transactions', '2023-08-19 03:13:04.408433', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(215, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 189', 'New Transactions', '2023-08-19 03:13:33.392078', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(216, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 190', 'New Transactions', '2023-08-19 03:14:22.848803', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(217, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-19 03:14:48.047547', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(218, '/manage_essentials/CreateDeptor', 'mansuurtech101@gmail.com has created new deptor with this name Mohammed Ahmed Nur ( Friend )', 'Deptors', '2023-08-19 03:15:08.971631', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(219, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 193', 'New Transactions', '2023-08-19 03:15:39.091774', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(220, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 194', 'New Transactions', '2023-08-19 03:16:06.469182', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(221, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 195', 'New Transactions', '2023-08-19 03:16:37.258282', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(222, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 196', 'New Transactions', '2023-08-19 03:17:09.719886', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(223, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 197', 'New Transactions', '2023-08-20 16:27:37.630973', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(224, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 198', 'New Transactions', '2023-08-20 16:28:07.655594', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(225, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 199', 'New Transactions', '2023-08-20 16:28:35.951607', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(226, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-20 18:01:00.031277', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(227, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 202', 'New Transactions', '2023-08-20 18:01:17.050489', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(228, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Premier Wallet', 'Transfer Amount - Account to account', '2023-08-21 15:52:59.657394', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(229, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Khidmadaha Appka (eDir)', 'Categories', '2023-08-21 15:54:01.838610', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(230, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 205', 'New Transactions', '2023-08-21 15:56:06.905825', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(231, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 206', 'New Transactions', '2023-08-22 04:07:42.700936', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(232, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 207', 'New Transactions', '2023-08-22 13:51:47.994574', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(233, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 208', 'New Transactions', '2023-08-22 16:26:45.680055', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(234, '/accounts/login_account', 'Logged in...', 'Users', '2023-08-22 19:52:42.869332', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(235, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 209', 'New Transactions', '2023-08-24 17:31:44.223512', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(236, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 210', 'New Transactions', '2023-08-24 17:32:25.820662', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(237, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 211', 'New Transactions', '2023-08-24 17:33:21.931855', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(238, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 212', 'New Transactions', '2023-08-25 19:57:29.310328', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(239, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 213', 'New Transactions', '2023-08-28 19:53:24.113817', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(240, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-28 19:57:46.346508', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(241, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 216', 'New Transactions', '2023-08-28 19:58:09.940131', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2);
INSERT INTO `audittrials` (`id`, `path`, `Actions`, `Module`, `date_of_action`, `operating_system`, `browser`, `ip_address`, `user_id`) VALUES
(242, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-28 19:58:59.687695', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(243, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 219', 'New Transactions', '2023-08-28 19:59:23.294914', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(244, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 220', 'New Transactions', '2023-08-28 19:59:57.671902', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(245, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-28 20:00:14.622174', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(246, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 223', 'New Transactions', '2023-08-28 20:00:39.868031', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(247, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-28 20:01:03.333327', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(248, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 226', 'New Transactions', '2023-08-28 20:01:19.134294', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(249, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 227', 'New Transactions', '2023-08-28 20:01:41.792312', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(250, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 228', 'New Transactions', '2023-08-28 20:02:08.331100', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(251, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 229', 'New Transactions', '2023-08-28 20:02:27.249079', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(252, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-28 20:02:39.431786', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(253, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 232', 'New Transactions', '2023-08-28 20:02:59.862804', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(254, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name For dept purpose', 'Categories', '2023-08-28 20:07:36.287735', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(255, '/accounts/login_account', 'Logged in...', 'Users', '2023-08-28 20:10:09.986948', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(256, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-613-992-210 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-29 14:22:43.397278', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(257, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 235', 'New Transactions', '2023-08-29 14:22:59.317200', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(258, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 236', 'New Transactions', '2023-08-30 18:19:41.323151', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(259, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 237', 'New Transactions', '2023-08-30 18:20:02.607626', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(260, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 238', 'New Transactions', '2023-08-30 18:20:27.147560', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(261, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 239', 'New Transactions', '2023-08-30 18:20:43.240845', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(262, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Barber shop', 'Categories', '2023-08-30 18:21:24.110062', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(263, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 240', 'New Transactions', '2023-08-30 18:22:02.830812', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(264, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 241', 'New Transactions', '2023-08-30 18:22:18.074442', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(265, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 242', 'New Transactions', '2023-08-30 18:55:47.494915', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(266, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name For dept purpose', 'Categories', '2023-08-30 18:56:19.328216', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(267, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 243', 'New Transactions', '2023-08-30 18:56:35.445053', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(268, '/manage_essentials/TransferAmount', 'mansuurtech101@gmail.com has transferred an amount from 252-612-224-355 to Salaama Bank', 'Transfer Amount - Account to account', '2023-08-30 19:05:23.725334', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(269, '/manage_essentials/CreateCategory', 'mansuurtech101@gmail.com has created new category with this name Samsung TV ( Haleel Samsung )', 'Categories', '2023-08-30 19:06:24.327361', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(270, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 246', 'New Transactions', '2023-08-30 19:08:10.883148', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(271, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 247', 'New Transactions', '2023-08-31 19:50:12.921948', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(272, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 248', 'New Transactions', '2023-08-31 19:51:32.785216', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(273, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 249', 'New Transactions', '2023-08-31 19:51:49.565588', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(274, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 250', 'New Transactions', '2023-08-31 19:52:08.829906', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(275, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 251', 'New Transactions', '2023-08-31 19:52:25.628394', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(276, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 252', 'New Transactions', '2023-08-31 19:52:46.000349', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(277, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 253', 'New Transactions', '2023-08-31 19:53:16.234442', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(278, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 254', 'New Transactions', '2023-08-31 19:53:38.738417', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(279, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 255', 'New Transactions', '2023-08-31 19:54:03.539850', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(280, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 256', 'New Transactions', '2023-08-31 19:54:22.280846', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(281, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 257', 'New Transactions', '2023-08-31 19:54:37.861571', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(282, '/accounts/login_account', 'Logged in...', 'Users', '2023-09-01 14:59:47.722813', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(283, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 258', 'New Transactions', '2023-09-01 15:00:05.529548', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(284, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 259', 'New Transactions', '2023-09-01 15:00:24.329493', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2),
(285, '/manage_essentials/NewTransaction', 'mansuurtech101@gmail.com has add new transaction with id 260', 'New Transactions', '2023-09-03 07:08:03.433931', 'Macintosh', 'ChromiumEdge', '127.0.0.1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `authtoken_token`
--

CREATE TABLE `authtoken_token` (
  `key` varchar(40) NOT NULL,
  `created` datetime(6) NOT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` bigint(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add content type', 4, 'add_contenttype'),
(14, 'Can change content type', 4, 'change_contenttype'),
(15, 'Can delete content type', 4, 'delete_contenttype'),
(16, 'Can view content type', 4, 'view_contenttype'),
(17, 'Can add session', 5, 'add_session'),
(18, 'Can change session', 5, 'change_session'),
(19, 'Can delete session', 5, 'delete_session'),
(20, 'Can view session', 5, 'view_session'),
(21, 'Can add audit trials', 6, 'add_audittrials'),
(22, 'Can change audit trials', 6, 'change_audittrials'),
(23, 'Can delete audit trials', 6, 'delete_audittrials'),
(24, 'Can view audit trials', 6, 'view_audittrials'),
(25, 'Can add error logs', 7, 'add_errorlogs'),
(26, 'Can change error logs', 7, 'change_errorlogs'),
(27, 'Can delete error logs', 7, 'delete_errorlogs'),
(28, 'Can view error logs', 7, 'view_errorlogs'),
(29, 'Can add users', 8, 'add_users'),
(30, 'Can change users', 8, 'change_users'),
(31, 'Can delete users', 8, 'delete_users'),
(32, 'Can view users', 8, 'view_users'),
(33, 'Can add payments', 9, 'add_payments'),
(34, 'Can change payments', 9, 'change_payments'),
(35, 'Can delete payments', 9, 'delete_payments'),
(36, 'Can view payments', 9, 'view_payments'),
(37, 'Can add category', 10, 'add_category'),
(38, 'Can change category', 10, 'change_category'),
(39, 'Can delete category', 10, 'delete_category'),
(40, 'Can view category', 10, 'view_category'),
(41, 'Can add accounts', 11, 'add_accounts'),
(42, 'Can change accounts', 11, 'change_accounts'),
(43, 'Can delete accounts', 11, 'delete_accounts'),
(44, 'Can view accounts', 11, 'view_accounts'),
(45, 'Can add deptors', 12, 'add_deptors'),
(46, 'Can change deptors', 12, 'change_deptors'),
(47, 'Can delete deptors', 12, 'delete_deptors'),
(48, 'Can view deptors', 12, 'view_deptors'),
(49, 'Can add transactions', 13, 'add_transactions'),
(50, 'Can change transactions', 13, 'change_transactions'),
(51, 'Can delete transactions', 13, 'delete_transactions'),
(52, 'Can view transactions', 13, 'view_transactions'),
(53, 'Can add Token', 14, 'add_token'),
(54, 'Can change Token', 14, 'change_token'),
(55, 'Can delete Token', 14, 'delete_token'),
(56, 'Can view Token', 14, 'view_token'),
(57, 'Can add token', 15, 'add_tokenproxy'),
(58, 'Can change token', 15, 'change_tokenproxy'),
(59, 'Can delete token', 15, 'delete_tokenproxy'),
(60, 'Can view token', 15, 'view_tokenproxy');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `modified_at` datetime(6) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `type`, `created_at`, `modified_at`, `user_id`) VALUES
(1, 'Salary', 'income', '2023-07-28 09:36:51.810949', '2023-07-28 09:36:51.811018', 2),
(2, 'Last month', 'income', '2023-07-28 18:31:31.050901', '2023-07-28 18:31:31.050982', 2),
(3, 'Others', 'income', '2023-07-28 18:31:39.384014', '2023-07-28 18:31:39.384059', 2),
(4, 'Authorised resellers (MBP 16)', 'expense', '2023-07-28 18:35:05.064809', '2023-07-28 18:35:05.064871', 2),
(5, 'Family Help', 'expense', '2023-07-28 18:36:32.671375', '2023-07-28 18:36:32.671422', 2),
(6, 'Father Net', 'expense', '2023-07-28 18:36:41.232853', '2023-07-28 18:36:41.232908', 2),
(7, 'Transportation', 'expense', '2023-07-28 18:37:20.458931', '2023-07-28 18:37:20.459018', 2),
(8, 'Medical && Health Care', 'expense', '2023-07-28 18:37:39.480398', '2023-07-28 18:37:39.480434', 2),
(9, 'Utilities', 'expense', '2023-07-28 18:37:48.707716', '2023-07-28 18:37:48.707767', 2),
(10, 'Food', 'expense', '2023-07-28 18:38:02.815294', '2023-07-28 18:38:02.815339', 2),
(11, 'Entertainment && Gaming', 'expense', '2023-07-28 18:38:18.526701', '2023-07-28 18:38:18.526748', 2),
(12, 'Untracked', 'expense', '2023-07-28 18:38:29.116298', '2023-07-28 18:38:29.116355', 2),
(13, 'Transfer To Next Month', 'expense', '2023-07-28 18:38:50.801925', '2023-07-28 18:38:50.801971', 2),
(14, 'Charity', 'expense', '2023-07-28 18:39:00.451530', '2023-07-28 18:39:00.451623', 2),
(15, 'GYM', 'expense', '2023-07-28 18:39:10.824602', '2023-07-28 18:39:10.824651', 2),
(16, 'Personal Spending', 'expense', '2023-07-28 19:04:29.091260', '2023-07-28 19:04:29.091278', 2),
(17, 'Dressing and Clothing', 'expense', '2023-08-14 18:24:27.335910', '2023-08-14 18:24:27.335992', 2),
(18, 'Khidmadaha Appka (eDir)', 'expense', '2023-08-21 15:54:01.835640', '2023-08-21 15:54:01.835680', 2),
(19, 'For dept purpose', 'income', '2023-08-28 20:07:36.278950', '2023-08-28 20:07:36.279002', 2),
(20, 'Barber shop', 'expense', '2023-08-30 18:21:24.107268', '2023-08-30 18:21:24.107321', 2),
(21, 'For dept purpose', 'expense', '2023-08-30 18:56:19.320991', '2023-08-30 18:56:19.321035', 2),
(22, 'Samsung TV ( Haleel Samsung )', 'expense', '2023-08-30 19:06:24.325973', '2023-08-30 19:06:24.326001', 2);

-- --------------------------------------------------------

--
-- Table structure for table `deptors`
--

CREATE TABLE `deptors` (
  `id` bigint(20) NOT NULL,
  `name` varchar(500) NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `modified_at` datetime(6) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `deptors`
--

INSERT INTO `deptors` (`id`, `name`, `created_at`, `modified_at`, `user_id`) VALUES
(1, 'Abdiqani HNSQ ( Friend )', '2023-07-31 10:36:58.622658', '2023-07-31 10:36:58.622780', 2),
(2, 'Abdisalaam ( Friend )', '2023-08-02 16:09:06.536854', '2023-08-02 16:09:06.536885', 2),
(3, 'Mohammed Ahmed Nur ( Friend )', '2023-08-19 03:15:08.967606', '2023-08-19 03:15:08.967986', 2);

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(6, 'accounts', 'audittrials'),
(7, 'accounts', 'errorlogs'),
(9, 'accounts', 'payments'),
(8, 'accounts', 'users'),
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(14, 'authtoken', 'token'),
(15, 'authtoken', 'tokenproxy'),
(4, 'contenttypes', 'contenttype'),
(11, 'finance', 'accounts'),
(10, 'finance', 'category'),
(12, 'finance', 'deptors'),
(13, 'finance', 'transactions'),
(5, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` bigint(20) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2023-07-28 09:28:45.159906'),
(2, 'contenttypes', '0002_remove_content_type_name', '2023-07-28 09:28:45.187862'),
(3, 'auth', '0001_initial', '2023-07-28 09:28:45.281766'),
(4, 'auth', '0002_alter_permission_name_max_length', '2023-07-28 09:28:45.300561'),
(5, 'auth', '0003_alter_user_email_max_length', '2023-07-28 09:28:45.304167'),
(6, 'auth', '0004_alter_user_username_opts', '2023-07-28 09:28:45.308673'),
(7, 'auth', '0005_alter_user_last_login_null', '2023-07-28 09:28:45.312058'),
(8, 'auth', '0006_require_contenttypes_0002', '2023-07-28 09:28:45.313629'),
(9, 'auth', '0007_alter_validators_add_error_messages', '2023-07-28 09:28:45.316723'),
(10, 'auth', '0008_alter_user_username_max_length', '2023-07-28 09:28:45.319657'),
(11, 'auth', '0009_alter_user_last_name_max_length', '2023-07-28 09:28:45.323627'),
(12, 'auth', '0010_alter_group_name_max_length', '2023-07-28 09:28:45.332413'),
(13, 'auth', '0011_update_proxy_permissions', '2023-07-28 09:28:45.335986'),
(14, 'auth', '0012_alter_user_first_name_max_length', '2023-07-28 09:28:45.339398'),
(15, 'accounts', '0001_initial', '2023-07-28 09:28:45.477070'),
(16, 'accounts', '0002_rename_expected_error_errorlogs_expected_error_and_more', '2023-07-28 09:28:45.569273'),
(17, 'accounts', '0003_remove_audittrials_avatar_remove_audittrials_name_and_more', '2023-07-28 09:28:45.652340'),
(18, 'accounts', '0004_users_is_payment_valid_payments', '2023-07-28 09:28:45.727397'),
(19, 'admin', '0001_initial', '2023-07-28 09:28:45.792142'),
(20, 'admin', '0002_logentry_remove_auto_add', '2023-07-28 09:28:45.801214'),
(21, 'admin', '0003_logentry_add_action_flag_choices', '2023-07-28 09:28:45.810761'),
(22, 'authtoken', '0001_initial', '2023-07-28 09:28:45.851786'),
(23, 'authtoken', '0002_auto_20160226_1747', '2023-07-28 09:28:45.870852'),
(24, 'authtoken', '0003_tokenproxy', '2023-07-28 09:28:45.872091'),
(25, 'finance', '0001_initial', '2023-07-28 09:28:45.903260'),
(26, 'finance', '0002_alter_category_table', '2023-07-28 09:28:45.910751'),
(27, 'finance', '0003_category_color_category_icon', '2023-07-28 09:28:45.942030'),
(28, 'finance', '0004_accounts_deptors_transactions', '2023-07-28 09:28:46.052021'),
(29, 'finance', '0005_remove_category_color_remove_category_icon', '2023-07-28 09:28:46.072476'),
(30, 'finance', '0006_alter_transactions_account_and_more', '2023-07-28 09:28:46.282383'),
(31, 'finance', '0007_transactions_month_transactions_year', '2023-07-28 09:28:46.324026'),
(32, 'finance', '0008_transactions_transaction_date', '2023-07-28 09:28:46.341051'),
(33, 'finance', '0009_transactions_source_account_transactions_sub_type_and_more', '2023-07-28 09:28:46.398434'),
(34, 'finance', '0010_alter_transactions_sub_type', '2023-07-28 09:28:46.411344'),
(35, 'sessions', '0001_initial', '2023-07-28 09:28:46.423958'),
(36, 'finance', '0011_alter_transactions_options_transactions_balance_and_more', '2023-08-06 03:31:39.018001');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('09i6fd3bpsnixnxoyqtwgemx2vyd50mw', '.eJxVjMsOwiAQRf-FtSFlKAy4dO83kIGZStWUpI-V8d-1SRe6veec-1KJtrWmbZE5jazOCtTpd8tUHjLtgO803ZoubVrnMetd0Qdd9LWxPC-H-3dQaanfOoMJZKyLgmhyQAcojrx4ttwDA7E46yNaU7o-2FiMJzYw5G7ADkqv3h_I0Tc7:1qc5ch:O1icTmhRBAg8DAflUQgAP6yXF6yrS2GX-OAZkeT3oSk', '2023-09-15 14:59:47.725457'),
('8smnptu12wvq5l7foxkqmn3h4k4vpn3t', '.eJxVjMsOwiAUBf-FtSFcWlBcuvcbyH2AVA0kpV0Z_12bdKHbMzPnpSKuS4lrT3OcRJ0VqMPvRsiPVDcgd6y3prnVZZ5Ib4readfXJul52d2_g4K9fOvMgwePEMSTFbY5DCjkPCQ7GCPkLWWggC4xQxgZRuOTQ7HhGFBOTr0_98k4Ug:1qPJoL:-dgSe-SDeWW8WYl_HnZMCX5s4yk6nPCKv3bk3kBVaIk', '2023-08-11 09:31:01.636957'),
('bzw70qoxnmbbb2lqg19cbwb8kxsys59f', '.eJxVjMsOwiAQRf-FtSFlKAy4dO83kIGZStWUpI-V8d-1SRe6veec-1KJtrWmbZE5jazOCtTpd8tUHjLtgO803ZoubVrnMetd0Qdd9LWxPC-H-3dQaanfOoMJZKyLgmhyQAcojrx4ttwDA7E46yNaU7o-2FiMJzYw5G7ADkqv3h_I0Tc7:1qaiYr:NwHz0U988PNEQkSDzjf78BaRCI-XKV83QHwG3gHVwtk', '2023-09-11 20:10:09.988087'),
('m0dybyukqu511pqautspdwbj5eg8fovy', '.eJxVjMsOwiAQRf-FtSFlKAy4dO83kIGZStWUpI-V8d-1SRe6veec-1KJtrWmbZE5jazOCtTpd8tUHjLtgO803ZoubVrnMetd0Qdd9LWxPC-H-3dQaanfOoMJZKyLgmhyQAcojrx4ttwDA7E46yNaU7o-2FiMJzYw5G7ADkqv3h_I0Tc7:1qRw6g:hi9zjzraNqpqMH6pW0lK8cieD7y3rhaX-5SIeLbcFoU', '2023-08-18 14:48:46.037284'),
('xupeurr8k2nod6ika6ojjam0a4bsv6vm', '.eJxVjMsOwiAQRf-FtSFlKAy4dO83kIGZStWUpI-V8d-1SRe6veec-1KJtrWmbZE5jazOCtTpd8tUHjLtgO803ZoubVrnMetd0Qdd9LWxPC-H-3dQaanfOoMJZKyLgmhyQAcojrx4ttwDA7E46yNaU7o-2FiMJzYw5G7ADkqv3h_I0Tc7:1qPJrV:Ym1Y6kZfStFk5B3cVII4AdsUQTOsl2jd2p2zfM9P-8U', '2023-08-11 09:34:17.724839');

-- --------------------------------------------------------

--
-- Table structure for table `errorlogs`
--

CREATE TABLE `errorlogs` (
  `id` bigint(20) NOT NULL,
  `expected_error` varchar(500) NOT NULL,
  `field_error` varchar(500) NOT NULL,
  `trace_back` longtext NOT NULL,
  `line_number` int(11) NOT NULL,
  `date_recorded` datetime(6) NOT NULL,
  `browser` varchar(500) NOT NULL,
  `ip_address` varchar(500) NOT NULL,
  `os` longtext NOT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `errorlogs`
--

INSERT INTO `errorlogs` (`id`, `expected_error`, `field_error`, `trace_back`, `line_number`, `date_recorded`, `browser`, `ip_address`, `os`, `user_id`) VALUES
(1, '<class \'django.core.exceptions.FieldError\'>', 'Cannot resolve keyword \'account_id\' into field. Choices are: created_at, from_account, id, modified_at, name, to_account, user, user_id', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 193, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 130, in AccountDetails\n    accounts_list = accounts.objects.filter(**args).order_by(\'-created_at\')\n                    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1534, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1565, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1415, in build_filter\n    lookups, parts, reffed_expression = self.solve_lookup_type(arg, summarize)\n                                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1225, in solve_lookup_type\n    _, field, _, lookup_parts = self.names_to_path(lookup_splitted, self.get_meta())\n                                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1713, in names_to_path\n    raise FieldError(\ndjango.core.exceptions.FieldError: Cannot resolve keyword \'account_id\' into field. Choices are: created_at, from_account, id, modified_at, name, to_account, user, user_id\n', 193, '2023-07-28 20:33:41.850375', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(2, '<class \'django.core.exceptions.FieldError\'>', 'Cannot resolve keyword \'account\' into field. Choices are: created_at, from_account, id, modified_at, name, to_account, user, user_id', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 193, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 130, in AccountDetails\n    accounts_list = accounts.objects.filter(**args).order_by(\'-created_at\')\n                    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1534, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1565, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1415, in build_filter\n    lookups, parts, reffed_expression = self.solve_lookup_type(arg, summarize)\n                                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1225, in solve_lookup_type\n    _, field, _, lookup_parts = self.names_to_path(lookup_splitted, self.get_meta())\n                                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1713, in names_to_path\n    raise FieldError(\ndjango.core.exceptions.FieldError: Cannot resolve keyword \'account\' into field. Choices are: created_at, from_account, id, modified_at, name, to_account, user, user_id\n', 193, '2023-07-28 20:34:15.335051', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(3, '<class \'django.urls.exceptions.NoReverseMatch\'>', 'Reverse for \'AccountStatement\' not found. \'AccountStatement\' is not a valid view function or pattern name.', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 193, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 156, in AccountDetails\n    return render(request, \'essentials/account_details.html\', context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/shortcuts.py\", line 24, in render\n    content = loader.render_to_string(template_name, context, request, using=using)\n              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader.py\", line 62, in render_to_string\n    return template.render(context, request)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/backends/django.py\", line 61, in render\n    return self.template.render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 175, in render\n    return self._render(context)\n           ^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 167, in _render\n    return self.nodelist.render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in render\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in <listcomp>\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 966, in render_annotated\n    return self.render(context)\n           ^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader_tags.py\", line 157, in render\n    return compiled_parent._render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 167, in _render\n    return self.nodelist.render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in render\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in <listcomp>\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 966, in render_annotated\n    return self.render(context)\n           ^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader_tags.py\", line 63, in render\n    result = block.nodelist.render(context)\n             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in render\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in <listcomp>\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 966, in render_annotated\n    return self.render(context)\n           ^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/defaulttags.py\", line 471, in render\n    url = reverse(view_name, args=args, kwargs=kwargs, current_app=current_app)\n          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/urls/base.py\", line 88, in reverse\n    return resolver._reverse_with_prefix(view, prefix, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/urls/resolvers.py\", line 828, in _reverse_with_prefix\n    raise NoReverseMatch(msg)\ndjango.urls.exceptions.NoReverseMatch: Reverse for \'AccountStatement\' not found. \'AccountStatement\' is not a valid view function or pattern name.\n', 193, '2023-07-28 20:39:51.684091', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(4, '<class \'AttributeError\'>', '\'QuerySet\' object has no attribute \'value\'', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 193, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 131, in AccountDetails\n    years = transactions.objects.filter(**args).value(\'year\').distinct()\n            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\nAttributeError: \'QuerySet\' object has no attribute \'value\'\n', 193, '2023-07-28 21:29:51.655985', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(5, '<class \'django.template.exceptions.TemplateSyntaxError\'>', 'Invalid block tag on line 74: \'endif\', expected \'endblock\'. Did you forget to register or load this tag?', 'Traceback (most recent call last):\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 505, in parse\n    compile_func = self.tags[command]\n                   ~~~~~~~~~^^^^^^^^^\nKeyError: \'endif\'\n\nDuring handling of the above exception, another exception occurred:\n\nTraceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 193, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 192, in AccountDetails\n    return render(request, \'essentials/account_details.html\', context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/shortcuts.py\", line 24, in render\n    content = loader.render_to_string(template_name, context, request, using=using)\n              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader.py\", line 61, in render_to_string\n    template = get_template(template_name, using=using)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader.py\", line 15, in get_template\n    return engine.get_template(template_name)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/backends/django.py\", line 33, in get_template\n    return Template(self.engine.get_template(template_name), self)\n                    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/engine.py\", line 175, in get_template\n    template, origin = self.find_template(template_name)\n                       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/engine.py\", line 157, in find_template\n    template = loader.get_template(name, skip=skip)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loaders/cached.py\", line 57, in get_template\n    template = super().get_template(template_name, skip)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loaders/base.py\", line 28, in get_template\n    return Template(\n           ^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 154, in __init__\n    self.nodelist = self.compile_nodelist()\n                    ^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 200, in compile_nodelist\n    return parser.parse()\n           ^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 513, in parse\n    raise self.error(token, e)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 511, in parse\n    compiled_result = compile_func(self, token)\n                      ^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader_tags.py\", line 293, in do_extends\n    nodelist = parser.parse()\n               ^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 513, in parse\n    raise self.error(token, e)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 511, in parse\n    compiled_result = compile_func(self, token)\n                      ^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader_tags.py\", line 232, in do_block\n    nodelist = parser.parse((\"endblock\",))\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 507, in parse\n    self.invalid_block_tag(token, command, parse_until)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 558, in invalid_block_tag\n    raise self.error(\ndjango.template.exceptions.TemplateSyntaxError: Invalid block tag on line 74: \'endif\', expected \'endblock\'. Did you forget to register or load this tag?\n', 193, '2023-07-28 22:02:37.292500', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(6, '<class \'django.core.exceptions.FieldError\'>', 'Cannot resolve keyword \'name\' into field. Choices are: account, account_id, amount, category, category_id, created_at, deptors, deptors_id, id, modified_at, month, source_account, source_account_id, sub_type, transaction_date, type, user, user_id, year', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 193, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 161, in AccountDetails\n    transactions_list = transactions.get_account_transactions(args , request)\n                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 209, in get_account_transactions\n    transactions = transactions.filter(Q(name__icontains=search) | Q(user__username__icontains=search) | Q(\n                   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1534, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1565, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1393, in build_filter\n    return self._add_q(\n           ^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1565, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1415, in build_filter\n    lookups, parts, reffed_expression = self.solve_lookup_type(arg, summarize)\n                                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1225, in solve_lookup_type\n    _, field, _, lookup_parts = self.names_to_path(lookup_splitted, self.get_meta())\n                                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1713, in names_to_path\n    raise FieldError(\ndjango.core.exceptions.FieldError: Cannot resolve keyword \'name\' into field. Choices are: account, account_id, amount, category, category_id, created_at, deptors, deptors_id, id, modified_at, month, source_account, source_account_id, sub_type, transaction_date, type, user, user_id, year\n', 193, '2023-07-31 20:00:28.150210', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(7, '<class \'django.template.exceptions.TemplateDoesNotExist\'>', 'Base/401.html', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 193, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/accounts/views.py\", line 219, in AccountsList\n    return render(request, \'Base/401.html\')\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/shortcuts.py\", line 24, in render\n    content = loader.render_to_string(template_name, context, request, using=using)\n              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader.py\", line 61, in render_to_string\n    template = get_template(template_name, using=using)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader.py\", line 19, in get_template\n    raise TemplateDoesNotExist(template_name, chain=chain)\ndjango.template.exceptions.TemplateDoesNotExist: Base/401.html\n', 193, '2023-08-02 19:28:16.462753', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(8, '<class \'AttributeError\'>', 'module \'django.db.models\' has no attribute \'Users\'', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 193, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/accounts/views.py\", line 190, in AccountsList\n    UsersList = models.Users.objects.filter(\n                ^^^^^^^^^^^^\nAttributeError: module \'django.db.models\' has no attribute \'Users\'\n', 193, '2023-08-02 19:31:09.511221', 'Chrome', '127.0.0.1', 'Macintosh', 1),
(9, '<class \'django.core.exceptions.FieldError\'>', 'Cannot resolve keyword \'is_admin\' into field. Choices are: accounts, auth_token, avatar, category, date_joined, deptors, email, first_name, gender, groups, id, is_active, is_client, is_payment_valid, is_staff, is_superuser, last_login, last_name, logentry, modified_at, password, phone, transactions, user_in_audit_trial, user_in_error_logs, user_in_payments, user_permissions, username', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 193, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/accounts/views.py\", line 190, in AccountsList\n    UsersList = Users.objects.filter(\n                ^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1534, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1565, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1393, in build_filter\n    return self._add_q(\n           ^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1565, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1415, in build_filter\n    lookups, parts, reffed_expression = self.solve_lookup_type(arg, summarize)\n                                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1225, in solve_lookup_type\n    _, field, _, lookup_parts = self.names_to_path(lookup_splitted, self.get_meta())\n                                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1713, in names_to_path\n    raise FieldError(\ndjango.core.exceptions.FieldError: Cannot resolve keyword \'is_admin\' into field. Choices are: accounts, auth_token, avatar, category, date_joined, deptors, email, first_name, gender, groups, id, is_active, is_client, is_payment_valid, is_staff, is_superuser, last_login, last_name, logentry, modified_at, password, phone, transactions, user_in_audit_trial, user_in_error_logs, user_in_payments, user_permissions, username\n', 193, '2023-08-02 19:31:34.927822', 'Chrome', '127.0.0.1', 'Macintosh', 1),
(10, '<class \'django.template.exceptions.TemplateDoesNotExist\'>', 'Users/user_lists.html', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 193, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/accounts/views.py\", line 216, in AccountsList\n    return render(request, \'Users/user_lists.html\', context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/shortcuts.py\", line 24, in render\n    content = loader.render_to_string(template_name, context, request, using=using)\n              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader.py\", line 61, in render_to_string\n    template = get_template(template_name, using=using)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader.py\", line 19, in get_template\n    raise TemplateDoesNotExist(template_name, chain=chain)\ndjango.template.exceptions.TemplateDoesNotExist: Users/user_lists.html\n', 193, '2023-08-02 19:32:09.933157', 'Chrome', '127.0.0.1', 'Macintosh', 1),
(11, '<class \'django.core.exceptions.ValidationError\'>', '[\'“” value has an invalid date format. It must be in YYYY-MM-DD format.\']', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 193, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 327, in TransactionList\n    transactions_list = transactions.objects.filter(\n                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1545, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1576, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1491, in build_filter\n    condition = self.build_lookup(lookups, col, value)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1318, in build_lookup\n    lookup = lookup_class(lhs, rhs)\n             ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 27, in __init__\n    self.rhs = self.get_prep_lookup()\n               ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 341, in get_prep_lookup\n    return super().get_prep_lookup()\n           ^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 85, in get_prep_lookup\n    return self.lhs.output_field.get_prep_value(self.rhs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 1464, in get_prep_value\n    return self.to_python(value)\n           ^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 1430, in to_python\n    raise exceptions.ValidationError(\ndjango.core.exceptions.ValidationError: [\'“” value has an invalid date format. It must be in YYYY-MM-DD format.\']\n', 193, '2023-08-06 04:10:07.434705', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(12, '<class \'ValueError\'>', 'Field \'id\' expected a number but got \'MQ:1qUUwN:q1QR98b4zrRULLBHTCSdTK7tSoBctbFYKBI5DCMs8Eo\'.', 'Traceback (most recent call last):\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2053, in get_prep_value\n    return int(value)\n           ^^^^^^^^^^\nValueError: invalid literal for int() with base 10: \'MQ:1qUUwN:q1QR98b4zrRULLBHTCSdTK7tSoBctbFYKBI5DCMs8Eo\'\n\nThe above exception was the direct cause of the following exception:\n\nTraceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/essentails.py\", line 541, in ManageEssentails\n    account_id = accounts.objects.get(id=account_id)\n                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 623, in get\n    clone = self._chain() if self.query.combinator else self.filter(*args, **kwargs)\n                                                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1545, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1576, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1491, in build_filter\n    condition = self.build_lookup(lookups, col, value)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1318, in build_lookup\n    lookup = lookup_class(lhs, rhs)\n             ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 27, in __init__\n    self.rhs = self.get_prep_lookup()\n               ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 341, in get_prep_lookup\n    return super().get_prep_lookup()\n           ^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 85, in get_prep_lookup\n    return self.lhs.output_field.get_prep_value(self.rhs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2055, in get_prep_value\n    raise e.__class__(\nValueError: Field \'id\' expected a number but got \'MQ:1qUUwN:q1QR98b4zrRULLBHTCSdTK7tSoBctbFYKBI5DCMs8Eo\'.\n', 222, '2023-08-11 16:24:55.625087', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(13, '<class \'ValueError\'>', 'Field \'id\' expected a number but got \'MQ:1qUUwN:q1QR98b4zrRULLBHTCSdTK7tSoBctbFYKBI5DCMs8Eo\'.', 'Traceback (most recent call last):\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2053, in get_prep_value\n    return int(value)\n           ^^^^^^^^^^\nValueError: invalid literal for int() with base 10: \'MQ:1qUUwN:q1QR98b4zrRULLBHTCSdTK7tSoBctbFYKBI5DCMs8Eo\'\n\nThe above exception was the direct cause of the following exception:\n\nTraceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/essentails.py\", line 541, in ManageEssentails\n    account_id = accounts.objects.get(id=account_id)\n                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 623, in get\n    clone = self._chain() if self.query.combinator else self.filter(*args, **kwargs)\n                                                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1545, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1576, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1491, in build_filter\n    condition = self.build_lookup(lookups, col, value)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1318, in build_lookup\n    lookup = lookup_class(lhs, rhs)\n             ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 27, in __init__\n    self.rhs = self.get_prep_lookup()\n               ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 341, in get_prep_lookup\n    return super().get_prep_lookup()\n           ^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 85, in get_prep_lookup\n    return self.lhs.output_field.get_prep_value(self.rhs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2055, in get_prep_value\n    raise e.__class__(\nValueError: Field \'id\' expected a number but got \'MQ:1qUUwN:q1QR98b4zrRULLBHTCSdTK7tSoBctbFYKBI5DCMs8Eo\'.\n', 222, '2023-08-11 16:25:11.830446', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(14, '<class \'ValueError\'>', 'Field \'id\' expected a number but got \'MQ:1qUUwN:q1QR98b4zrRULLBHTCSdTK7tSoBctbFYKBI5DCMs8Eo\'.', 'Traceback (most recent call last):\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2053, in get_prep_value\n    return int(value)\n           ^^^^^^^^^^\nValueError: invalid literal for int() with base 10: \'MQ:1qUUwN:q1QR98b4zrRULLBHTCSdTK7tSoBctbFYKBI5DCMs8Eo\'\n\nThe above exception was the direct cause of the following exception:\n\nTraceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/essentails.py\", line 541, in ManageEssentails\n    account_id = accounts.objects.get(id=account_id)\n                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 623, in get\n    clone = self._chain() if self.query.combinator else self.filter(*args, **kwargs)\n                                                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1545, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1576, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1491, in build_filter\n    condition = self.build_lookup(lookups, col, value)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1318, in build_lookup\n    lookup = lookup_class(lhs, rhs)\n             ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 27, in __init__\n    self.rhs = self.get_prep_lookup()\n               ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 341, in get_prep_lookup\n    return super().get_prep_lookup()\n           ^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 85, in get_prep_lookup\n    return self.lhs.output_field.get_prep_value(self.rhs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2055, in get_prep_value\n    raise e.__class__(\nValueError: Field \'id\' expected a number but got \'MQ:1qUUwN:q1QR98b4zrRULLBHTCSdTK7tSoBctbFYKBI5DCMs8Eo\'.\n', 222, '2023-08-11 16:25:26.940479', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(15, '<class \'django.core.signing.BadSignature\'>', 'No \":\" found in value', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 119, in AccountDetails\n    account = accounts.objects.get(id=signing.loads(id))\n                                      ^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/core/signing.py\", line 172, in loads\n    ).unsign_object(\n      ^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/core/signing.py\", line 268, in unsign_object\n    base64d = self.unsign(signed_obj, **kwargs).encode()\n              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/core/signing.py\", line 292, in unsign\n    result = super().unsign(value)\n             ^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/core/signing.py\", line 233, in unsign\n    raise BadSignature(\'No \"%s\" found in value\' % self.sep)\ndjango.core.signing.BadSignature: No \":\" found in value\n', 222, '2023-08-11 16:27:54.388793', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(16, '<class \'django.core.signing.BadSignature\'>', 'No \":\" found in value', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 119, in AccountDetails\n    account = accounts.objects.get(id=signing.loads(id))\n                                      ^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/core/signing.py\", line 172, in loads\n    ).unsign_object(\n      ^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/core/signing.py\", line 268, in unsign_object\n    base64d = self.unsign(signed_obj, **kwargs).encode()\n              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/core/signing.py\", line 292, in unsign\n    result = super().unsign(value)\n             ^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/core/signing.py\", line 233, in unsign\n    raise BadSignature(\'No \"%s\" found in value\' % self.sep)\ndjango.core.signing.BadSignature: No \":\" found in value\n', 222, '2023-08-11 17:44:18.317432', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(17, '<class \'django.core.signing.BadSignature\'>', 'No \":\" found in value', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 119, in AccountDetails\n    account = accounts.objects.get(id=signing.loads(id))\n                                      ^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/core/signing.py\", line 172, in loads\n    ).unsign_object(\n      ^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/core/signing.py\", line 268, in unsign_object\n    base64d = self.unsign(signed_obj, **kwargs).encode()\n              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/core/signing.py\", line 292, in unsign\n    result = super().unsign(value)\n             ^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/core/signing.py\", line 233, in unsign\n    raise BadSignature(\'No \"%s\" found in value\' % self.sep)\ndjango.core.signing.BadSignature: No \":\" found in value\n', 222, '2023-08-11 17:45:54.589972', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(18, '<class \'AttributeError\'>', '\'QuerySet\' object has no attribute \'value\'', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/dashboard.py\", line 24, in ManageDashboard\n    for index, item in enumerate(transactions.objects.all().value(\'year\').distinct())\n                                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\nAttributeError: \'QuerySet\' object has no attribute \'value\'\n', 222, '2023-08-21 18:27:52.800246', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(19, '<class \'AttributeError\'>', '\'QuerySet\' object has no attribute \'value\'', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/dashboard.py\", line 24, in ManageDashboard\n    for index, item in enumerate(transactions.objects.all().value(\'year\').distinct())\n                                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\nAttributeError: \'QuerySet\' object has no attribute \'value\'\n', 222, '2023-08-21 18:28:01.574881', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(20, '<class \'TypeError\'>', 'list indices must be integers or slices, not str', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/dashboard.py\", line 34, in ManageDashboard\n    year_args[\'year\'] = year\n    ~~~~~~~~~^^^^^^^^\nTypeError: list indices must be integers or slices, not str\n', 222, '2023-08-21 18:47:33.932614', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(21, '<class \'UnboundLocalError\'>', 'cannot access local variable \'data\' where it is not associated with a value', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/dashboard.py\", line 69, in ManageDashboard\n    return JsonResponse({\"message\": data})\n                                    ^^^^\nUnboundLocalError: cannot access local variable \'data\' where it is not associated with a value\n', 222, '2023-08-21 19:34:40.544991', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2);
INSERT INTO `errorlogs` (`id`, `expected_error`, `field_error`, `trace_back`, `line_number`, `date_recorded`, `browser`, `ip_address`, `os`, `user_id`) VALUES
(22, '<class \'TypeError\'>', '\'category\' object is not subscriptable', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/dashboard.py\", line 84, in ManageDashboard\n    keys = {\n           ^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/dashboard.py\", line 85, in <dictcomp>\n    item[\'name\']:  transactions.objects.filter(**chart_args, type=\'withdraw\', category=item.id).aggregate(\n    ~~~~^^^^^^^^\nTypeError: \'category\' object is not subscriptable\n', 222, '2023-08-22 17:41:13.815482', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(23, '<class \'TypeError\'>', '\'category\' object is not subscriptable', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/dashboard.py\", line 85, in ManageDashboard\n    keys = {\n           ^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/dashboard.py\", line 86, in <dictcomp>\n    item[\'name\']:  transactions.objects.filter(**chart_args, type=\'withdraw\', category=item.id).aggregate(\n    ~~~~^^^^^^^^\nTypeError: \'category\' object is not subscriptable\n', 222, '2023-08-22 17:44:34.333583', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(24, '<class \'AttributeError\'>', 'module \'finance.models\' has no attribute \'finance_models\'', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/dashboard.py\", line 25, in ManageDashboard\n    for index, item in enumerate(finance_models.finance_models.transactions.objects.all().values(\'year\').distinct())\n                                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\nAttributeError: module \'finance.models\' has no attribute \'finance_models\'\n', 222, '2023-08-22 17:46:39.654577', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(25, '<class \'TypeError\'>', '\'category\' object is not subscriptable', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/dashboard.py\", line 85, in ManageDashboard\n    keys = {\n           ^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/dashboard.py\", line 86, in <dictcomp>\n    item[\'name\']:  finance_models.transactions.objects.filter(**chart_args, type=\'withdraw\', category=item.id).aggregate(\n    ~~~~^^^^^^^^\nTypeError: \'category\' object is not subscriptable\n', 222, '2023-08-22 17:47:33.103304', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(26, '<class \'ValueError\'>', 'not enough values to unpack (expected 2, got 1)', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 385, in ViewAsExcel\n    transactions.generate_excel_data(year)\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 259, in generate_excel_data\n    for y, month in months:\n        ^^^^^^^^\nValueError: not enough values to unpack (expected 2, got 1)\n', 222, '2023-08-30 19:56:40.844894', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(27, '<class \'AttributeError\'>', '\'NoneType\' object has no attribute \'name\'', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 385, in ViewAsExcel\n    transactions.generate_excel_data(year)\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 256, in generate_excel_data\n    \'category\': category.category.name\n                ^^^^^^^^^^^^^^^^^^^^^^\nAttributeError: \'NoneType\' object has no attribute \'name\'\n', 222, '2023-08-30 19:57:54.934260', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(28, '<class \'AttributeError\'>', '\'NoneType\' object has no attribute \'name\'', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 385, in ViewAsExcel\n    transactions.generate_excel_data(year)\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 256, in generate_excel_data\n    \'category\': category.category.name\n                ^^^^^^^^^^^^^^^^^^^^^^\nAttributeError: \'NoneType\' object has no attribute \'name\'\n', 222, '2023-08-30 19:58:42.359917', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(29, '<class \'AttributeError\'>', '\'NoneType\' object has no attribute \'name\'', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 385, in ViewAsExcel\n    transactions.generate_excel_data(year)\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 256, in generate_excel_data\n    \'category\': category.category.name\n                ^^^^^^^^^^^^^^^^^^^^^^\nAttributeError: \'NoneType\' object has no attribute \'name\'\n', 222, '2023-08-30 19:58:45.033517', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(30, '<class \'TypeError\'>', 'In order to allow non-dict objects to be serialized set the safe parameter to False.', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 387, in ViewAsExcel\n    return JsonResponse( transactions.generate_excel_data(year))\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/http/response.py\", line 724, in __init__\n    raise TypeError(\nTypeError: In order to allow non-dict objects to be serialized set the safe parameter to False.\n', 222, '2023-08-30 20:02:26.554922', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(31, '<class \'KeyError\'>', '\'category__id\'', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 387, in ViewAsExcel\n    return JsonResponse({\"Data\": transactions.generate_excel_data(year)})\n                                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 257, in generate_excel_data\n    category__instance = category.objects.get(id=categ[\'category__id\'])\n                                                 ~~~~~^^^^^^^^^^^^^^^^\nKeyError: \'category__id\'\n', 222, '2023-08-30 20:16:21.569680', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(32, '<class \'ValueError\'>', 'Need 2 values to unpack in for loop; got 8. ', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 391, in ViewAsExcel\n    return render(request, \'transactions/view_as_excel.html\', context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/shortcuts.py\", line 24, in render\n    content = loader.render_to_string(template_name, context, request, using=using)\n              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader.py\", line 62, in render_to_string\n    return template.render(context, request)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/backends/django.py\", line 61, in render\n    return self.template.render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 175, in render\n    return self._render(context)\n           ^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 167, in _render\n    return self.nodelist.render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in render\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in <listcomp>\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 966, in render_annotated\n    return self.render(context)\n           ^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader_tags.py\", line 157, in render\n    return compiled_parent._render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 167, in _render\n    return self.nodelist.render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in render\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in <listcomp>\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 966, in render_annotated\n    return self.render(context)\n           ^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader_tags.py\", line 63, in render\n    result = block.nodelist.render(context)\n             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in render\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in <listcomp>\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 966, in render_annotated\n    return self.render(context)\n           ^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/defaulttags.py\", line 321, in render\n    return nodelist.render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in render\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in <listcomp>\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 966, in render_annotated\n    return self.render(context)\n           ^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/defaulttags.py\", line 238, in render\n    nodelist.append(node.render_annotated(context))\n                    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 966, in render_annotated\n    return self.render(context)\n           ^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/defaulttags.py\", line 226, in render\n    raise ValueError(\nValueError: Need 2 values to unpack in for loop; got 8. \n', 222, '2023-08-30 20:44:07.713217', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(33, '<class \'TypeError\'>', 'type Coalesce doesn\'t define __round__ method', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 388, in ViewAsExcel\n    \'summary\': transactions.generate_excel_data(year)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 271, in generate_excel_data\n    total=round(functions.Coalesce(Sum(\'amount\'), 0.0),2)\n          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\nTypeError: type Coalesce doesn\'t define __round__ method\n', 222, '2023-08-30 21:04:30.482749', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(34, '<class \'AttributeError\'>', 'module \'django.db.models.functions\' has no attribute \'round\'', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 388, in ViewAsExcel\n    \'summary\': transactions.generate_excel_data(year)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 271, in generate_excel_data\n    total=functions.round(functions.Coalesce(Sum(\'amount\'), 0.0),2)\n          ^^^^^^^^^^^^^^^\nAttributeError: module \'django.db.models.functions\' has no attribute \'round\'\n', 222, '2023-08-30 21:04:53.859579', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(35, '<class \'TypeError\'>', 'transactions.generate_excel_data() takes 1 positional argument but 2 were given', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 388, in ViewAsExcel\n    \'summary\': transactions.generate_excel_data(year)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\nTypeError: transactions.generate_excel_data() takes 1 positional argument but 2 were given\n', 222, '2023-08-30 21:34:41.194669', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(36, '<class \'AttributeError\'>', '\'QuerySet\' object has no attribute \'value\'', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 388, in ViewAsExcel\n    \'summary\': transactions.generate_excel_data(year)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 294, in generate_excel_data\n    income_categories = cls.objects.filter(year=year, type=\"deposit\", sub_type=\"normal\").value(\'category__id\', flat=True).distinct()\n                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\nAttributeError: \'QuerySet\' object has no attribute \'value\'\n', 222, '2023-08-31 19:59:53.684899', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(37, '<class \'TypeError\'>', 'Field \'id\' expected a number but got {\'category__id\': 1}.', 'Traceback (most recent call last):\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2053, in get_prep_value\n    return int(value)\n           ^^^^^^^^^^\nTypeError: int() argument must be a string, a bytes-like object or a real number, not \'dict\'\n\nThe above exception was the direct cause of the following exception:\n\nTraceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 388, in ViewAsExcel\n    \'summary\': transactions.generate_excel_data(year)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 297, in generate_excel_data\n    category_instance = category.objects.get(id=category_id)\n                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 623, in get\n    clone = self._chain() if self.query.combinator else self.filter(*args, **kwargs)\n                                                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1545, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1576, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1491, in build_filter\n    condition = self.build_lookup(lookups, col, value)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1318, in build_lookup\n    lookup = lookup_class(lhs, rhs)\n             ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 27, in __init__\n    self.rhs = self.get_prep_lookup()\n               ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 341, in get_prep_lookup\n    return super().get_prep_lookup()\n           ^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 85, in get_prep_lookup\n    return self.lhs.output_field.get_prep_value(self.rhs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2055, in get_prep_value\n    raise e.__class__(\nTypeError: Field \'id\' expected a number but got {\'category__id\': 1}.\n', 222, '2023-08-31 20:00:07.342836', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(38, '<class \'TypeError\'>', 'Field \'id\' expected a number but got {\'category__id\': 1}.', 'Traceback (most recent call last):\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2053, in get_prep_value\n    return int(value)\n           ^^^^^^^^^^\nTypeError: int() argument must be a string, a bytes-like object or a real number, not \'dict\'\n\nThe above exception was the direct cause of the following exception:\n\nTraceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 388, in ViewAsExcel\n    \'summary\': transactions.generate_excel_data(year)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 297, in generate_excel_data\n    category_instance = category.objects.get(id=category_id)\n                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 623, in get\n    clone = self._chain() if self.query.combinator else self.filter(*args, **kwargs)\n                                                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1545, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1576, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1491, in build_filter\n    condition = self.build_lookup(lookups, col, value)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1318, in build_lookup\n    lookup = lookup_class(lhs, rhs)\n             ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 27, in __init__\n    self.rhs = self.get_prep_lookup()\n               ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 341, in get_prep_lookup\n    return super().get_prep_lookup()\n           ^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 85, in get_prep_lookup\n    return self.lhs.output_field.get_prep_value(self.rhs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2055, in get_prep_value\n    raise e.__class__(\nTypeError: Field \'id\' expected a number but got {\'category__id\': 1}.\n', 222, '2023-08-31 20:00:08.651158', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(39, '<class \'TypeError\'>', 'Field \'id\' expected a number but got {\'category__id\': 1}.', 'Traceback (most recent call last):\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2053, in get_prep_value\n    return int(value)\n           ^^^^^^^^^^\nTypeError: int() argument must be a string, a bytes-like object or a real number, not \'dict\'\n\nThe above exception was the direct cause of the following exception:\n\nTraceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 388, in ViewAsExcel\n    \'summary\': transactions.generate_excel_data(year)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 297, in generate_excel_data\n    category_instance = category.objects.get(id=category_id)\n                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 623, in get\n    clone = self._chain() if self.query.combinator else self.filter(*args, **kwargs)\n                                                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1545, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1576, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1491, in build_filter\n    condition = self.build_lookup(lookups, col, value)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1318, in build_lookup\n    lookup = lookup_class(lhs, rhs)\n             ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 27, in __init__\n    self.rhs = self.get_prep_lookup()\n               ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 341, in get_prep_lookup\n    return super().get_prep_lookup()\n           ^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 85, in get_prep_lookup\n    return self.lhs.output_field.get_prep_value(self.rhs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2055, in get_prep_value\n    raise e.__class__(\nTypeError: Field \'id\' expected a number but got {\'category__id\': 1}.\n', 222, '2023-08-31 20:00:28.422434', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(40, '<class \'TypeError\'>', 'Field \'id\' expected a number but got {\'category__id\': 1}.', 'Traceback (most recent call last):\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2053, in get_prep_value\n    return int(value)\n           ^^^^^^^^^^\nTypeError: int() argument must be a string, a bytes-like object or a real number, not \'dict\'\n\nThe above exception was the direct cause of the following exception:\n\nTraceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 388, in ViewAsExcel\n    \'summary\': transactions.generate_excel_data(year)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 301, in generate_excel_data\n    total_income = transactions.objects.filter(year=year, month=month, type=\"deposit\", sub_type=\"normal\", category=category_id).aggregate(total=functions.Round(functions.Coalesce(Sum(\'amount\'), 0.0), 2))[\'total\']\n                   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1545, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1576, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1491, in build_filter\n    condition = self.build_lookup(lookups, col, value)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1318, in build_lookup\n    lookup = lookup_class(lhs, rhs)\n             ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 27, in __init__\n    self.rhs = self.get_prep_lookup()\n               ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/related_lookups.py\", line 166, in get_prep_lookup\n    self.rhs = target_field.get_prep_value(self.rhs)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2055, in get_prep_value\n    raise e.__class__(\nTypeError: Field \'id\' expected a number but got {\'category__id\': 1}.\n', 222, '2023-08-31 20:00:59.823031', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(41, '<class \'TypeError\'>', 'Field \'id\' expected a number but got {\'category__id\': 1}.', 'Traceback (most recent call last):\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2053, in get_prep_value\n    return int(value)\n           ^^^^^^^^^^\nTypeError: int() argument must be a string, a bytes-like object or a real number, not \'dict\'\n\nThe above exception was the direct cause of the following exception:\n\nTraceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 388, in ViewAsExcel\n    \'summary\': transactions.generate_excel_data(year)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 301, in generate_excel_data\n    total_income = transactions.objects.filter(year=year, month=month, type=\"deposit\", sub_type=\"normal\", category=category_id).aggregate(total=functions.Round(functions.Coalesce(Sum(\'amount\'), 0.0), 2))[\'total\']\n                   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1545, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1576, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1491, in build_filter\n    condition = self.build_lookup(lookups, col, value)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1318, in build_lookup\n    lookup = lookup_class(lhs, rhs)\n             ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 27, in __init__\n    self.rhs = self.get_prep_lookup()\n               ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/related_lookups.py\", line 166, in get_prep_lookup\n    self.rhs = target_field.get_prep_value(self.rhs)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2055, in get_prep_value\n    raise e.__class__(\nTypeError: Field \'id\' expected a number but got {\'category__id\': 1}.\n', 222, '2023-08-31 20:01:45.539767', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(42, '<class \'TypeError\'>', 'Field \'id\' expected a number but got {\'category__id\': 1}.', 'Traceback (most recent call last):\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2053, in get_prep_value\n    return int(value)\n           ^^^^^^^^^^\nTypeError: int() argument must be a string, a bytes-like object or a real number, not \'dict\'\n\nThe above exception was the direct cause of the following exception:\n\nTraceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 388, in ViewAsExcel\n    \'summary\': transactions.generate_excel_data(year)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 301, in generate_excel_data\n    total_income = transactions.objects.filter(year=year, month=month, type=\"deposit\", sub_type=\"normal\", category=category_id).aggregate(total=functions.Round(functions.Coalesce(Sum(\'amount\'), 0.0), 2))[\'total\']\n                   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1545, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1576, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1491, in build_filter\n    condition = self.build_lookup(lookups, col, value)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1318, in build_lookup\n    lookup = lookup_class(lhs, rhs)\n             ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 27, in __init__\n    self.rhs = self.get_prep_lookup()\n               ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/related_lookups.py\", line 166, in get_prep_lookup\n    self.rhs = target_field.get_prep_value(self.rhs)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2055, in get_prep_value\n    raise e.__class__(\nTypeError: Field \'id\' expected a number but got {\'category__id\': 1}.\n', 222, '2023-08-31 20:01:46.640856', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(43, '<class \'TypeError\'>', 'Field \'id\' expected a number but got {\'category__id\': 1}.', 'Traceback (most recent call last):\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2053, in get_prep_value\n    return int(value)\n           ^^^^^^^^^^\nTypeError: int() argument must be a string, a bytes-like object or a real number, not \'dict\'\n\nThe above exception was the direct cause of the following exception:\n\nTraceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 388, in ViewAsExcel\n    \'summary\': transactions.generate_excel_data(year)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 302, in generate_excel_data\n    total_income = transactions.objects.filter(year=year, month=month, type=\"deposit\", sub_type=\"normal\", category=category_id).aggregate(total=functions.Round(functions.Coalesce(Sum(\'amount\'), 0.0), 2))[\'total\']\n                   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1545, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1576, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1491, in build_filter\n    condition = self.build_lookup(lookups, col, value)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1318, in build_lookup\n    lookup = lookup_class(lhs, rhs)\n             ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 27, in __init__\n    self.rhs = self.get_prep_lookup()\n               ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/related_lookups.py\", line 166, in get_prep_lookup\n    self.rhs = target_field.get_prep_value(self.rhs)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2055, in get_prep_value\n    raise e.__class__(\nTypeError: Field \'id\' expected a number but got {\'category__id\': 1}.\n', 222, '2023-08-31 20:02:30.964854', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2),
(44, '<class \'TypeError\'>', 'Field \'id\' expected a number but got {\'category__id\': 1}.', 'Traceback (most recent call last):\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2053, in get_prep_value\n    return int(value)\n           ^^^^^^^^^^\nTypeError: int() argument must be a string, a bytes-like object or a real number, not \'dict\'\n\nThe above exception was the direct cause of the following exception:\n\nTraceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 388, in ViewAsExcel\n    \'summary\': transactions.generate_excel_data(year)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 303, in generate_excel_data\n    total_income = transactions.objects.filter(year=year, month=month, type=\"deposit\", sub_type=\"normal\", category=category_id).aggregate(total=functions.Round(functions.Coalesce(Sum(\'amount\'), 0.0), 2))[\'total\']\n                   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/manager.py\", line 87, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1436, in filter\n    return self._filter_or_exclude(False, args, kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1454, in _filter_or_exclude\n    clone._filter_or_exclude_inplace(negate, args, kwargs)\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/query.py\", line 1461, in _filter_or_exclude_inplace\n    self._query.add_q(Q(*args, **kwargs))\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1545, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1576, in _add_q\n    child_clause, needed_inner = self.build_filter(\n                                 ^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1491, in build_filter\n    condition = self.build_lookup(lookups, col, value)\n                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/sql/query.py\", line 1318, in build_lookup\n    lookup = lookup_class(lhs, rhs)\n             ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/lookups.py\", line 27, in __init__\n    self.rhs = self.get_prep_lookup()\n               ^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/related_lookups.py\", line 166, in get_prep_lookup\n    self.rhs = target_field.get_prep_value(self.rhs)\n               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/db/models/fields/__init__.py\", line 2055, in get_prep_value\n    raise e.__class__(\nTypeError: Field \'id\' expected a number but got {\'category__id\': 1}.\n', 222, '2023-08-31 20:03:22.583895', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2);
INSERT INTO `errorlogs` (`id`, `expected_error`, `field_error`, `trace_back`, `line_number`, `date_recorded`, `browser`, `ip_address`, `os`, `user_id`) VALUES
(45, '<class \'AttributeError\'>', '\'accounts\' object has no attribute \'account\'', 'Traceback (most recent call last):\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/my_packges/logs.py\", line 222, in wrapper\n    return view_func(request, *args, **kwargs)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/views.py\", line 114, in Accounts\n    return render(request, \'essentials/accounts.html\', context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/shortcuts.py\", line 24, in render\n    content = loader.render_to_string(template_name, context, request, using=using)\n              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader.py\", line 62, in render_to_string\n    return template.render(context, request)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/backends/django.py\", line 61, in render\n    return self.template.render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 175, in render\n    return self._render(context)\n           ^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 167, in _render\n    return self.nodelist.render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in render\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in <listcomp>\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 966, in render_annotated\n    return self.render(context)\n           ^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader_tags.py\", line 157, in render\n    return compiled_parent._render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 167, in _render\n    return self.nodelist.render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in render\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in <listcomp>\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 966, in render_annotated\n    return self.render(context)\n           ^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/loader_tags.py\", line 63, in render\n    result = block.nodelist.render(context)\n             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in render\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in <listcomp>\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 966, in render_annotated\n    return self.render(context)\n           ^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/defaulttags.py\", line 321, in render\n    return nodelist.render(context)\n           ^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in render\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1005, in <listcomp>\n    return SafeString(\"\".join([node.render_annotated(context) for node in self]))\n                               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 966, in render_annotated\n    return self.render(context)\n           ^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/defaulttags.py\", line 238, in render\n    nodelist.append(node.render_annotated(context))\n                    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 966, in render_annotated\n    return self.render(context)\n           ^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 1064, in render\n    output = self.filter_expression.resolve(context)\n             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 715, in resolve\n    obj = self.var.resolve(context)\n          ^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 847, in resolve\n    value = self._resolve_lookup(context)\n            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n  File \"/opt/homebrew/lib/python3.11/site-packages/django/template/base.py\", line 914, in _resolve_lookup\n    current = current()\n              ^^^^^^^^^\n  File \"/Users/mtech-10/Documents/Projects/Django/wealth_track_website/finance/models.py\", line 67, in get_account_balance\n    print(self.account.name , \'=>\', normal_deposit, account_deposit,\n          ^^^^^^^^^^^^\nAttributeError: \'accounts\' object has no attribute \'account\'\n', 222, '2023-09-06 04:43:53.146358', 'ChromiumEdge', '127.0.0.1', 'Macintosh', 2);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) NOT NULL,
  `amount` double NOT NULL,
  `start_date` datetime(6) NOT NULL,
  `end_date` datetime(6) NOT NULL,
  `cancel_duration` int(11) NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `modified_at` datetime(6) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) NOT NULL,
  `amount` double NOT NULL,
  `type` varchar(10) NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `modified_at` datetime(6) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `deptors_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `month` varchar(50) NOT NULL,
  `year` varchar(50) NOT NULL,
  `transaction_date` date NOT NULL,
  `source_account_id` bigint(20) DEFAULT NULL,
  `sub_type` varchar(20) NOT NULL,
  `balance` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `amount`, `type`, `created_at`, `modified_at`, `account_id`, `category_id`, `deptors_id`, `user_id`, `month`, `year`, `transaction_date`, `source_account_id`, `sub_type`, `balance`) VALUES
(1, 712.5, 'deposit', '2023-07-28 18:30:11.984421', '2023-08-06 03:49:32.643043', 1, 1, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 712.5),
(2, 2, 'deposit', '2023-07-28 18:32:23.252203', '2023-08-06 03:49:32.644215', 1, 2, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 714.5),
(3, 10, 'deposit', '2023-07-28 18:32:41.288636', '2023-08-06 03:49:32.645076', 1, 3, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 724.5),
(4, 215, 'withdraw', '2023-07-28 19:02:13.790050', '2023-08-06 03:49:32.646238', 1, 4, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 509.5),
(5, 32.5, 'withdraw', '2023-07-28 19:03:29.526012', '2023-08-06 03:49:32.647111', 1, 5, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 477),
(6, 300, 'withdraw', '2023-07-28 19:03:44.303690', '2023-08-06 03:49:32.648473', 1, 6, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 177),
(7, 44, 'withdraw', '2023-07-28 19:05:02.164131', '2023-08-06 03:49:32.649608', 1, 16, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 133),
(8, 93.5, 'withdraw', '2023-07-28 19:05:19.580802', '2023-08-06 03:49:32.650968', 1, 7, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 39.5),
(9, 6, 'withdraw', '2023-07-28 19:05:48.383284', '2023-08-06 03:49:32.652182', 1, 8, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 33.5),
(10, 5, 'withdraw', '2023-07-28 19:06:14.217051', '2023-08-06 03:49:32.653277', 1, 9, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 28.5),
(11, 9, 'withdraw', '2023-07-28 19:06:28.330729', '2023-08-06 03:49:32.654054', 1, 10, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 19.5),
(12, 4, 'withdraw', '2023-07-28 19:06:48.964528', '2023-08-06 03:49:32.654786', 1, 11, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 15.5),
(13, 9.2, 'withdraw', '2023-07-28 19:07:18.155333', '2023-08-06 03:49:32.655614', 1, 12, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 6.3),
(14, 6.3, 'withdraw', '2023-07-28 19:10:41.255452', '2023-08-06 03:49:32.656502', 1, 13, NULL, 2, '1', '2023', '2023-01-28', NULL, 'normal', 0),
(15, 712.5, 'deposit', '2023-07-28 19:13:29.426878', '2023-08-06 03:49:32.657215', 1, 1, NULL, 2, '2', '2023', '2023-02-28', NULL, 'normal', 712.5),
(16, 6.3, 'deposit', '2023-07-28 19:13:45.042271', '2023-08-06 03:49:32.657960', 1, 2, NULL, 2, '2', '2023', '2023-02-28', NULL, 'normal', 718.8),
(17, 33, 'deposit', '2023-07-28 19:14:45.011324', '2023-08-06 03:49:32.658660', 1, 3, NULL, 2, '2', '2023', '2023-02-28', NULL, 'normal', 751.8),
(18, 100, 'withdraw', '2023-07-28 19:15:10.601078', '2023-08-06 03:49:32.659338', 1, 4, NULL, 2, '2', '2023', '2023-02-28', NULL, 'normal', 651.8),
(19, 42.5, 'withdraw', '2023-07-28 19:15:28.903187', '2023-08-06 03:49:32.660095', 1, 5, NULL, 2, '2', '2023', '2023-02-28', NULL, 'normal', 609.3),
(20, 300, 'withdraw', '2023-07-28 19:16:07.743625', '2023-08-06 03:49:32.661176', 1, 6, NULL, 2, '2', '2023', '2023-02-28', NULL, 'normal', 309.3),
(21, 12, 'withdraw', '2023-07-28 19:16:22.261369', '2023-08-06 03:49:32.662197', 1, 16, NULL, 2, '2', '2023', '2023-02-28', NULL, 'normal', 297.3),
(22, 110, 'withdraw', '2023-07-28 19:16:43.083190', '2023-08-06 03:49:32.663263', 1, 7, NULL, 2, '2', '2023', '2023-02-28', NULL, 'normal', 187.3),
(23, 132.5, 'withdraw', '2023-07-28 19:17:07.840553', '2023-08-06 03:49:32.664789', 1, 9, NULL, 2, '2', '2023', '2023-02-28', NULL, 'normal', 54.8),
(24, 32, 'withdraw', '2023-07-28 19:18:52.379926', '2023-08-06 03:49:32.665968', 1, 10, NULL, 2, '2', '2023', '2023-02-28', NULL, 'normal', 22.8),
(25, 15.8, 'withdraw', '2023-07-28 19:19:35.702314', '2023-08-06 03:49:32.667005', 1, 12, NULL, 2, '2', '2023', '2023-02-28', NULL, 'normal', 7),
(26, 7, 'withdraw', '2023-07-28 19:19:54.763383', '2023-08-06 03:49:32.668077', 1, 14, NULL, 2, '2', '2023', '2023-02-28', NULL, 'normal', -0),
(27, 712.5, 'deposit', '2023-07-28 19:22:31.401804', '2023-08-06 03:49:32.669350', 1, 1, NULL, 2, '3', '2023', '2023-03-28', NULL, 'normal', 712.5),
(28, 300, 'withdraw', '2023-07-28 19:22:50.404193', '2023-08-06 03:49:32.670426', 1, 4, NULL, 2, '3', '2023', '2023-03-28', NULL, 'normal', 412.5),
(29, 68, 'withdraw', '2023-07-28 19:23:10.180025', '2023-08-06 03:49:32.671442', 1, 5, NULL, 2, '3', '2023', '2023-03-28', NULL, 'normal', 344.5),
(30, 22, 'withdraw', '2023-07-28 19:23:21.283832', '2023-08-06 03:49:32.672551', 1, 16, NULL, 2, '3', '2023', '2023-03-28', NULL, 'normal', 322.5),
(31, 137, 'withdraw', '2023-07-28 19:23:38.664575', '2023-08-06 03:49:32.673635', 1, 7, NULL, 2, '3', '2023', '2023-03-28', NULL, 'normal', 185.5),
(32, 98, 'withdraw', '2023-07-28 19:24:06.670991', '2023-08-06 03:49:32.674679', 1, 9, NULL, 2, '3', '2023', '2023-03-28', NULL, 'normal', 87.5),
(33, 16, 'withdraw', '2023-07-28 19:25:13.600276', '2023-08-06 03:49:32.675703', 1, 10, NULL, 2, '3', '2023', '2023-03-28', NULL, 'normal', 71.5),
(34, 5, 'withdraw', '2023-07-28 19:25:26.574577', '2023-08-06 03:49:32.678901', 1, 11, NULL, 2, '3', '2023', '2023-03-28', NULL, 'normal', 66.5),
(35, 5, 'withdraw', '2023-07-28 19:25:46.720727', '2023-08-06 03:49:32.680124', 1, 13, NULL, 2, '3', '2023', '2023-03-28', NULL, 'normal', 61.5),
(36, 4, 'withdraw', '2023-07-28 19:26:02.675240', '2023-08-06 03:49:32.681237', 1, 14, NULL, 2, '3', '2023', '2023-03-28', NULL, 'normal', 57.5),
(37, 57.5, 'withdraw', '2023-07-28 19:26:25.524536', '2023-08-06 03:49:32.682766', 1, 12, NULL, 2, '3', '2023', '2023-03-28', NULL, 'normal', 0),
(38, 712.5, 'deposit', '2023-07-28 19:30:22.441573', '2023-08-06 03:49:32.683900', 1, 1, NULL, 2, '4', '2023', '2023-04-28', NULL, 'normal', 712.5),
(39, 5, 'deposit', '2023-07-28 19:30:40.010954', '2023-08-06 03:49:32.685124', 1, 2, NULL, 2, '4', '2023', '2023-04-28', NULL, 'normal', 717.5),
(40, 28, 'withdraw', '2023-07-28 19:30:59.863072', '2023-08-06 03:49:32.687276', 1, 5, NULL, 2, '4', '2023', '2023-04-28', NULL, 'normal', 689.5),
(41, 300, 'withdraw', '2023-07-28 19:31:14.106072', '2023-08-06 03:49:32.688452', 1, 6, NULL, 2, '4', '2023', '2023-04-28', NULL, 'normal', 389.5),
(42, 1, 'withdraw', '2023-07-28 19:31:31.378207', '2023-08-06 03:49:32.689511', 1, 16, NULL, 2, '4', '2023', '2023-04-28', NULL, 'normal', 388.5),
(43, 84, 'withdraw', '2023-07-28 19:31:47.444362', '2023-08-06 03:49:32.690521', 1, 7, NULL, 2, '4', '2023', '2023-04-28', NULL, 'normal', 304.5),
(44, 171, 'withdraw', '2023-07-28 19:32:04.437943', '2023-08-06 03:49:32.691859', 1, 9, NULL, 2, '4', '2023', '2023-04-28', NULL, 'normal', 133.5),
(45, 18.6, 'withdraw', '2023-07-28 19:32:22.331537', '2023-08-06 03:49:32.693290', 1, 10, NULL, 2, '4', '2023', '2023-04-28', NULL, 'normal', 114.9),
(46, 82.9, 'withdraw', '2023-07-28 19:32:50.254709', '2023-08-06 03:49:32.695659', 1, 12, NULL, 2, '4', '2023', '2023-04-28', NULL, 'normal', 32),
(47, 32, 'withdraw', '2023-07-28 19:33:06.671749', '2023-08-06 03:49:32.697753', 1, 14, NULL, 2, '4', '2023', '2023-04-28', NULL, 'normal', 0),
(48, 712.5, 'deposit', '2023-07-28 19:33:47.610873', '2023-08-06 03:49:32.699321', 1, 1, NULL, 2, '5', '2023', '2023-05-28', NULL, 'normal', 712.5),
(49, 123, 'withdraw', '2023-07-28 19:34:22.836729', '2023-08-06 03:49:32.700732', 1, 5, NULL, 2, '5', '2023', '2023-05-28', NULL, 'normal', 589.5),
(50, 300, 'withdraw', '2023-07-28 19:34:38.491301', '2023-08-06 03:49:32.702259', 1, 6, NULL, 2, '5', '2023', '2023-05-28', NULL, 'normal', 289.5),
(51, 77, 'withdraw', '2023-07-28 19:35:04.623961', '2023-08-06 03:49:32.704138', 1, 7, NULL, 2, '5', '2023', '2023-05-28', NULL, 'normal', 212.5),
(52, 24.7, 'withdraw', '2023-07-28 19:35:18.439897', '2023-08-06 03:49:32.705694', 1, 10, NULL, 2, '5', '2023', '2023-05-28', NULL, 'normal', 187.8),
(53, 58.5, 'withdraw', '2023-07-28 19:35:35.875369', '2023-08-06 03:49:32.707089', 1, 9, NULL, 2, '5', '2023', '2023-05-28', NULL, 'normal', 129.3),
(54, 53.3, 'withdraw', '2023-07-28 19:35:48.141998', '2023-08-06 03:49:32.708447', 1, 12, NULL, 2, '5', '2023', '2023-05-28', NULL, 'normal', 76),
(55, 2, 'withdraw', '2023-07-28 19:38:33.117140', '2023-08-06 03:49:32.712094', 1, 14, NULL, 2, '5', '2023', '2023-05-28', NULL, 'normal', 74),
(56, 4, 'withdraw', '2023-07-28 19:39:51.869131', '2023-08-06 03:49:32.713813', 1, 16, NULL, 2, '5', '2023', '2023-05-28', NULL, 'normal', 70),
(57, 70, 'withdraw', '2023-07-28 19:41:08.643554', '2023-08-06 03:49:32.714811', 1, 12, NULL, 2, '5', '2023', '2023-05-28', NULL, 'normal', 0),
(58, 712.5, 'deposit', '2023-07-28 19:43:12.635412', '2023-08-06 03:49:32.715906', 1, 1, NULL, 2, '6', '2023', '2023-06-28', NULL, 'normal', 712.5),
(59, 94, 'withdraw', '2023-07-28 19:44:19.449686', '2023-08-06 03:49:32.716782', 1, 7, NULL, 2, '6', '2023', '2023-06-28', NULL, 'normal', 618.5),
(60, 68.8, 'withdraw', '2023-07-28 19:46:51.655907', '2023-08-06 03:49:32.717886', 1, 5, NULL, 2, '6', '2023', '2023-06-28', NULL, 'normal', 549.7),
(61, 20, 'withdraw', '2023-07-28 19:47:53.109796', '2023-08-06 03:49:32.718753', 1, 8, NULL, 2, '6', '2023', '2023-06-28', NULL, 'normal', 529.7),
(62, 117, 'withdraw', '2023-07-28 19:48:11.306842', '2023-08-06 03:49:32.721215', 1, 9, NULL, 2, '6', '2023', '2023-06-28', NULL, 'normal', 412.7),
(63, 22, 'withdraw', '2023-07-28 19:48:32.796962', '2023-08-06 03:49:32.722586', 1, 10, NULL, 2, '6', '2023', '2023-06-28', NULL, 'normal', 390.7),
(64, 250, 'withdraw', '2023-07-28 19:48:48.683837', '2023-08-06 03:49:32.723853', 1, 6, NULL, 2, '6', '2023', '2023-06-28', NULL, 'normal', 140.7),
(65, 20, 'withdraw', '2023-07-28 19:49:06.680238', '2023-08-06 03:49:32.725375', 1, 15, NULL, 2, '6', '2023', '2023-06-28', NULL, 'normal', 120.7),
(66, 108.2, 'withdraw', '2023-07-28 19:53:24.016547', '2023-08-06 03:49:32.726684', 1, 12, NULL, 2, '6', '2023', '2023-06-28', NULL, 'normal', 12.5),
(67, 12.5, 'withdraw', '2023-07-28 19:53:41.325153', '2023-08-06 03:49:32.729433', 1, 13, NULL, 2, '6', '2023', '2023-06-28', NULL, 'normal', 0),
(68, 712.5, 'deposit', '2023-07-28 19:54:57.987722', '2023-08-06 03:49:32.731131', 1, 1, NULL, 2, '7', '2023', '2023-07-27', NULL, 'normal', 712.5),
(69, 12.5, 'deposit', '2023-07-28 19:55:09.930414', '2023-08-06 03:49:32.731990', 1, 2, NULL, 2, '7', '2023', '2023-07-27', NULL, 'normal', 725),
(70, 10, 'withdraw', '2023-07-28 19:57:56.156480', '2023-08-06 03:49:32.732704', 1, 5, NULL, 2, '7', '2023', '2023-07-28', NULL, 'normal', 715),
(71, 3, 'withdraw', '2023-07-28 19:58:14.408178', '2023-08-06 03:49:32.733442', 1, 10, NULL, 2, '7', '2023', '2023-07-28', NULL, 'normal', 712),
(72, 5, 'deposit', '2023-07-28 20:00:58.605015', '2023-08-06 03:49:32.734138', 1, 3, NULL, 2, '7', '2023', '2023-07-28', NULL, 'normal', 717),
(74, 1, 'withdraw', '2023-07-29 18:51:20.301720', '2023-08-06 03:49:32.736327', 1, NULL, NULL, 2, '7', '2023', '2023-07-29', 4, 'transfer', 717),
(75, 1, 'withdraw', '2023-07-29 18:52:13.271895', '2023-08-06 03:49:32.738254', 4, 14, NULL, 2, '7', '2023', '2023-07-29', NULL, 'normal', 716),
(77, 3.5, 'withdraw', '2023-07-29 18:52:56.961158', '2023-08-06 03:49:32.740303', 1, NULL, NULL, 2, '7', '2023', '2023-07-29', 2, 'transfer', 716),
(78, 3.5, 'withdraw', '2023-07-29 18:53:16.306648', '2023-08-06 03:49:32.742900', 2, 10, NULL, 2, '7', '2023', '2023-07-29', NULL, 'normal', 712.5),
(80, 1.5, 'withdraw', '2023-07-29 19:01:09.958015', '2023-08-06 03:49:32.745900', 1, NULL, NULL, 2, '7', '2023', '2023-07-29', 4, 'transfer', 712.5),
(81, 1.5, 'withdraw', '2023-07-29 19:01:36.602832', '2023-08-06 03:49:32.746819', 4, 10, NULL, 2, '7', '2023', '2023-07-29', NULL, 'normal', 711),
(83, 8, 'withdraw', '2023-07-29 19:02:18.704778', '2023-08-06 03:49:32.749114', 1, NULL, NULL, 2, '7', '2023', '2023-07-29', 3, 'transfer', 711),
(84, 5, 'withdraw', '2023-07-29 19:18:49.621549', '2023-08-06 03:49:32.751182', 3, 11, NULL, 2, '7', '2023', '2023-07-29', NULL, 'normal', 706),
(85, 10, 'deposit', '2023-07-31 10:37:22.882431', '2023-08-06 03:49:32.752085', 2, NULL, 1, 2, '7', '2023', '2023-07-30', NULL, 'take_dept', 716),
(86, 5, 'withdraw', '2023-07-31 10:38:08.714819', '2023-08-06 03:49:32.752881', 2, 7, NULL, 2, '7', '2023', '2023-07-31', NULL, 'normal', 711),
(87, 4, 'withdraw', '2023-07-31 10:38:26.303699', '2023-08-06 03:49:32.753680', 2, 10, NULL, 2, '7', '2023', '2023-07-30', NULL, 'normal', 707),
(89, 10, 'withdraw', '2023-07-31 10:39:04.066397', '2023-08-06 03:49:32.756643', 1, NULL, NULL, 2, '7', '2023', '2023-07-31', 3, 'transfer', 707),
(90, 10, 'withdraw', '2023-07-31 10:39:29.311331', '2023-08-06 03:49:32.757488', 3, 9, NULL, 2, '7', '2023', '2023-07-31', NULL, 'normal', 697),
(91, 0.5, 'withdraw', '2023-07-31 10:48:35.679713', '2023-08-06 03:49:32.758641', 2, 10, NULL, 2, '7', '2023', '2023-07-31', NULL, 'normal', 696.5),
(93, 0.5, 'withdraw', '2023-07-31 10:48:48.791717', '2023-08-06 03:49:32.765217', 2, NULL, NULL, 2, '7', '2023', '2023-07-31', 4, 'transfer', 696.5),
(95, 10, 'withdraw', '2023-07-31 19:45:07.450419', '2023-08-06 03:49:32.767983', 1, NULL, NULL, 2, '7', '2023', '2023-07-31', 3, 'transfer', 696.5),
(96, 5, 'withdraw', '2023-07-31 19:45:31.762572', '2023-08-06 03:49:32.769402', 3, 10, NULL, 2, '7', '2023', '2023-07-31', NULL, 'normal', 691.5),
(97, 2, 'withdraw', '2023-07-31 19:46:43.915248', '2023-08-06 03:49:32.770530', 3, 9, NULL, 2, '7', '2023', '2023-07-31', NULL, 'normal', 689.5),
(99, 5, 'withdraw', '2023-07-31 19:50:47.060646', '2023-08-06 03:49:32.772216', 1, NULL, NULL, 2, '7', '2023', '2023-07-31', 3, 'transfer', 689.5),
(100, 5, 'withdraw', '2023-07-31 19:51:14.336049', '2023-08-06 03:49:32.774333', 3, 9, NULL, 2, '7', '2023', '2023-07-31', NULL, 'normal', 684.5),
(102, 5, 'withdraw', '2023-07-31 19:52:24.589907', '2023-08-06 03:49:32.779295', 1, NULL, NULL, 2, '7', '2023', '2023-07-31', 3, 'transfer', 684.5),
(104, 100, 'withdraw', '2023-07-31 19:52:43.379161', '2023-08-06 03:49:32.781025', 1, NULL, NULL, 2, '7', '2023', '2023-07-31', 3, 'transfer', 684.5),
(105, 24, 'withdraw', '2023-07-31 19:53:31.864899', '2023-08-06 03:49:32.782421', 3, 9, NULL, 2, '7', '2023', '2023-07-31', NULL, 'normal', 660.5),
(106, 2, 'withdraw', '2023-07-31 19:53:55.998957', '2023-08-06 03:49:32.784273', 3, 8, NULL, 2, '7', '2023', '2023-07-31', NULL, 'normal', 658.5),
(107, 2, 'withdraw', '2023-07-31 19:57:03.546342', '2023-08-06 03:49:32.785177', 3, 10, NULL, 2, '7', '2023', '2023-07-31', NULL, 'normal', 656.5),
(108, 0.25, 'withdraw', '2023-07-31 19:57:59.216523', '2023-08-06 03:49:32.786663', 3, 8, NULL, 2, '7', '2023', '2023-07-31', NULL, 'normal', 656.25),
(109, 1, 'withdraw', '2023-07-31 19:58:34.227436', '2023-08-06 03:49:32.788190', 3, 10, NULL, 2, '7', '2023', '2023-07-31', NULL, 'normal', 655.25),
(110, 50, 'withdraw', '2023-08-02 16:00:05.153476', '2023-08-06 03:49:32.789103', 3, 9, NULL, 2, '8', '2023', '2023-08-01', NULL, 'normal', 605.25),
(111, 2, 'withdraw', '2023-08-02 16:02:06.286589', '2023-08-06 03:49:32.789927', 3, 8, NULL, 2, '8', '2023', '2023-08-01', NULL, 'normal', 603.25),
(112, 15, 'withdraw', '2023-08-02 16:05:37.366825', '2023-08-06 03:49:32.790747', 3, 9, NULL, 2, '8', '2023', '2023-08-01', NULL, 'normal', 588.25),
(114, 50, 'withdraw', '2023-08-02 16:07:39.412748', '2023-08-06 03:49:32.792988', 1, NULL, NULL, 2, '8', '2023', '2023-08-01', 3, 'transfer', 588.25),
(116, 160, 'withdraw', '2023-08-02 16:07:51.853650', '2023-08-06 03:49:32.795826', 1, NULL, NULL, 2, '8', '2023', '2023-08-02', 3, 'transfer', 588.25),
(117, 160, 'withdraw', '2023-08-02 16:08:34.058894', '2023-08-06 03:49:32.796673', 3, 9, NULL, 2, '8', '2023', '2023-08-02', NULL, 'normal', 428.25),
(118, 50, 'withdraw', '2023-08-02 16:09:27.806214', '2023-08-06 03:49:32.797861', 3, NULL, 2, 2, '8', '2023', '2023-08-02', NULL, 'give_dept', 378.25),
(119, 1, 'withdraw', '2023-08-02 16:10:34.495658', '2023-08-06 03:49:32.798955', 3, 16, NULL, 2, '8', '2023', '2023-08-02', NULL, 'normal', 377.25),
(121, 25, 'withdraw', '2023-08-02 16:23:28.331431', '2023-08-06 03:49:32.800558', 1, NULL, NULL, 2, '8', '2023', '2023-08-02', 5, 'transfer', 377.25),
(122, 1, 'withdraw', '2023-08-02 17:21:55.417636', '2023-08-06 03:49:32.801387', 3, 10, NULL, 2, '8', '2023', '2023-08-02', NULL, 'normal', 376.25),
(123, 2.5, 'withdraw', '2023-08-02 17:22:10.420877', '2023-08-06 03:49:32.802814', 3, 9, NULL, 2, '8', '2023', '2023-08-02', NULL, 'normal', 373.75),
(125, 200, 'withdraw', '2023-08-04 14:49:28.417776', '2023-08-06 03:49:32.804680', 1, NULL, NULL, 2, '8', '2023', '2023-08-04', 3, 'transfer', 373.75),
(127, 10, 'withdraw', '2023-08-04 14:51:09.804713', '2023-08-06 03:49:32.809561', 1, NULL, NULL, 2, '8', '2023', '2023-08-04', 3, 'transfer', 373.75),
(128, 200, 'withdraw', '2023-08-04 14:51:47.422453', '2023-08-06 03:49:32.811363', 3, 6, NULL, 2, '8', '2023', '2023-08-04', NULL, 'normal', 173.75),
(129, 9, 'withdraw', '2023-08-04 14:52:11.948591', '2023-08-06 03:49:32.812441', 3, 10, NULL, 2, '8', '2023', '2023-08-04', NULL, 'normal', 164.75),
(130, 1, 'withdraw', '2023-08-04 14:52:23.343425', '2023-08-06 03:49:32.813245', 3, 16, NULL, 2, '8', '2023', '2023-08-04', NULL, 'normal', 163.75),
(131, 2, 'withdraw', '2023-08-04 14:52:36.570868', '2023-08-06 03:49:32.814107', 3, 5, NULL, 2, '8', '2023', '2023-08-04', NULL, 'normal', 161.75),
(132, 3, 'withdraw', '2023-08-04 14:54:20.436215', '2023-08-06 03:49:32.815344', 3, 9, NULL, 2, '8', '2023', '2023-08-04', NULL, 'normal', 158.75),
(133, 2, 'withdraw', '2023-08-04 14:54:47.666780', '2023-08-06 03:49:32.817534', 3, 9, NULL, 2, '8', '2023', '2023-08-04', NULL, 'normal', 156.75),
(135, 1.5, 'withdraw', '2023-08-04 18:22:34.327371', '2023-08-06 03:49:32.818663', 3, 7, NULL, 2, '8', '2023', '2023-08-04', NULL, 'normal', 155.25),
(136, 1.5, 'withdraw', '2023-08-04 18:22:48.518207', '2023-08-06 03:49:32.819472', 3, 10, NULL, 2, '8', '2023', '2023-08-04', NULL, 'normal', 153.75),
(138, 30, 'withdraw', '2023-08-05 16:43:09.311464', '2023-08-06 03:49:32.822826', 1, NULL, NULL, 2, '8', '2023', '2023-08-05', 3, 'transfer', 153.75),
(139, 20, 'withdraw', '2023-08-05 16:43:24.780763', '2023-08-06 03:49:32.823827', 3, 5, NULL, 2, '8', '2023', '2023-08-05', NULL, 'normal', 133.75),
(140, 1, 'withdraw', '2023-08-05 16:43:46.807153', '2023-08-06 03:49:32.824744', 3, 16, NULL, 2, '8', '2023', '2023-08-05', NULL, 'normal', 132.75),
(141, 1, 'withdraw', '2023-08-06 04:05:05.615987', '2023-08-06 04:05:05.616061', 3, 16, NULL, 2, '8', '2023', '2023-08-07', NULL, 'normal', 126.25),
(142, 10, 'withdraw', '2023-08-10 04:02:45.161631', '2023-08-10 04:02:45.161647', 1, NULL, NULL, 2, '8', '2023', '2023-08-08', 3, 'transfer', 116.25),
(144, 4, 'withdraw', '2023-08-10 04:03:09.907546', '2023-08-10 04:03:09.907615', 3, 10, NULL, 2, '8', '2023', '2023-08-10', NULL, 'normal', 122.25),
(145, 3, 'withdraw', '2023-08-10 04:03:34.605341', '2023-08-10 04:03:34.605419', 3, 7, NULL, 2, '8', '2023', '2023-08-08', NULL, 'normal', 116.25),
(146, 3, 'withdraw', '2023-08-10 04:04:56.179427', '2023-08-10 04:04:56.179477', 3, 7, NULL, 2, '8', '2023', '2023-08-09', NULL, 'normal', 113.25),
(147, 1, 'withdraw', '2023-08-10 04:05:23.381198', '2023-08-10 04:05:23.381237', 3, 16, NULL, 2, '8', '2023', '2023-08-09', NULL, 'normal', 112.25),
(148, 1.25, 'withdraw', '2023-08-10 04:05:47.340260', '2023-08-10 04:05:47.340315', 3, 10, NULL, 2, '8', '2023', '2023-08-09', NULL, 'normal', 111),
(149, 0.5, 'withdraw', '2023-08-11 16:24:39.251839', '2023-08-11 16:24:39.251932', 3, 7, NULL, 2, '8', '2023', '2023-08-10', NULL, 'normal', 110.5),
(150, 10, 'withdraw', '2023-08-11 16:27:53.346257', '2023-08-11 16:27:53.346308', 1, NULL, NULL, 2, '8', '2023', '2023-08-11', 3, 'transfer', 100.5),
(152, 10, 'withdraw', '2023-08-11 17:45:53.121675', '2023-08-11 17:45:53.121732', 3, NULL, NULL, 2, '8', '2023', '2023-08-10', 4, 'transfer', 100.5),
(154, 5, 'withdraw', '2023-08-11 17:49:53.225804', '2023-08-11 17:49:53.225852', 4, 10, NULL, 2, '8', '2023', '2023-08-11', NULL, 'normal', 105.5),
(155, 5, 'withdraw', '2023-08-11 17:50:18.581339', '2023-08-11 17:50:18.581380', 4, 10, NULL, 2, '8', '2023', '2023-08-11', NULL, 'normal', 100.5),
(156, 0.5, 'withdraw', '2023-08-11 17:50:40.377591', '2023-08-11 17:50:40.377628', 4, 5, NULL, 2, '8', '2023', '2023-08-10', NULL, 'normal', 100),
(157, 1, 'withdraw', '2023-08-11 17:51:22.642937', '2023-08-11 17:51:22.642969', 3, 16, NULL, 2, '8', '2023', '2023-08-11', NULL, 'normal', 99),
(158, 12, 'withdraw', '2023-08-11 17:56:04.236194', '2023-08-11 17:56:04.236224', 5, 16, NULL, 2, '8', '2023', '2023-08-09', NULL, 'normal', 87),
(159, 4.29, 'withdraw', '2023-08-11 17:57:35.891668', '2023-08-11 17:57:35.891709', 3, 12, NULL, 2, '8', '2023', '2023-08-11', NULL, 'normal', 82.71),
(160, 3, 'withdraw', '2023-08-11 17:59:55.706333', '2023-08-11 17:59:55.706384', 1, 12, NULL, 2, '8', '2023', '2023-08-11', NULL, 'normal', 79.71),
(161, 20, 'withdraw', '2023-08-14 18:21:29.997694', '2023-08-14 18:21:29.997736', 1, NULL, NULL, 2, '8', '2023', '2023-08-12', 3, 'transfer', 59.709999999999994),
(163, 16, 'withdraw', '2023-08-14 18:25:09.765374', '2023-08-14 18:25:09.765406', 3, 17, NULL, 2, '8', '2023', '2023-08-12', NULL, 'normal', 63.709999999999994),
(164, 1, 'withdraw', '2023-08-14 18:25:30.500883', '2023-08-14 18:25:30.500916', 3, 7, NULL, 2, '8', '2023', '2023-08-12', NULL, 'normal', 62.709999999999994),
(165, 1, 'withdraw', '2023-08-14 18:27:26.605332', '2023-08-14 18:27:26.605361', 3, 16, NULL, 2, '8', '2023', '2023-08-13', NULL, 'normal', 61.709999999999994),
(166, 1, 'withdraw', '2023-08-14 18:27:40.655283', '2023-08-14 18:27:40.655313', 3, 10, NULL, 2, '8', '2023', '2023-08-13', NULL, 'normal', 60.709999999999994),
(167, 1, 'withdraw', '2023-08-14 18:27:59.111489', '2023-08-14 18:27:59.111519', 3, 9, NULL, 2, '8', '2023', '2023-08-13', NULL, 'normal', 59.709999999999994),
(168, 10, 'withdraw', '2023-08-14 18:28:16.756249', '2023-08-14 18:28:16.756280', 1, NULL, NULL, 2, '8', '2023', '2023-08-14', 3, 'transfer', 49.709999999999994),
(170, 0.5, 'withdraw', '2023-08-14 18:28:34.901602', '2023-08-14 18:28:34.901629', 3, 7, NULL, 2, '8', '2023', '2023-08-14', NULL, 'normal', 59.209999999999994),
(171, 1, 'withdraw', '2023-08-14 18:28:50.893391', '2023-08-14 18:28:50.893419', 3, NULL, NULL, 2, '8', '2023', '2023-08-14', 2, 'transfer', 58.209999999999994),
(173, 1, 'withdraw', '2023-08-14 18:29:07.785742', '2023-08-14 18:29:07.785770', 2, 14, NULL, 2, '8', '2023', '2023-08-14', NULL, 'normal', 58.209999999999994),
(174, 5, 'withdraw', '2023-08-14 18:29:28.566866', '2023-08-14 18:29:28.566896', 3, 5, NULL, 2, '8', '2023', '2023-08-14', NULL, 'normal', 53.209999999999994),
(175, 0.5, 'withdraw', '2023-08-14 18:30:10.845699', '2023-08-14 18:30:10.845737', 3, 10, NULL, 2, '8', '2023', '2023-08-14', NULL, 'normal', 52.709999999999994),
(176, 1, 'withdraw', '2023-08-14 18:30:40.243534', '2023-08-14 18:30:40.243562', 3, 9, NULL, 2, '8', '2023', '2023-08-14', NULL, 'normal', 51.709999999999994),
(177, 0.5, 'withdraw', '2023-08-15 17:48:41.714790', '2023-08-15 17:48:41.714840', 3, 7, NULL, 2, '8', '2023', '2023-08-15', NULL, 'normal', 51.209999999999994),
(178, 10, 'withdraw', '2023-08-15 17:49:03.086364', '2023-08-15 17:49:03.086446', 1, NULL, NULL, 2, '8', '2023', '2023-08-15', 3, 'transfer', 41.209999999999994),
(180, 5.95, 'withdraw', '2023-08-15 17:49:40.744638', '2023-08-15 17:49:40.744685', 3, 10, NULL, 2, '8', '2023', '2023-08-15', NULL, 'normal', 45.25999999999999),
(181, 2, 'withdraw', '2023-08-15 17:49:57.504128', '2023-08-15 17:49:57.504164', 3, 10, NULL, 2, '8', '2023', '2023-08-15', NULL, 'normal', 43.25999999999999),
(182, 1, 'withdraw', '2023-08-15 17:50:15.766379', '2023-08-15 17:50:15.766434', 3, 16, NULL, 2, '8', '2023', '2023-08-15', NULL, 'normal', 42.25999999999999),
(183, 0.5, 'withdraw', '2023-08-19 03:06:49.641556', '2023-08-19 03:06:49.641629', 3, 5, NULL, 2, '8', '2023', '2023-08-16', NULL, 'normal', 41.75999999999999),
(184, 0.5, 'withdraw', '2023-08-19 03:07:41.702832', '2023-08-19 03:07:41.702879', 3, 7, NULL, 2, '8', '2023', '2023-08-17', NULL, 'normal', 40.00999999999999),
(185, 0.75, 'withdraw', '2023-08-19 03:09:15.481094', '2023-08-19 03:09:15.481148', 3, 10, NULL, 2, '8', '2023', '2023-08-17', NULL, 'normal', 39.25999999999999),
(186, 10, 'withdraw', '2023-08-19 03:09:51.043524', '2023-08-19 03:09:51.043594', 1, NULL, NULL, 2, '8', '2023', '2023-08-19', 3, 'transfer', 29.25999999999999),
(188, 3, 'withdraw', '2023-08-19 03:13:04.406190', '2023-08-19 03:13:04.406238', 3, 10, NULL, 2, '8', '2023', '2023-08-17', NULL, 'normal', 36.25999999999999),
(189, 1, 'withdraw', '2023-08-19 03:13:33.388326', '2023-08-19 03:13:33.388412', 3, 16, NULL, 2, '8', '2023', '2023-08-17', NULL, 'normal', 35.25999999999999),
(190, 1, 'withdraw', '2023-08-19 03:14:22.846193', '2023-08-19 03:14:22.846226', 3, 10, NULL, 2, '8', '2023', '2023-08-18', NULL, 'normal', 34.25999999999999),
(191, 20, 'withdraw', '2023-08-19 03:14:48.041865', '2023-08-19 03:14:48.041914', 1, NULL, NULL, 2, '8', '2023', '2023-08-18', 3, 'transfer', 14.259999999999991),
(193, 15, 'withdraw', '2023-08-19 03:15:39.088840', '2023-08-19 03:15:39.088885', 3, NULL, 3, 2, '8', '2023', '2023-08-18', NULL, 'give_dept', 19.25999999999999),
(194, 1, 'withdraw', '2023-08-19 03:16:06.467586', '2023-08-19 03:16:06.467614', 3, 10, NULL, 2, '8', '2023', '2023-08-18', NULL, 'normal', 18.25999999999999),
(195, 4.64, 'withdraw', '2023-08-19 03:16:37.254141', '2023-08-19 03:16:37.254220', 3, 9, NULL, 2, '8', '2023', '2023-08-18', NULL, 'normal', 13.61999999999999),
(196, 0.5, 'withdraw', '2023-08-19 03:17:09.718112', '2023-08-19 03:17:09.718145', 3, 10, NULL, 2, '8', '2023', '2023-08-18', NULL, 'normal', 13.11999999999999),
(197, 3, 'withdraw', '2023-08-20 16:27:37.625888', '2023-08-20 16:27:37.625939', 3, 10, NULL, 2, '8', '2023', '2023-08-19', NULL, 'normal', 10.11999999999999),
(198, 1, 'withdraw', '2023-08-20 16:28:07.652364', '2023-08-20 16:28:07.652401', 3, 16, NULL, 2, '8', '2023', '2023-08-19', NULL, 'normal', 9.11999999999999),
(199, 0.5, 'withdraw', '2023-08-20 16:28:35.948236', '2023-08-20 16:28:35.948294', 3, 7, NULL, 2, '8', '2023', '2023-08-20', NULL, 'normal', 8.61999999999999),
(200, 5, 'withdraw', '2023-08-20 18:01:00.021814', '2023-08-20 18:01:00.021847', 1, NULL, NULL, 2, '8', '2023', '2023-08-20', 3, 'transfer', 3.6199999999999903),
(202, 5, 'withdraw', '2023-08-20 18:01:17.048303', '2023-08-20 18:01:17.048342', 3, 10, NULL, 2, '8', '2023', '2023-08-20', NULL, 'normal', 3.6199999999999903),
(203, 13, 'withdraw', '2023-08-21 15:52:59.647618', '2023-08-21 15:52:59.647653', 5, NULL, NULL, 2, '8', '2023', '2023-08-21', 3, 'transfer', -9.38000000000001),
(205, 0.14, 'withdraw', '2023-08-21 15:56:06.903878', '2023-08-21 15:56:06.903904', 3, 18, NULL, 2, '8', '2023', '2023-08-21', NULL, 'normal', 3.47999999999999),
(206, 1, 'withdraw', '2023-08-22 04:07:42.690259', '2023-08-22 04:07:42.690297', 3, 16, NULL, 2, '8', '2023', '2023-08-21', NULL, 'normal', 2.47999999999999),
(207, 1, 'withdraw', '2023-08-22 13:51:47.987981', '2023-08-22 13:51:47.988015', 3, 16, NULL, 2, '8', '2023', '2023-08-22', NULL, 'normal', 1.4799999999999902),
(208, 3, 'withdraw', '2023-08-22 16:26:45.675265', '2023-08-22 16:26:45.675320', 3, 7, NULL, 2, '8', '2023', '2023-08-22', NULL, 'normal', -1.5200000000000098),
(209, 4, 'withdraw', '2023-08-24 17:31:44.214478', '2023-08-24 17:31:44.214519', 3, 7, NULL, 2, '8', '2023', '2023-08-22', NULL, 'normal', -5.52000000000001),
(210, 1, 'withdraw', '2023-08-24 17:32:25.819319', '2023-08-24 17:32:25.819337', 3, 16, NULL, 2, '8', '2023', '2023-08-23', NULL, 'normal', -6.52000000000001),
(211, 3, 'withdraw', '2023-08-24 17:33:21.928986', '2023-08-24 17:33:21.929047', 3, 7, NULL, 2, '8', '2023', '2023-08-24', NULL, 'normal', -9.52000000000001),
(212, 0.23, 'withdraw', '2023-08-25 19:57:29.307736', '2023-08-25 19:57:29.307768', 3, 10, NULL, 2, '8', '2023', '2023-08-25', NULL, 'normal', -9.75000000000001),
(213, 712.5, 'deposit', '2023-08-28 19:53:24.109636', '2023-08-28 19:53:24.109685', 1, 1, NULL, 2, '8', '2023', '2023-08-27', NULL, 'normal', 702.75),
(214, 20, 'withdraw', '2023-08-28 19:57:46.339833', '2023-08-28 19:57:46.339865', 1, NULL, NULL, 2, '8', '2023', '2023-08-27', 3, 'transfer', 682.75),
(216, 20, 'withdraw', '2023-08-28 19:58:09.937773', '2023-08-28 19:58:09.937792', 3, 9, NULL, 2, '8', '2023', '2023-08-27', NULL, 'normal', 682.75),
(217, 10, 'withdraw', '2023-08-28 19:58:59.679751', '2023-08-28 19:58:59.679779', 1, NULL, NULL, 2, '8', '2023', '2023-08-28', 3, 'transfer', 672.75),
(219, 4, 'withdraw', '2023-08-28 19:59:23.289429', '2023-08-28 19:59:23.289501', 3, 14, NULL, 2, '8', '2023', '2023-08-28', NULL, 'normal', 678.75),
(220, 2, 'withdraw', '2023-08-28 19:59:57.666947', '2023-08-28 19:59:57.667029', 3, 7, NULL, 2, '8', '2023', '2023-08-28', NULL, 'normal', 676.75),
(221, 10, 'withdraw', '2023-08-28 20:00:14.613881', '2023-08-28 20:00:14.613934', 1, NULL, NULL, 2, '8', '2023', '2023-08-28', 3, 'transfer', 666.75),
(223, 7, 'withdraw', '2023-08-28 20:00:39.864434', '2023-08-28 20:00:39.864463', 3, 9, NULL, 2, '8', '2023', '2023-08-28', NULL, 'normal', 669.75),
(224, 30, 'withdraw', '2023-08-28 20:01:03.327171', '2023-08-28 20:01:03.327205', 1, NULL, NULL, 2, '8', '2023', '2023-08-28', 3, 'transfer', 639.75),
(226, 22, 'withdraw', '2023-08-28 20:01:19.131387', '2023-08-28 20:01:19.131429', 3, 17, NULL, 2, '8', '2023', '2023-08-28', NULL, 'normal', 647.75),
(227, 11, 'withdraw', '2023-08-28 20:01:41.789321', '2023-08-28 20:01:41.789358', 3, 9, NULL, 2, '8', '2023', '2023-08-28', NULL, 'normal', 636.75),
(228, 0.5, 'withdraw', '2023-08-28 20:02:08.327122', '2023-08-28 20:02:08.327178', 3, 14, NULL, 2, '8', '2023', '2023-08-28', NULL, 'normal', 636.25),
(229, 3, 'withdraw', '2023-08-28 20:02:27.244298', '2023-08-28 20:02:27.244390', 3, 10, NULL, 2, '8', '2023', '2023-08-28', NULL, 'normal', 633.25),
(230, 10, 'withdraw', '2023-08-28 20:02:39.426831', '2023-08-28 20:02:39.426855', 1, NULL, NULL, 2, '8', '2023', '2023-08-28', 3, 'transfer', 623.25),
(232, 3, 'withdraw', '2023-08-28 20:02:59.858198', '2023-08-28 20:02:59.858241', 3, 9, NULL, 2, '8', '2023', '2023-08-28', NULL, 'normal', 630.25),
(233, 10, 'withdraw', '2023-08-29 14:22:43.384258', '2023-08-29 14:22:43.384323', 1, NULL, NULL, 2, '8', '2023', '2023-08-29', 3, 'transfer', 620.25),
(235, 10, 'withdraw', '2023-08-29 14:22:59.314674', '2023-08-29 14:22:59.314728', 3, 14, NULL, 2, '8', '2023', '2023-08-29', NULL, 'normal', 620.25),
(236, 25, 'deposit', '2023-08-30 18:19:41.318058', '2023-08-30 18:19:41.318079', 2, 19, NULL, 2, '8', '2023', '2023-08-29', NULL, 'normal', 645.25),
(237, 0.5, 'withdraw', '2023-08-30 18:20:02.603198', '2023-08-30 18:20:02.603279', 2, 5, NULL, 2, '8', '2023', '2023-08-30', NULL, 'normal', 644.75),
(238, 1, 'withdraw', '2023-08-30 18:20:27.145601', '2023-08-30 18:20:27.145656', 2, 14, NULL, 2, '8', '2023', '2023-08-30', NULL, 'normal', 643.75),
(239, 25, 'deposit', '2023-08-30 18:20:43.235554', '2023-08-30 18:20:43.235625', 2, 19, NULL, 2, '8', '2023', '2023-08-30', NULL, 'normal', 668.75),
(240, 3, 'withdraw', '2023-08-30 18:22:02.827335', '2023-08-30 18:22:02.827388', 2, 20, NULL, 2, '8', '2023', '2023-08-30', NULL, 'normal', 665.75),
(241, 4, 'withdraw', '2023-08-30 18:22:18.069981', '2023-08-30 18:22:18.070025', 2, 10, NULL, 2, '8', '2023', '2023-08-30', NULL, 'normal', 661.75),
(242, 130, 'withdraw', '2023-08-30 18:55:47.489609', '2023-08-30 18:55:47.489807', 1, 9, NULL, 2, '8', '2023', '2023-08-30', NULL, 'normal', 531.75),
(243, 23, 'withdraw', '2023-08-30 18:56:35.441159', '2023-08-30 18:56:35.441215', 1, 21, NULL, 2, '8', '2023', '2023-08-30', NULL, 'normal', 508.75),
(244, 100, 'withdraw', '2023-08-30 19:05:23.716341', '2023-08-30 19:05:23.716426', 1, NULL, NULL, 2, '8', '2023', '2023-08-30', 4, 'transfer', 408.75),
(246, 100, 'withdraw', '2023-08-30 19:08:10.878118', '2023-08-30 19:08:10.878205', 3, 22, NULL, 2, '8', '2023', '2023-08-30', NULL, 'normal', 408.75),
(247, 1, 'withdraw', '2023-08-31 19:50:12.914596', '2023-08-31 19:50:12.914634', 2, 5, NULL, 2, '8', '2023', '2023-08-31', NULL, 'normal', 407.75),
(248, 1, 'withdraw', '2023-08-31 19:51:32.781852', '2023-08-31 19:51:32.781887', 2, 14, NULL, 2, '8', '2023', '2023-08-31', NULL, 'normal', 406.75),
(249, 0.5, 'withdraw', '2023-08-31 19:51:49.560836', '2023-08-31 19:51:49.560898', 2, 7, NULL, 2, '8', '2023', '2023-08-31', NULL, 'normal', 406.25),
(250, 1, 'withdraw', '2023-08-31 19:52:08.825827', '2023-08-31 19:52:08.825888', 2, 7, NULL, 2, '8', '2023', '2023-08-31', NULL, 'normal', 405.25),
(251, 1, 'withdraw', '2023-08-31 19:52:25.625291', '2023-08-31 19:52:25.625332', 2, 7, NULL, 2, '8', '2023', '2023-08-31', NULL, 'normal', 404.25),
(252, 1.5, 'withdraw', '2023-08-31 19:52:45.996828', '2023-08-31 19:52:45.996878', 2, 7, NULL, 2, '8', '2023', '2023-08-31', NULL, 'normal', 402.75),
(253, 0.5, 'withdraw', '2023-08-31 19:53:16.230322', '2023-08-31 19:53:16.230378', 2, 16, NULL, 2, '8', '2023', '2023-08-31', NULL, 'normal', 402.25),
(254, 1, 'withdraw', '2023-08-31 19:53:38.734404', '2023-08-31 19:53:38.734512', 2, 7, NULL, 2, '8', '2023', '2023-08-31', NULL, 'normal', 401.25),
(255, 0.5, 'withdraw', '2023-08-31 19:54:03.535706', '2023-08-31 19:54:03.535767', 2, 7, NULL, 2, '8', '2023', '2023-08-31', NULL, 'normal', 400.75),
(256, 3, 'withdraw', '2023-08-31 19:54:22.276347', '2023-08-31 19:54:22.276599', 2, 10, NULL, 2, '8', '2023', '2023-08-31', NULL, 'normal', 397.75),
(257, 1.5, 'withdraw', '2023-08-31 19:54:37.858374', '2023-08-31 19:54:37.858424', 2, 7, NULL, 2, '8', '2023', '2023-08-31', NULL, 'normal', 396.25),
(258, 3, 'withdraw', '2023-09-01 15:00:05.524438', '2023-09-01 15:00:05.524485', 2, 10, NULL, 2, '9', '2023', '2023-09-01', NULL, 'normal', 393.25),
(259, 0.5, 'withdraw', '2023-09-01 15:00:24.323957', '2023-09-01 15:00:24.324021', 2, 10, NULL, 2, '9', '2023', '2023-09-01', NULL, 'normal', 392.75),
(260, 0.5, 'withdraw', '2023-09-03 07:08:03.427952', '2023-09-03 07:08:03.428084', 2, 7, NULL, 2, '9', '2023', '2023-09-03', NULL, 'normal', 392.25);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `email` varchar(254) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `is_client` tinyint(1) NOT NULL,
  `modified_at` datetime(6) DEFAULT NULL,
  `is_payment_valid` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `is_staff`, `is_active`, `date_joined`, `email`, `phone`, `gender`, `avatar`, `is_client`, `modified_at`, `is_payment_valid`) VALUES
(1, 'pbkdf2_sha256$600000$DDb8hoKfWEXlKJ6XqQB2yQ$ec3BmLXa8zP0FJSM0h3klR/KkDD5Ci93S9nNzs5v1pk=', '2023-07-28 09:31:01.633945', 1, 'WT0001', 'WealthTrack', 'Support', 1, 1, '2023-07-28 09:29:26.285192', 'wealth@support.com', '', NULL, '', 0, '2023-07-28 09:29:26.425986', 0),
(2, 'pbkdf2_sha256$600000$UWNblIkAo2LbQkWRIg93I3$/PJZ+V+eI+KjPhJ6+wFKmjeWTK8aqNKG65ZbujFAiwQ=', '2023-09-01 14:59:47.718295', 0, 'WT0002', 'Mansuur Abdullahi', 'Abdirahman', 0, 1, '2023-07-28 09:34:08.022823', 'mansuurtech101@gmail.com', '613992210', 'Male', 'profile-images/WT0002-profile.jpeg', 1, '2023-08-14 18:42:51.803099', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` bigint(20) NOT NULL,
  `users_id` bigint(20) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_user_permissions`
--

CREATE TABLE `users_user_permissions` (
  `id` bigint(20) NOT NULL,
  `users_id` bigint(20) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accounts_user_id_7f1e1f1e_fk_users_id` (`user_id`);

--
-- Indexes for table `audittrials`
--
ALTER TABLE `audittrials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `audittrials_user_id_ef677a04_fk_users_id` (`user_id`);

--
-- Indexes for table `authtoken_token`
--
ALTER TABLE `authtoken_token`
  ADD PRIMARY KEY (`key`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `finance_category_user_id_be013ba3_fk_users_id` (`user_id`);

--
-- Indexes for table `deptors`
--
ALTER TABLE `deptors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deptors_user_id_6ba04d97_fk_users_id` (`user_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_users_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `errorlogs`
--
ALTER TABLE `errorlogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `errorlogs_user_id_00c8da84_fk_users_id` (`user_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_user_id_189b9948_fk_users_id` (`user_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_user_id_766cc893_fk_users_id` (`user_id`),
  ADD KEY `transactions_account_id_d92b47af_fk_accounts_id` (`account_id`),
  ADD KEY `transactions_category_id_65740af9_fk_category_id` (`category_id`),
  ADD KEY `transactions_deptors_id_7d2a32ec_fk_deptors_id` (`deptors_id`),
  ADD KEY `transactions_source_account_id_52b823bd_fk_accounts_id` (`source_account_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_groups_users_id_group_id_83a49e68_uniq` (`users_id`,`group_id`),
  ADD KEY `users_groups_group_id_2f3517aa_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `users_user_permissions`
--
ALTER TABLE `users_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_user_permissions_users_id_permission_id_d7a00931_uniq` (`users_id`,`permission_id`),
  ADD KEY `users_user_permissio_permission_id_6d08dcd2_fk_auth_perm` (`permission_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `audittrials`
--
ALTER TABLE `audittrials`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=286;

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `deptors`
--
ALTER TABLE `deptors`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `errorlogs`
--
ALTER TABLE `errorlogs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_user_permissions`
--
ALTER TABLE `users_user_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_user_id_7f1e1f1e_fk_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `audittrials`
--
ALTER TABLE `audittrials`
  ADD CONSTRAINT `audittrials_user_id_ef677a04_fk_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `authtoken_token`
--
ALTER TABLE `authtoken_token`
  ADD CONSTRAINT `authtoken_token_user_id_35299eff_fk_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `finance_category_user_id_be013ba3_fk_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `deptors`
--
ALTER TABLE `deptors`
  ADD CONSTRAINT `deptors_user_id_6ba04d97_fk_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `errorlogs`
--
ALTER TABLE `errorlogs`
  ADD CONSTRAINT `errorlogs_user_id_00c8da84_fk_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_user_id_189b9948_fk_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_account_id_d92b47af_fk_accounts_id` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  ADD CONSTRAINT `transactions_category_id_65740af9_fk_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `transactions_deptors_id_7d2a32ec_fk_deptors_id` FOREIGN KEY (`deptors_id`) REFERENCES `deptors` (`id`),
  ADD CONSTRAINT `transactions_source_account_id_52b823bd_fk_accounts_id` FOREIGN KEY (`source_account_id`) REFERENCES `accounts` (`id`),
  ADD CONSTRAINT `transactions_user_id_766cc893_fk_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `users_groups_group_id_2f3517aa_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `users_groups_users_id_1e682706_fk_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users_user_permissions`
--
ALTER TABLE `users_user_permissions`
  ADD CONSTRAINT `users_user_permissio_permission_id_6d08dcd2_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `users_user_permissions_users_id_e1ed60a2_fk_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
