from datetime import datetime, timedelta

def calculate_time_difference(start_datetime, end_datetime):
    time_difference = end_datetime - start_datetime

    # If the time difference is less than a minute
    if time_difference < timedelta(minutes=1):
        return f"{time_difference.seconds} seconds ago"

    # If the time difference is less than an hour
    elif time_difference < timedelta(hours=1):
        return f"{time_difference.seconds // 60} minutes ago"

    # If the time difference is less than a day
    elif time_difference < timedelta(days=1):
        return f"{time_difference.seconds // 3600} hours ago"

    # If the time difference is less than a month
    elif time_difference.days < 30:
        return f"{time_difference.days} days ago"

    # If the time difference is less than a year
    elif time_difference.days < 365:
        months = time_difference.days // 30
        return f"{months} months ago"

    # If the time difference is more than a year
    else:
        years = time_difference.days // 365
        return f"{years} years ago"
    
start = datetime(2022, 1, 1, 12, 0, 0)
end = datetime(2024, 3, 1, 12, 0, 0)
difference = calculate_time_difference(start, end)
print(difference)  # Output: 8 months ago