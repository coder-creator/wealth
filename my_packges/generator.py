# Function to generate username with some options
from datetime import date, datetime
import locale
import re
from django.core.exceptions import ValidationError
from email_validator import validate_email, EmailNotValidError
current_date = date.today()


def get_month(value):
    months = {
        1: 'January',
        2: 'February',
        3: 'March',
        4: 'April',
        5: 'May',
        6: 'June',
        7: 'July',
        8: 'August',
        9: 'September',
        10: 'October',
        11: 'November',
        12: 'December'
    }

    if isinstance(value, int):
        return months[value]
    else:
        for month_num, month_name in months.items():
            if value.lower() in month_name.lower():
                return month_num


def remove_item_from_dict(dict, items=[]):
    for item in items:
        try:
            del dict[item]
        except KeyError:
            pass

    return dict


def convert_string_date(string):
    return datetime.strptime(string, '%Y-%m-%d').date()


def generate_username(last_number=None, letter=None, digits=4, with_letter=False):
    if last_number is None:
        last_number = 0

    # Increment the number
    last_number += 1

    # `Check if the digits is not full
    # Then fill with zeros
    last_number_length = len(str(last_number))
    if last_number_length < digits:
        digits_remaining = digits - last_number_length

        new_last_number = ''
        for x in range(0, digits_remaining):
            new_last_number += '0'

    # Update the last_number
    day = current_date.strftime('%d')
    month = current_date.strftime('%m')
    year = current_date.strftime('%Y')
    last_number = f"{day}{month}{year}{new_last_number}{last_number}"

    # çheck if the letetrs are allowes and the letter is not None
    if with_letter and letter is not None:
        return f"{letter}{last_number}"

    return last_number

# Function to validate inputs/data


def validate_input(input):
    if input is None or input == '' or input == 'null' or input == 'undefined':
        return False

    return True


# Function to validate email
def validate_email_address(email):
    # Regular expression pattern for email validation
    pattern = r'^[\w\.-]+@[\w\.-]+\.\w+$'

    # Check if the email matches the pattern
    if re.match(pattern, email):
        return True
    else:
        return False


class Messages:
    def validation_message(message, input=None):
        return {
            'title': "Validation Error",
            'icon': "warning",
            'message': message,
            'isError': True,
            'input': input
        }

    def unauthenticated_message(message, input=None):
        return {
            'title': "Unauthenticated Error",
            'icon': "warning",
            'message': message,
            'isError': True,
            'input': input
        }

    def exception_message(message, input=None):
        return {
            'title': "Exception Error",
            'icon': "error",
            'message': message,
            'isError': True,
            'input': input
        }

    def success_message(message, input=None):
        return {
            'title': "Congratulations",
            'icon': "success",
            'message': message,
            'isError': False,
            'input': input
        }

    def unrecognized_message(message="You don't have permission to perform this action", input=None):
        return {
            'title': "Unrecognized",
            'icon': "warning",
            'message': message,
            'isError': True,
            'input': input
        }


def format_currency(amount):
    # Format the amount as currency
    formatted_amount = "${:,.2f}".format(amount)

    if formatted_amount.endswith(".00"):
        formatted_amount = formatted_amount[:-3]

    return formatted_amount
