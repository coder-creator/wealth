import random

kNumbers = '0123456789'
kLetters = 'abcdefghijklmnopqrstuvwxyz'
kCapitals = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
kSymbols = '&%$@*'


def GeneratePassword(length=6, letters=True, capitals=False, numbers=True, symbols=False):
    # First combine all these letters, numbers, symbols and capital letters
    # based on the credentials
    combined_letters = ''

    if letters == True:
        combined_letters += kLetters

    if capitals == True:
        combined_letters += kCapitals

    if numbers == True:
        combined_letters += kNumbers

    if symbols == True:
        combined_letters += kSymbols

    # Check if there is something to generate
    if combined_letters == '':
        return "Please enter your specifications to generate your password"

    # Now we will shuffle the string
    temp = list(combined_letters)
    random.shuffle(temp)
    combined_letters = ''.join(temp)

    random_password = "".join(random.choice(combined_letters)
                              for _ in range(length))

    return random_password
