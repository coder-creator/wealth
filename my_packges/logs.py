from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
import smtplib
import ssl
from typing import List
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
import httpagentparser
import traceback
import sys
from django.core.mail import send_mail
from django.conf import settings
from accounts.models import ErrorLogs, AuditTrials
from functools import wraps
from datetime import datetime
from django.conf import settings


# Import m custom packages
from my_packges.generator import Messages


def remove_non_ascii_2(string):
    return string.encode('ascii', errors='ignore').decode()


def validate_image(image) -> JsonResponse:
    """
    Validates the extension and size of an image file.

    Args:
        image: The image file to be validated.

    Returns:
        A JsonResponse with a validation message if the file does not meet the requirements.
    """
    extension = image.name.split(".")[-1].lower()
    extension_types: List[str] = ['jpg', 'jpeg', 'png']
    supported_extensions = ", ".join(extension_types)

    if extension not in extension_types:
        return JsonResponse(Messages.validation_message(f"Image we support is {supported_extensions}"))

    if image.size > 2621440:
        return JsonResponse(Messages.validation_message("Image's size must be less than or equal to 2 megabytes (MB)"))


def sendException(request, error, is_web=True):
    user = None
    if request.user.is_authenticated:
        user = request.user

    ip = request.META.get('REMOTE_ADDR')
    get_client_agent = request.META['HTTP_USER_AGENT']

    if is_web:
        detect_os = httpagentparser.detect(
            get_client_agent)['os']['name']
        browser = httpagentparser.detect(get_client_agent)[
            'browser']['name']
    else:
        detect_os = request.data['os']
        browser = request.data['browser']

    trace_err = traceback.format_exc()
    Expected_error = str(sys.exc_info()[0])
    field_error = str(sys.exc_info()[1])
    line_number = str(sys.exc_info()[-1].tb_lineno)
    error_logs = ErrorLogs(
        user=user,
        ip_address=ip,
        browser=browser,
        expected_error=Expected_error,
        field_error=field_error,
        trace_back=str(trace_err),
        line_number=line_number,
        os=detect_os,
    )

    error_logs.save()


def sendTrials(request, action, module, is_web=True):
    user = None

    if request.user.is_authenticated:
        user = request.user

    ip = request.META.get('REMOTE_ADDR')
    get_client_agent = request.META['HTTP_USER_AGENT']

    if is_web:
        detect_os = httpagentparser.detect(
            get_client_agent)['os']['name']
        browser = httpagentparser.detect(get_client_agent)[
            'browser']['name']
    else:
        detect_os = request.data['os']
        browser = request.data['browser']

    action = action
    module = module

    audit_trails = AuditTrials(
        user=user,
        Actions=action,
        path=request.path,
        Module=module,
        operating_system=detect_os,
        ip_address=ip,
        browser=browser,
    )

    audit_trails.save()

    return {
        'title': "Audit Trials Saved Successfully!!",
    }


def PreviewDate(date_string, is_datetime, add_time=True):
    if is_datetime:
        new_date = date_string
        if add_time:
            date_string = new_date.strftime("%a") + ', ' + new_date.strftime(
                "%b") + ' ' + str(new_date.day) + ', ' + str(new_date.year) + '  ' + new_date.strftime("%I") + ':' + new_date.strftime("%M") + ':' + new_date.strftime("%S") + ' ' + new_date.strftime("%p")
        else:
            date_string = new_date.strftime("%a") + ', ' + new_date.strftime(
                "%b") + ' ' + str(new_date.day) + ', ' + str(new_date.year)
    else:
        date_string = str(date_string)
        date_string = date_string.split('-')

        new_date = datetime(int(date_string[0]), int(
            date_string[1]), int(date_string[2]))

        date_string = new_date.strftime("%a") + ', ' + new_date.strftime(
            "%b") + ' ' + str(new_date.day) + ', ' + str(new_date.year)

    return date_string

    username = username
    Name = name
    ip = request.META.get('REMOTE_ADDR')
    get_client_agent = request.META['HTTP_USER_AGENT']
    try:
        detect_os = httpagentparser.detect(
            get_client_agent)['os']['name']
    except KeyError:
        detect_os = get_client_agent
    try:
        browser = httpagentparser.detect(get_client_agent)[
            'browser']['name']
    except KeyError:
        browser = get_client_agent
    trace_err = traceback.format_exc()
    Expected_error = str(sys.exc_info()[0])
    field_error = str(sys.exc_info()[1])
    line_number = str(sys.exc_info()[-1].tb_lineno)
    user_agent = str(ip) + ","
    user_agent += str(detect_os) + ',' if brand == '' else brand + ','
    user_agent += browser if model == '' else model
    error_logs = models.ErrorLogs(
        Avatar=str(request.user.avatar) if avatar == '' else avatar,
        Name=Name,
        Username=username,
        ip_address=ip,
        browser=browser if model == '' else model,
        Expected_error=Expected_error,
        field_error=field_error,
        trace_back=str(trace_err),
        line_number=line_number,
        user_agent=user_agent)

    error_logs.save()

    return {
        'error': str(error),
        'isError': True,
        'title': "An error occurred please contact us"
    }


def SendEmail(email_subject, email_message, email_reciever):
    sender_email = settings.EMAIL_HOST_USER
    receiver_email = email_reciever
    password = settings.EMAIL_HOST_PASSWORD

    message = MIMEMultipart("alternative")
    message["Subject"] = email_subject
    message["From"] = sender_email
    message["To"] = receiver_email

    # Create the plain-text and HTML version of your message
    text = email_message

    # Turn these into plain/html MIMEText objects
    part1 = MIMEText(text, "plain")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part1)

    try:
        # Create secure connection with server and send email
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(
                sender_email, receiver_email, message.as_string()
            )
    except Exception as error:
        print('Exception Occured', error)


def handle_exceptions(is_view=True, is_web=True):
    def decorator(view_func):
        @wraps(view_func)
        def wrapper(request, *args, **kwargs):
            try:
                return view_func(request, *args, **kwargs)
            except Exception as error:
                # if is view is False
                # then return a message with jsonResponse
                # if else return a error page

                sendException(request, error, is_web)

                if is_view:
                    return render(request, 'errors/500.html', {"message": error})
                else:
                    return JsonResponse(Messages.exception_message('Oops! Something went wrong. Please try again later or contact support at support@example.com for assistance.'))

        return wrapper
    return decorator


def handle_auth():
    def decorator(view_func):
        @wraps(view_func)
        def wrapper(request, *args, **kwargs):
            if request.user.is_authenticated:
                return view_func(request, *args, **kwargs)
            else:
                return JsonResponse(
                    Messages.unauthenticated_message(
                        'Your session expired, please refersh the page to login again or Logout the user manually')
                )

        return wrapper
    return decorator
