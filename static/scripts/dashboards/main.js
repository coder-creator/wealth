$(document).ready(function () {
  getTransactionYears();
  const transactionYears = $("#transaction_years").val();
  if (transactionYears) {
    getTransactionMonths(transactionYears);
  }

  const transactionMonths = $("#transaction_months").val();

  if (transactionYears && transactionMonths) {
    getTransactionChart(transactionYears, transactionMonths);
    getExpenseChart(transactionYears, transactionMonths);
    getIncomeChart(transactionYears, transactionMonths);
  }

  function getTransactionYears() {
    let formData = new FormData();
    $.ajax({
      method: "POST",
      url: "/manage_dashboards/TransactionYears",
      headers: { "X-CSRFToken": csrftoken },
      processData: false,
      contentType: false,
      async: false,
      success: function (response) {
        $("#transaction_years, #expense_years , #income_years").html("");

        response.message.forEach((item) => {
          const option = `<option value='${item}'>${item}</option>`;
          $("#transaction_years, #expense_years , #income_years").append(option);
        });
      },
      error: function (response) {},
    });
  }

  function getTransactionMonths(year) {
    let formData = new FormData();
    formData.append("year", year);
    $.ajax({
      method: "POST",
      url: "/manage_dashboards/TransactionMonths",
      headers: { "X-CSRFToken": csrftoken },
      processData: false,
      contentType: false,
      data: formData,
      async: false,
      success: function (response) {
        $("#transaction_months, #expense_months , #income_months").html("");

        response.message.forEach((item, index) => {
          if (index === 0) {
            $("#transaction_months, #expense_months , #income_months").append(
              `<option value='all'>All months</option>`
            );
          }

          $("#transaction_months, #expense_months , #income_months").append(
            `<option value='${item.month}'>${item.name}</option>`
          );
        });
      },
      error: function (response) {},
    });
  }

  function getTransactionChart(year, month) {
    let formData = new FormData();
    formData.append("year", year);
    formData.append("month", month);
    $.ajax({
      method: "POST",
      url: "/manage_dashboards/TransactionChart",
      headers: { "X-CSRFToken": csrftoken },
      processData: false,
      contentType: false,
      data: formData,
      async: false,
      success: function (response) {
        drawTransactionChart(response.message);
      },
      error: function (response) {},
    });
  }

  function getExpenseChart(year, month) {
    let formData = new FormData();
    formData.append("year", year);
    formData.append("month", month);
    $.ajax({
      method: "POST",
      url: "/manage_dashboards/ExpenseChart",
      headers: { "X-CSRFToken": csrftoken },
      processData: false,
      contentType: false,
      data: formData,
      async: false,
      success: function (response) {
        drawExpenseChart(response.message);
      },
      error: function (response) {},
    });
  }

  function getIncomeChart(year, month) {
    let formData = new FormData();
    formData.append("year", year);
    formData.append("month", month);
    $.ajax({
      method: "POST",
      url: "/manage_dashboards/IncomeChart",
      headers: { "X-CSRFToken": csrftoken },
      processData: false,
      contentType: false,
      data: formData,
      async: false,
      success: function (response) {
        drawIncomeChart(response.message);
      },
      error: function (response) {},
    });
  }


  $("#transaction_years , #expense_years").on("change", function () {
    var selectedYear = $(this).val();

    if (!["", undefined, null].includes(selectedYear)) {
      getTransactionMonths(selectedYear);
    }
  });

  $("#transaction_months").on("change", function () {
    var selectedYear = $("#transaction_years").val();
    var selectedMonth = $(this).val();
    getTransactionChart(selectedYear, selectedMonth);
  });

  $("#expense_months").on("change", function () {
    var selectedYear = $("#expense_years").val();
    var selectedMonth = $(this).val();
    getExpenseChart(selectedYear, selectedMonth);
  });

  $("#income_months").on("change", function () {
    var selectedYear = $("#income_years").val();
    var selectedMonth = $(this).val();
    getIncomeChart(selectedYear, selectedMonth);
  });

  function drawTransactionChart(message) {
    $(".transaction_chart").html(
      `<div id="transaction_chart" class="e-charts w-100"></div>`
    );
    let data = [];
    let labels = [];
    let colors = [];

    for (const index in message) {
      data.push(message[index]);
      labels.push(index);
      colors.push(generateRandomHexColor());
    }
    let option = {
      grid: {
        left: "0%",
        right: "0%",
        bottom: "0%",
        top: "4%",
        containLabel: !0,
      },
      xAxis: {
        type: "category",
        data: labels,
        axisLine: { lineStyle: { color: "#858d98" } },
      },
      yAxis: {
        type: "value",
        axisLine: { lineStyle: { color: "#858d98" } },
        splitLine: { lineStyle: { color: "rgba(133, 141, 152, 0.1)" } },
      },
      series: [{ data: data, type: "line" }],
      textStyle: { fontFamily: "Poppins, sans-serif" },
      color: colors,
    };

    let chartDom = document.getElementById("transaction_chart");
    let myChart = echarts.init(chartDom);
    myChart.setOption(option);
  }

  function drawExpenseChart(message) {
    $(".expense_chart").html(
      `<div id="expense_chart" class="e-charts w-100"></div>`
    );
    let data = [];
    let labels = [];
    let colors = [];

    for (let index = 0; index < message.length; index++) {
      colors.push(generateRandomHexColor());
    }
   
    option = {
      tooltip: { trigger: "item" },
      legend: {
        orient: "vertical",
        left: "left",
        textStyle: { color: "#858d98" },
      },
      color: colors,
      series: [
        {
          name: "Expense",
          type: "pie",
          radius: "50%",
          data: message,
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: "rgba(0, 0, 0, 0.5)",
            },
          },
        },
      ],
      textStyle: { fontFamily: "Poppins, sans-serif" },
    };

    let chartDom = document.getElementById("expense_chart");
    let myChart = echarts.init(chartDom);
    myChart.setOption(option);
  }

  function drawIncomeChart(message) {
    $(".income_chart").html(
      `<div id="income_chart" class="e-charts w-100"></div>`
    );
    let data = [];
    let labels = [];
    let colors = [];

    for (let index = 0; index < message.length; index++) {
      colors.push(generateRandomHexColor());
    }
   
    option = {
      tooltip: { trigger: "item" },
      legend: {
        orient: "vertical",
        left: "left",
        textStyle: { color: "#858d98" },
      },
      color: colors,
      series: [
        {
          name: "Income",
          type: "pie",
          radius: "50%",
          data: message,
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: "rgba(0, 0, 0, 0.5)",
            },
          },
        },
      ],
      textStyle: { fontFamily: "Poppins, sans-serif" },
    };

    let chartDom = document.getElementById("income_chart");
    let myChart = echarts.init(chartDom);
    myChart.setOption(option);
  }

  function generateRandomHexColor() {
    // Generate random values for red, green, and blue
    var red = Math.floor(Math.random() * 256);
    var green = Math.floor(Math.random() * 256);
    var blue = Math.floor(Math.random() * 256);

    // Convert values to hexadecimal format
    var hexRed = red.toString(16).padStart(2, "0");
    var hexGreen = green.toString(16).padStart(2, "0");
    var hexBlue = blue.toString(16).padStart(2, "0");

    // Combine hex values to create the color code
    var hexColor = "#" + hexRed + hexGreen + hexBlue;

    return hexColor;
  }
});
