$(document).ready(function () {
  // set deafult dates for expense and income date
  let date = new Date();
  const formattedDate = date.toISOString().substr(0, 10);
  $("#transaction_date").val(formattedDate);

  $("#transaction_type").on("change", function () {
    const transaction_type = $("#transaction_type").val();
    $("#transaction_category").parent().addClass("d-none");
    $("#transaction_deptor").parent().addClass("d-none");
    $("#transaction_source").parent().addClass("d-none");
    $("#to_account").parent().addClass("d-none");

    if (["income", "expense"].includes(transaction_type))
      handleTransactionSource();

    if (transaction_type == "saving") {
      $("#to_account").parent().removeClass("d-none");
      $("#transaction_category").parent().removeClass("d-none");
      getCategries(transaction_type);
    }

    if (transaction_type == "transfer") {
      $("#to_account").parent().removeClass("d-none");
    }

    handleAccountBalance();
  });

  $("#transaction_source").on("change", function () {
    const value = $(this).val();
    $("#transaction_category").parent().addClass("d-none");
    $("#transaction_deptor").parent().addClass("d-none");

    if (value === "category") {
      $("#transaction_category").parent().removeClass("d-none");
      getCategries($("#transaction_type").val());
    } else {
      $("#transaction_deptor").parent().removeClass("d-none");
    }
  });

  $("#transaction_account").on("change", function () {
    if ($(this).val() < 0) {
      $(this).val(0);
    }
    handleAccountBalance();
  });

  $("#transaction_amount").on("input", function (event) {
    if ($(this).val() < 0) {
      $(this).val(0);
    }
    handleAccountBalance();
    event.preventDefault();
  });

  function handleTransactionSource() {
    $("#transaction_source").parent().addClass("d-none");
    $("#transaction_category").parent().addClass("d-none");
    const transaction_type = $("#transaction_type").val();
    const incomes = {
      category: "By category",
      take_dept: "Take a dept",
      return_dept: "Return a dept",
    };

    const expenses = {
      category: "By category",
      give_dept: "Give a dept",
      pay_dept: "Payback a dept",
    };

    if (
      !["", null, undefined].includes(transaction_type) &&
      ["income", "expense"].includes(transaction_type)
    ) {
      $("#transaction_source").parent().removeClass("d-none");
      const temp = transaction_type == "income" ? incomes : expenses;
      $("#transaction_source").html(
        "<option value=''>Select transaction source?</option>"
      );
      for (const item in temp) {
        $("#transaction_source").append(
          `<option value='${item}'>${temp[item]}</option>`
        );
      }
    }
  }

  function getCategries(type) {
    let formData = new FormData();
    formData.append("type", type);

    $.ajax({
      method: "POST",
      url: `/manage_essentials/GetCategories`,
      headers: { "X-CSRFToken": csrftoken },
      processData: false,
      contentType: false,
      data: formData,
      async: true,
      success: function (response) {
        if (!response.isError) {
          $("#transaction_category").html(
            "<option value=''>Select category?</option>"
          );

          response.message.forEach((item) => {
            $("#transaction_category").append(
              `<option value='${item.id}'>${item.name}</option>`
            );
          });
        } else {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          });
        }
      },
      error: function (response) {},
    });
  }

  function handleAccountBalance() {
    $(`.account_alert`).addClass("d-none");

    // check if an account is selected
    if ($(`#transaction_account`).val() != "") {
      $(`.account_alert`).removeClass("d-none");

      const amount = $("option:selected", `#transaction_account`).attr(
        "amount"
      );
      const name = $("option:selected", `#transaction_account`).text();
      let transaction_amount = $(`#transaction_amount`).val();

      transaction_amount = [NaN, undefined, ""].includes(transaction_amount)
        ? 0
        : transaction_amount;

      //   update the alert
      $(`#account_name_show`).text(name);
      $(`#account_balance_show`).text(formatCurrency(amount));
      $(`#transaction_balance`).text(formatCurrency(transaction_amount));

      let new_balance = 0;
      const transaction_type = $("#transaction_type").val();

      if (["expense", "saving", "transfer"].includes(transaction_type))
        new_balance = parseFloat(amount) - parseFloat(transaction_amount);

      if (transaction_type == "income")
        new_balance = parseFloat(amount) + parseFloat(transaction_amount);

      $(`#new_balance`).text(formatCurrency(new_balance));
    }
  }

  function formatCurrency(number, currencySymbol = "$") {
    // Convert the number to a string with two decimal places
    const formattedNumber = Number(number).toFixed(2);

    // Split the number into integer and decimal parts
    const [integerPart, decimalPart] = formattedNumber.split(".");

    // Add commas to the integer part of the number
    const formattedIntegerPart = integerPart.replace(
      /\B(?=(\d{3})+(?!\d))/g,
      ","
    );

    // Combine the formatted integer and decimal parts with the currency symbol
    const formattedAmount =
      currencySymbol + formattedIntegerPart + "." + decimalPart;

    // Return the formatted currency string
    return formattedAmount;
  }

  $("#save_transaction").on("click", function () {
    save_transaction(this);
  });

  // save the transaction
  function save_transaction(button) {
    // Check if the action has already been performed
    // if so, deny any further processing
    button = $(button);
    const loading = button.attr("data-loading");
    if (loading == "true") return;

    let formData = new FormData();
    formData.append("transaction_type", $("#transaction_type").val());
    formData.append("transaction_source", $("#transaction_source").val());
    formData.append("transaction_category", $("#transaction_category").val());
    formData.append("transaction_deptor", $("#transaction_deptor").val());
    formData.append("transaction_account", $("#transaction_account").val());
    formData.append("to_account", $("#to_account").val());
    formData.append("transaction_amount", $("#transaction_amount").val());
    formData.append("transaction_date", $("#transaction_date").val());

    $.ajax({
      method: "POST",
      url: `/manage_essentials/SubmitTransaction`,
      headers: { "X-CSRFToken": csrftoken },
      beforeSend: function () {
        // Make the laoding to true
        button.attr("data-loading", "true");
        button.html(`
              <span class="d-flex justify-content-center align-items-center">
                  <span class="spinner-border flex-shrink-0" role="status">
                      <span class="visually-hidden">Loading...</span>
                  </span>
              </span>
          `);
      },
      processData: false,
      contentType: false,
      data: formData,
      async: true,
      success: function (response) {
        button.attr("data-loading", "false");
        button.html("Submit transaction");
        if (!response.isError) {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          }).then((e) => {
            if (e.value) {
              location.reload();
            }
          });
        } else {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          });
        }
      },
      error: function (response) {
        button.attr("data-loading", "false");
        button.html("Save");
      },
    });
  }
});
