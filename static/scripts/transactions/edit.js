$(document).ready(function () {
  // for the first load

  const income_types = ["normal", "take_dept", "return_dept"];
  const expense_types = ["normal", "give_dept", "pay_dept"];

  if (private__type === "deposit") {
    if (income_types.includes(private__where_to)) {
      private__type = "income";
    } else if (private__where_to === "transfer") {
      private__type = "to";
    }
  } else if (private__type === "withdraw") {
    if (expense_types.includes(private__where_to)) {
      private__type = "expense";
    } else if (private__where_to === "saving") {
      private__type = "saving";
    }
  }

  if (
    income_types.includes(private__where_to) ||
    expense_types.includes(private__where_to)
  ) {
    $(`#${private__type}_type`).val(private__where_to);
    $(`#${private__type}_deptors`).val(private__deptors);
  }

  $(`#${private__type}_account`).val(private__account);
  $(`#${private__type == 'to' ? 'from' : private_type}_amount`).val(private__amount);

  if (
    income_types.includes(private__where_to) ||
    expense_types.includes(private__where_to) ||
    private__where_to == "saving"
  ) {
    $(`#${private__type}_category`).val(private__category);
    $(`#${private__type}_date`).val(private__date);
  }

  if (private__where_to == "saving") {
    $(`#saving_to_account`).val(private__source__account);
  }

  if (private__where_to == "transfer") {
    $(`#transfer_date`).val(private__date);
    $(`#from_account`).val(private__source__account);
  }

  !["saving", "transfer"].includes(private__where_to) &&
    handleWhere(private__type, private__where_to);
  handleAccount(private__type);

  $("#expense_type").on("change", function () {
    handleWhere("expense", $(this).val());
  });

  $("#income_type").on("change", function () {
    handleWhere("income", $(this).val());
  });

  $("#expense_account").on("change", function () {
    handleAccount("expense");
  });

  $("#income_account").on("change", function () {
    handleAccount("income");
  });

  $("#saving_account").on("change", function () {
    handleAccount("saving");
  });

  $("#from_account").on("change", function () {
    handleAccount("from");
  });

  $("#expense_amount").on("input", function () {
    if ($(this).val() < 0) {
      $(this).val(0);
    }
    handleAccount("expense");
  });

  $("#income_amount").on("input", function () {
    if ($(this).val() < 0) {
      $(this).val(0);
    }
    handleAccount("income");
  });

  $("#saving_amount").on("input", function () {
    if ($(this).val() < 0) {
      $(this).val(0);
    }
    handleAccount("saving");
  });

  $("#from_amount").on("input", function () {
    if ($(this).val() < 0) {
      $(this).val(0);
    }
    handleAccount("from");
  });

  // this function will handle where to or where from
  // both are from income and expense when determining that
  // the transaction is normal or dealing with dept
  function handleWhere(type, value) {
    $(`.${type}_category`).addClass("d-none");
    $(`.${type}_deptors`).addClass("d-none");

    if (value == "normal") {
      $(`.${type}_category`).removeClass("d-none");
    }

    if (["return_dept", "take_dept", "give_dept", "pay_dept"].includes(value)) {
      $(`.${type}_deptors`).removeClass("d-none");
    }
  }

  // this function will handle for showing the account selected's balance
  // also watching over the amount entered to update the fields
  // also to keep up what will be the new balance
  function handleAccount(type) {
    type = type == "to" ? "from" : type;
    $(`.${type}_alert`).addClass("d-none");

    // check if an account is selected
    if ($(`#${type}_account`).val() != "") {
      $(`.${type}_alert`).removeClass("d-none");

      const amount = $("option:selected", `#${type}_account`).attr("amount");
      const name = $("option:selected", `#${type}_account`).text();
      let transaction_amount = $(`#${type}_amount`).val();

      transaction_amount = [NaN, undefined, ""].includes(transaction_amount)
        ? 0
        : transaction_amount;

      //   update the alert
      $(`#${type}_account_name_show`).text(name);
      $(`#${type}_account_balance_show`).text(formatCurrency(amount));
      $(`#${type}_transaction_balance_show`).text(
        formatCurrency(transaction_amount)
      );

      let new_balance = 0;

      if (["expense", "saving", "from"].includes(type))
        new_balance = parseFloat(amount) - parseFloat(transaction_amount);

      if (type == "income")
        new_balance = parseFloat(amount) + parseFloat(transaction_amount);

      $(`#${type}_new_balance_show`).text(formatCurrency(new_balance));
    }
  }

  function formatCurrency(number, currencySymbol = "$") {
    // Convert the number to a string with two decimal places
    const formattedNumber = Number(number).toFixed(2);

    // Split the number into integer and decimal parts
    const [integerPart, decimalPart] = formattedNumber.split(".");

    // Add commas to the integer part of the number
    const formattedIntegerPart = integerPart.replace(
      /\B(?=(\d{3})+(?!\d))/g,
      ","
    );

    // Combine the formatted integer and decimal parts with the currency symbol
    const formattedAmount =
      currencySymbol + formattedIntegerPart + "." + decimalPart;

    // Return the formatted currency string
    return formattedAmount;
  }

  $("#save_income").on("click", function () {
    save_transaction($(this), "income");
  });

  $("#save_expense").on("click", function () {
    save_transaction($(this), "expense");
  });

  $("#save_saving").on("click", function () {
    save_transfer($(this), "saving");
  });

  $("#save_transfer").on("click", function () {
    save_transfer($(this), "account_to_account");
  });

  // save the transaction
  function save_transaction(button, type) {
    // Check if the action has already been performed
    // if so, deny any further processing
    const loading = button.attr("data-loading");
    if (loading == "true") return;

    let formData = new FormData();
    formData.append("type", type == "income" ? "deposit" : "withdraw");
    formData.append("income_type", $(`#${type}_type`).val());
    formData.append("income_category", $(`#${type}_category`).val());
    formData.append("income_deptors", $(`#${type}_deptors`).val());
    formData.append("income_account", $(`#${type}_account`).val());
    formData.append("income_amount", $(`#${type}_amount`).val());
    formData.append("income_date", $(`#${type}_date`).val());
    formData.append("row", private__id);

    // if (action == "EditAccount") {
    //   formData.append("account_id", $("#account_id").val());
    // }

    $.ajax({
      method: "POST",
      url: `/manage_essentials/EditTransaction`,
      headers: { "X-CSRFToken": csrftoken },
      beforeSend: function () {
        // Make the laoding to true
        button.attr("data-loading", "true");
        button.html(`
              <span class="d-flex justify-content-center align-items-center">
                  <span class="spinner-border flex-shrink-0" role="status">
                      <span class="visually-hidden">Loading...</span>
                  </span>
              </span>
          `);
      },
      processData: false,
      contentType: false,
      data: formData,
      async: true,
      success: function (response) {
        button.attr("data-loading", "false");
        button.html("Submit transaction");
        if (!response.isError) {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          }).then((e) => {
            if (e.value) {
              location.reload();
            }
          });
        } else {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          });
        }
      },
      error: function (response) {
        button.attr("data-loading", "false");
        button.html("Save");
      },
    });
  }

  function save_transfer(button, type) {
    if (!["saving", "account_to_account"].includes(type)) return;

    // Check if the action has already been performed
    // if so, deny any further processing
    const loading = button.attr("data-loading");
    if (loading == "true") return;

    // variables
    let transfer_account = $(
      `#${type == "saving" ? "saving_to_account" : "to_account"}`
    ).val();
    let transfer_amount = $(
      `#${type == "saving" ? "saving_amount" : "from_amount"}`
    ).val();
    let account_id = $(
      `#${type == "saving" ? "saving_account" : "from_account"}`
    ).val();
    let transfer_date = $(
      `#${type == "saving" ? "saving_date" : "transfer_date"}`
    ).val();
    let transfer_category = $("#saving_category").val();

    let formData = new FormData();
    formData.append("transfer_account", transfer_account);
    formData.append("transfer_category", transfer_category);
    formData.append("transfer_amount", transfer_amount);
    formData.append("account_id", account_id);
    formData.append("transfer_date", transfer_date);
    formData.append("transfer_type", type);
    formData.append("row", private__id);

    $.ajax({
      method: "POST",
      url: `/manage_essentials/EditTransferAmount`,
      headers: { "X-CSRFToken": csrftoken },
      beforeSend: function () {
        // Make the laoding to true
        button.attr("data-loading", "true");
        button.html(`
              <span class="d-flex justify-content-center align-items-center">
                  <span class="spinner-border flex-shrink-0" role="status">
                      <span class="visually-hidden">Loading...</span>
                  </span>
              </span>
          `);
      },
      processData: false,
      contentType: false,
      data: formData,
      async: true,
      success: function (response) {
        button.attr("data-loading", "false");
        button.html("Submit transaction");
        if (!response.isError) {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          }).then((e) => {
            if (e.value) {
              location.reload();
            }
          });
        } else {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          });
        }
      },
      error: function (response) {
        button.attr("data-loading", "false");
        button.html("Login");
      },
    });
  }
});
