$(document).ready(function () {
    // action variable with default value
    //   this is for creating and updating for the same modal
    let action = "CreateDeptor";
  
    // Update The Entries Selection
    $("#DataNumber").val($("#DataNumber").attr("DataNumber"));
  
    $("#DataNumber").change(function () {
      RefreshPage();
    });
  
    $("#SearchQuery").on("keydown", function (event) {
      if (event.keyCode === 13) {
        event.preventDefault();
        RefreshPage();
      }
    });
  
    $(".pagination .page-item .page-link").on("click", function (e) {
      const pageNumber = $(this).attr("page");
      $(".activePage").attr("activePage", pageNumber);
      RefreshPage();
    });
  
    $(".SearchQueryBTN").on("click", function () {
      RefreshPage();
    });
  
    function RefreshPage() {
      let page = $(".activePage").attr("activePage");
      let search = $("#SearchQuery").val();
      let entries = $("#DataNumber").val();
  
      let url = `/deptors?`;
  
      if (search != "") {
        url += `&search=${search}`;
      }
  
      if (page != "" && page != undefined && page != null) {
        url += `&page=${page}`;
      }
  
      if (entries != "" && entries != undefined && entries != null) {
        url += `&rows=${entries}`;
      }
  
      window.location.replace(url);
    }
  
    $("#new_deptor_btn").on("click", function () {
      $("#deptorModal #modalLabel").text("Add deptor");
      $("#save_deptor").text("Create");
      action = "CreateDeptor";
      $("#deptorModal").modal("show");
  
      // reset the input
      $("#deptor_name").val("");
      $("#deptor_phone").val("");
    });
  
    $("#save_deptor").on("click", function (e) {
      e.preventDefault();
  
      const button = $(this);
  
      // Check if the action has already been performed
      // if so, deny any further processing
      const loading = button.attr("data-loading");
      if (loading == "true") return;
  
      let formData = new FormData();
      formData.append("deptor_name", $("#deptor_name").val());
      formData.append("deptor_phone", $("#deptor_phone").val());
  
      if (action == "EditDeptor") {
        formData.append("deptor_id", $("#deptor_id").val());
      }
  
      $.ajax({
        method: "POST",
        url: `/manage_essentials/${action}`,
        headers: { "X-CSRFToken": csrftoken },
        beforeSend: function () {
          // Make the laoding to true
          button.attr("data-loading", "true");
          button.html(`
                <span class="d-flex justify-content-center align-items-center">
                    <span class="spinner-border flex-shrink-0" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </span>
                </span>
            `);
        },
        processData: false,
        contentType: false,
        data: formData,
        async: true,
        success: function (response) {
          button.attr("data-loading", "false");
          button.html(action == "EditDeptor" ? "Update" : "Create");
          if (!response.isError) {
            Swal.fire({
              title: response.title,
              text: response.message,
              icon: response.icon,
              confirmButtonClass: "btn btn-primary w-xs mt-2",
              buttonsStyling: !1,
              showCloseButton: true,
            }).then((e) => {
              if (e.value) {
                RefreshPage();
              }
            });
          } else {
            Swal.fire({
              title: response.title,
              text: response.message,
              icon: response.icon,
              confirmButtonClass: "btn btn-primary w-xs mt-2",
              buttonsStyling: !1,
              showCloseButton: true,
            });
          }
        },
        error: function (response) {
          button.attr("data-loading", "false");
          button.html("Login");
        },
      });
    });
  
    $(".deptors_table").on("click", "#edit_deptor", function () {
      const button = $(this);
  
      // Check if the action has already been performed
      // if so, deny any further processing
      const loading = button.attr("data-loading");
      const row = button.attr("data-row");
      if (loading == "true") return;
  
      let formData = new FormData();
      formData.append("row", row);
  
      $.ajax({
        method: "POST",
        url: `/manage_essentials/GetDeptor`,
        headers: { "X-CSRFToken": csrftoken },
        beforeSend: function () {
          // Make the laoding to true
          button.attr("data-loading", "true");
          button.html(`
                <span class="d-flex justify-content-center align-items-center">
                    <span class="spinner-border flex-shrink-0" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </span>
                </span>
            `);
        },
        processData: false,
        contentType: false,
        data: formData,
        async: true,
        success: function (response) {
          button.attr("data-loading", "false");
          button.html(`<i class="feather-icon-sm" data-feather="edit-2"></i>`);
          feather.replace();
  
          if (!response.isError) {
            $("#deptor_name").val(response.message.name);
            $("#deptor_phone").val(response.message.phone);
            $("#deptor_id").val(response.message.id);
            $("#deptorModal #modalLabel").text("Edit deptor");
            $("#save_deptor").text("Update");
            $("#deptorModal").modal("show");
            action = "EditDeptor";
          } else {
            Swal.fire({
              title: response.title,
              text: response.message,
              icon: response.icon,
              confirmButtonClass: "btn btn-primary w-xs mt-2",
              buttonsStyling: !1,
              showCloseButton: true,
            });
          }
        },
        error: function (response) {
          button.html(`<i class="feather-icon-sm" data-feather="edit-2"></i>`);
          feather.replace();
        },
      });
    });
  });
  