$(document).ready(function () {
  // action variable with default value
  //   this is for creating and updating for the same modal
  let action = "CreateCategory";

  // Update The Entries Selection
  $("#DataNumber").val($("#DataNumber").attr("DataNumber"));
  $("#CategoryType").val($("#CategoryType").attr("CategoryType"));

  $("#DataNumber").change(function () {
    RefreshPage();
  });

  $("#CategoryType").change(function () {
    RefreshPage();
  });

  $("#SearchQuery").on("keydown", function (event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      RefreshPage();
    }
  });

  $(".pagination .page-item .page-link").on("click", function (e) {
    const pageNumber = $(this).attr("page");
    $(".activePage").attr("activePage", pageNumber);
    RefreshPage();
  });

  $(".SearchQueryBTN").on("click", function () {
    RefreshPage();
  });

  function RefreshPage() {
    let page = $(".activePage").attr("activePage");
    let search = $("#SearchQuery").val();
    let entries = $("#DataNumber").val();
    let category = $("#CategoryType").val();

    let url = `/categories?`;

    if (search != "") {
      url += `&search=${search}`;
    }

    if (page != "" && page != undefined && page != null) {
      url += `&page=${page}`;
    }

    if (entries != "" && entries != undefined && entries != null) {
      url += `&rows=${entries}`;
    }

    if (category != "" && category != undefined && category != null) {
      url += `&category=${category}`;
    }

    window.location.replace(url);
  }

  $("#new_category_btn").on("click", function () {
    $("#categoryModal #modalLabel").text("Add category");
    $("#save_category").text("Create");
    action = "CreateCategory";
    $("#categoryModal").modal("show");

    // reset the input
    $("#category_name").val("");
    $("#category_type").val("");
  });

  $("#save_category").on("click", function (e) {
    e.preventDefault();

    const button = $(this);

    // Check if the action has already been performed
    // if so, deny any further processing
    const loading = button.attr("data-loading");
    if (loading == "true") return;

    let formData = new FormData();
    formData.append("category_name", $("#category_name").val());
    formData.append("category_type", $("#category_type").val());

    if (action == "EditCategory") {
      formData.append("category_id", $("#category_id").val());
    }

    $.ajax({
      method: "POST",
      url: `/manage_essentials/${action}`,
      headers: { "X-CSRFToken": csrftoken },
      beforeSend: function () {
        // Make the laoding to true
        button.attr("data-loading", "true");
        button.html(`
                <span class="d-flex justify-content-center align-items-center">
                    <span class="spinner-border flex-shrink-0" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </span>
                </span>
            `);
      },
      processData: false,
      contentType: false,
      data: formData,
      async: true,
      success: function (response) {
        button.attr("data-loading", "false");
        button.html(action == "EditCategory" ? "Update" : "Create");
        if (!response.isError) {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          }).then((e) => {
            if (e.value) {
              RefreshPage();
            }
          });
        } else {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          });
        }
      },
      error: function (response) {
        button.attr("data-loading", "false");
        button.html("Login");
      },
    });
  });

  $(".category_table").on("click", "#edit_category", function () {
    const button = $(this);

    // Check if the action has already been performed
    // if so, deny any further processing
    const loading = button.attr("data-loading");
    const row = button.attr("data-row");
    if (loading == "true") return;

    let formData = new FormData();
    formData.append("row", row);

    $.ajax({
      method: "POST",
      url: `/manage_essentials/GetCategory`,
      headers: { "X-CSRFToken": csrftoken },
      beforeSend: function () {
        // Make the laoding to true
        button.attr("data-loading", "true");
        button.html(`
                <span class="d-flex justify-content-center align-items-center">
                    <span class="spinner-border flex-shrink-0" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </span>
                </span>
            `);
      },
      processData: false,
      contentType: false,
      data: formData,
      async: true,
      success: function (response) {
        button.attr("data-loading", "false");
        button.html(`<i class="feather-icon-sm" data-feather="edit-2"></i>`);
        feather.replace();

        if (!response.isError) {
          $("#category_type").val(response.message.type);
          $("#category_name").val(response.message.name);
          $("#category_id").val(response.message.id);
          $("#categoryModal #modalLabel").text("Edit category");
          $("#save_category").text("Update");
          $("#categoryModal").modal("show");
          action = "EditCategory";
        } else {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          });
        }
      },
      error: function (response) {
        button.html(`<i class="feather-icon-sm" data-feather="edit-2"></i>`);
        feather.replace();
      },
    });
  });
});
