$(document).ready(function () {
  // set deafult dates for expense and income date
  let date = new Date();
  const formattedDate = date.toISOString().substr(0, 10);
  $("#transfer_date").val(formattedDate);

  // Update The Entries Selection
  $("#DataNumber").val($("#DataNumber").attr("DataNumber"));
  $("#years").val($("#years").attr("years"));
  $("#months").val($("#months").attr("months"));

  $("#DataNumber").change(function () {
    RefreshPage();
  });

  $("#years").change(function () {
    RefreshPage();
  });

  $("#months").change(function () {
    RefreshPage();
  });

  $("#SearchQuery").on("keydown", function (event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      RefreshPage();
    }
  });

  $(".pagination .page-item .page-link").on("click", function (e) {
    const pageNumber = $(this).attr("page");
    $(".activePage").attr("activePage", pageNumber);
    RefreshPage();
  });

  $(".SearchQueryBTN").on("click", function () {
    RefreshPage();
  });

  function RefreshPage() {
    let page = $(".activePage").attr("activePage");
    let search = $("#SearchQuery").val();
    let entries = $("#DataNumber").val();
    let year = $("#years").val();
    let month = $("#months").val();

    let url = `/account_details/${private__encrypted__id}?`;

    if (search != "") {
      url += `&search=${search}`;
    }

    if (page != "" && page != undefined && page != null) {
      url += `&page=${page}`;
    }

    if (entries != "" && entries != undefined && entries != null) {
      url += `&rows=${entries}`;
    }

    if (year != "" && year != undefined && year != null) {
      url += `&year=${year}`;
    }

    if (
      month != "" &&
      month != undefined &&
      month != null &&
      year != "" &&
      year != undefined &&
      year != null &&
      year != "all"
    ) {
      url += `&month=${month}`;
    }

    window.location.replace(url);
  }

  $("#save_transfer").on("click", function (e) {
    e.preventDefault();

    const button = $(this);

    // Check if the action has already been performed
    // if so, deny any further processing
    const loading = button.attr("data-loading");
    if (loading == "true") return;

    let formData = new FormData();
    formData.append("transfer_account", $("#transfer_account").val());
    formData.append("transfer_amount", $("#transfer_amount").val());
    formData.append("account_id", private__id);
    formData.append("transfer_date", $("#transfer_date").val());
    formData.append("transfer_type", 'account_to_account');

    $.ajax({
      method: "POST",
      url: `/manage_essentials/TransferAmount`,
      headers: { "X-CSRFToken": csrftoken },
      beforeSend: function () {
        // Make the laoding to true
        button.attr("data-loading", "true");
        button.html(`
              <span class="d-flex justify-content-center align-items-center">
                  <span class="spinner-border flex-shrink-0" role="status">
                      <span class="visually-hidden">Loading...</span>
                  </span>
              </span>
          `);
      },
      processData: false,
      contentType: false,
      data: formData,
      async: true,
      success: function (response) {
        button.attr("data-loading", "false");
        button.html("Submit transfer");
        if (!response.isError) {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          }).then((e) => {
            if (e.value) {
              RefreshPage();
            }
          });
        } else {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          });
        }
      },
      error: function (response) {
        button.attr("data-loading", "false");
        button.html("Login");
      },
    });
  });

  $("#transfer_money").on("click", function () {
    $("#transferModal").modal("show");

    // reset the input
    $("#transfer_amount").val("");
    $("#transfer_account").val("");
    $("#transfer_amount_words").val("");
  });

  $("#save_account").on("click", function (e) {
    e.preventDefault();

    const button = $(this);

    // Check if the action has already been performed
    // if so, deny any further processing
    const loading = button.attr("data-loading");
    if (loading == "true") return;

    let formData = new FormData();
    formData.append("account_name", $("#account_name").val());

    if (action == "EditAccount") {
      formData.append("account_id", $("#account_id").val());
    }

    $.ajax({
      method: "POST",
      url: `/manage_essentials/${action}`,
      headers: { "X-CSRFToken": csrftoken },
      beforeSend: function () {
        // Make the laoding to true
        button.attr("data-loading", "true");
        button.html(`
                <span class="d-flex justify-content-center align-items-center">
                    <span class="spinner-border flex-shrink-0" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </span>
                </span>
            `);
      },
      processData: false,
      contentType: false,
      data: formData,
      async: true,
      success: function (response) {
        button.attr("data-loading", "false");
        button.html(action == "EditAccount" ? "Update" : "Create");
        if (!response.isError) {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          }).then((e) => {
            if (e.value) {
              RefreshPage();
            }
          });
        } else {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          });
        }
      },
      error: function (response) {
        button.attr("data-loading", "false");
        button.html("Login");
      },
    });
  });

  $("#transfer_amount").on("input", function () {
    if ($(this).val() < 0) {
      $(this).val(0);
    }
    $("#transfer_amount_words").val();

    if ($(this).val() != "") {
      $("#transfer_amount_words").val(number2words($(this).val()));
    }
  });

  // System for American Numbering
  var th_val = ["", "thousand", "million", "billion", "trillion"];
  // System for uncomment this line for Number of English
  // var th_val = ['','thousand','million', 'milliard','billion'];

  var dg_val = [
    "zero",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
  ];
  var tn_val = [
    "ten",
    "eleven",
    "twelve",
    "thirteen",
    "fourteen",
    "fifteen",
    "sixteen",
    "seventeen",
    "eighteen",
    "nineteen",
  ];
  var tw_val = [
    "twenty",
    "thirty",
    "forty",
    "fifty",
    "sixty",
    "seventy",
    "eighty",
    "ninety",
  ];
  function number2words(s) {
    s = s.toString();
    s = s.replace(/[\, ]/g, "");
    if (s != parseFloat(s)) return "not a number ";
    var x_val = s.indexOf(".");
    if (x_val == -1) x_val = s.length;
    if (x_val > 15) return "too big";
    var n_val = s.split("");
    var str_val = "";
    var sk_val = 0;
    for (var i = 0; i < x_val; i++) {
      if ((x_val - i) % 3 == 2) {
        if (n_val[i] == "1") {
          str_val += tn_val[Number(n_val[i + 1])] + " ";
          i++;
          sk_val = 1;
        } else if (n_val[i] != 0) {
          str_val += tw_val[n_val[i] - 2] + " ";
          sk_val = 1;
        }
      } else if (n_val[i] != 0) {
        str_val += dg_val[n_val[i]] + " ";
        if ((x_val - i) % 3 == 0) str_val += "hundred ";
        sk_val = 1;
      }
      if ((x_val - i) % 3 == 1) {
        if (sk_val) str_val += th_val[(x_val - i - 1) / 3] + " ";
        sk_val = 0;
      }
    }
    if (x_val != s.length) {
      var y_val = s.length;
      str_val += "point ";
      for (var i = x_val + 1; i < y_val; i++) str_val += dg_val[n_val[i]] + " ";
    }
    return str_val.replace(/\s+/g, " ");
  }
});
