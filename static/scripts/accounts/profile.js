$(document).ready(function () {
  $("#update_information").on("click", function (e) {
    e.preventDefault();

    const button = $(this);

    // Check if the action has already been performed
    // if so, deny any further processing
    const loading = button.attr("data-loading");
    if (loading == "true") return;

    let formData = new FormData();
    formData.append("first_name", $("#first_name").val());
    formData.append("last_name", $("#last_name").val());
    formData.append("email", $("#email_address").val());
    formData.append("phone_number", $("#phone_number").val());
    formData.append("gender", $("#gender").val());

    $.ajax({
      method: "POST",
      url: "manage_account/UpdateInformation",
      headers: { "X-CSRFToken": csrftoken },
      beforeSend: function () {
        // Make the laoding to true
        button.attr("data-loading", "true");
        button.html(`
                <span class="d-flex justify-content-center align-items-center">
                    <span class="spinner-border flex-shrink-0" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </span>
                </span>
            `);
      },
      processData: false,
      contentType: false,
      data: formData,
      async: true,
      success: function (response) {
        button.attr("data-loading", "false");
        button.html("Update information");
        if (!response.isError) {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          }).then((e) => {
            e.value && location.reload();
          });
        } else {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          });
        }
      },
      error: function (response) {
        button.attr("data-loading", "false");
        button.html("Login");
      },
    });
  });

  $("#change_password").on("click", function (e) {
    e.preventDefault();

    const button = $(this);

    // Check if the action has already been performed
    // if so, deny any further processing
    const loading = button.attr("data-loading");
    if (loading == "true") return;

    let formData = new FormData();
    formData.append("current_password", $("#current_password").val());
    formData.append("new_password", $("#new_password").val());
    formData.append("confirm_password", $("#confirm_password").val());

    $.ajax({
      method: "POST",
      url: "manage_account/ChangePassword",
      headers: { "X-CSRFToken": csrftoken },
      beforeSend: function () {
        // Make the laoding to true
        button.attr("data-loading", "true");
        button.html(`
                <span class="d-flex justify-content-center align-items-center">
                    <span class="spinner-border flex-shrink-0" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </span>
                </span>
            `);
      },
      processData: false,
      contentType: false,
      data: formData,
      async: true,
      success: function (response) {
        button.attr("data-loading", "false");
        button.html("Update Password");
        if (!response.isError) {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          }).then((e) => {
            e.value && location.reload();
          });
        } else {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          });
        }
      },
      error: function (response) {
        button.attr("data-loading", "false");
        button.html("Login");
      },
    });
  });

  let image_profile = "";

  $(".profile-img-file-input").on("change", function (e) {
    e.preventDefault();
    // document.querySelector(".user-profile-image").src = "";
    if ($(this).val() !== "") {
      image_profile = e.target.files[0];
      updateImage(e);
    }
  });

  function updateImage(e) {
    const button = $(".spin_button");
    // Check if the action has already been performed
    // if so, deny any further processing
    const loading = button.attr("data-loading");
    if (loading == "true") return;

    let formData = new FormData();
    formData.append("image", image_profile);

    $.ajax({
      method: "POST",
      url: "manage_account/UpdateProfile",
      headers: { "X-CSRFToken": csrftoken },
      beforeSend: function () {
        // Make the laoding to true
        button.attr("data-loading", "true");
        button.html(`
                <span class="d-flex justify-content-center align-items-center">
                    <span class="spinner-border flex-shrink-0" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </span>
                </span>
            `);
      },
      processData: false,
      contentType: false,
      data: formData,
      async: true,
      success: function (response) {
        button.attr("data-loading", "false");
        button.html(`<i class="mdi mdi-camera"></i>`);
        if (!response.isError) {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          }).then((e) => {
            e.value && location.reload();
          });
        } else {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          });
        }
      },
      error: function (response) {
        button.attr("data-loading", "false");
        button.html("Login");
      },
    });
  }

  // if (event.target.files && event.target.files[0]) {
  //   var reader = new FileReader();
  //   reader.onload = function (e) {
  //     document.querySelector(".user-profile-image").src =
  //       event.target.result;
  //     document.querySelector(".profile-header-image").src =
  //       event.target.result;
  //   };
  //   reader.readAsDataURL(event.target.files[0]);
  // }
});
