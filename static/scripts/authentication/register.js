$(document).ready(function () {
  $("#create_account").on("click", function (e) {
    e.preventDefault();

    const button = $(this);

    // Check if the action has already been performed
    // if so, deny any further processing
    const loading = button.attr("data-loading");
    if (loading == "true") return;

    let formData = new FormData();
    formData.append("email", $("#email").val());
    formData.append("password", $("#password").val());
    formData.append("confirm_password", $("#confirm-password").val());

    $.ajax({
      method: "POST",
      url: "/accounts/create_account",
      headers: { "X-CSRFToken": csrftoken },
      beforeSend: function () {
        // Make the laoding to true
        button.attr("data-loading" , "true");
        button.html(`
            <span class="d-flex justify-content-center align-items-center">
                <span class="spinner-border flex-shrink-0" role="status">
                    <span class="visually-hidden">Loading...</span>
                </span>
            </span>
        `);
      },
      processData: false,
      contentType: false,
      data: formData,
      async: true,
      success: function (response) {
        button.attr("data-loading" , "false");
        button.html("Register");
        if (!response.isError) {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          }).then((e) => {
            if (e.value) {
                window.location.replace('/accounts/login');            
            }
          });
        } else {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          });
        }
      },
      error: function (response) {
        button.attr("data-loading" , "false");
        button.html("Register");
      },
    });
  });
});
