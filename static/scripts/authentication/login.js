$(document).ready(function () {
  $("#login_account").on("click", function (e) {
    e.preventDefault();

    const button = $(this);

    // Check if the action has already been performed
    // if so, deny any further processing
    const loading = button.attr("data-loading");
    if (loading == "true") return;

    let formData = new FormData();
    formData.append("email", $("#email").val());
    formData.append("password", $("#password").val());

    $.ajax({
      method: "POST",
      url: "login_account",
      headers: { "X-CSRFToken": csrftoken },
      beforeSend: function () {
        // Make the laoding to true
        button.attr("data-loading", "true");
        button.html(`
              <span class="d-flex justify-content-center align-items-center">
                  <span class="spinner-border flex-shrink-0" role="status">
                      <span class="visually-hidden">Loading...</span>
                  </span>
              </span>
          `);
      },
      processData: false,
      contentType: false,
      data: formData,
      async: true,
      success: function (response) {
        button.attr("data-loading", "false");
        button.html("Login");
        if (!response.isError) {
          window.location.replace(response.message.url);
        } else {
          Swal.fire({
            title: response.title,
            text: response.message,
            icon: response.icon,
            confirmButtonClass: "btn btn-primary w-xs mt-2",
            buttonsStyling: !1,
            showCloseButton: true,
          });
        }
      },
      error: function (response) {
        button.attr("data-loading", "false");
        button.html("Login");
      },
    });
  });
});
