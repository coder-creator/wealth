$(document).ready(function () {
  flatpickr("#range_date", {
    mode: "range",
    dateFormat: "Y-m-d",
    defaultDate: range_date,
  });
  flatpickr("#multiple_date", {
    mode: "multiple",
    dateFormat: "Y-m-d",
    defaultDate: multiple_date,
  });

  $("#type_of_date").on("change", handleDates);

  function handleDates() {
    // hide all dates fiest
    $("#range_date").parent().addClass("d-none");
    $("#multiple_date").parent().addClass("d-none");
    $("#single_date").parent().addClass("d-none");

    var date_type = $("#type_of_date").val();

    if (date_type != "") {
      $(`#${date_type.toLowerCase()}_date`).parent().removeClass("d-none");
    }
  }

  // Update The Entries Selection
  $("#DataNumber").val($("#DataNumber").attr("DataNumber"));
  $("#type_of_date").val($("#type_of_date").attr("data-type-of-date"));
  $("#single_date").val($("#single_date").attr("data-single-date"));

  handleDates($("#type_of_date").val());

  $("#DataNumber").change(function () {
    RefreshPage();
  });

  $("#process_filter").click(function () {
    RefreshPage();
  });

  $("#clear_filter").click(function () {
    let url = `/earning_report`;
    window.location.replace(url);
  });

  $("#SearchQuery").on("keydown", function (event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      RefreshPage();
    }
  });

  $(".pagination .page-item .page-link").on("click", function (e) {
    const pageNumber = $(this).attr("page");
    $(".activePage").attr("activePage", pageNumber);
    RefreshPage();
  });

  $(".SearchQueryBTN").on("click", function () {
    RefreshPage();
  });

  function RefreshPage() {
    let page = $(".activePage").attr("activePage");
    let search = $("#SearchQuery").val();
    let entries = $("#DataNumber").val();
    let type_of_date = $("#type_of_date").val();
    let single_date = $("#single_date").val();
    let range_date = $("#range_date").val();
    let multiple_date = $("#multiple_date").val();

    let url = `/earning_report?`;

    if (search != "") {
      url += `&search=${search}`;
    }

    if (page != "" && page != undefined && page != null) {
      url += `&page=${page}`;
    }

    if (entries != "" && entries != undefined && entries != null) {
      url += `&rows=${entries}`;
    }

    if (
      type_of_date != "" &&
      type_of_date != undefined &&
      type_of_date != null
    ) {
      url += `&type_of_date=${type_of_date}`;
    }

    if (
      single_date != "" &&
      single_date != undefined &&
      single_date != null &&
      type_of_date == "single"
    ) {
      url += `&single_date=${single_date}`;
    }

    if (
      range_date != "" &&
      range_date != undefined &&
      range_date != null &&
      type_of_date == "range"
    ) {
      url += `&range_date=${range_date}`;
    }

    if (
      multiple_date != "" &&
      multiple_date != undefined &&
      multiple_date != null &&
      type_of_date == "multiple"
    ) {
      url += `&multiple_date=${multiple_date}`;
    }

    window.location.replace(url);
  }
});
