$(document).ready(function () {
  // Update The Entries Selection
  $("#DataNumber").val($("#DataNumber").attr("DataNumber"));
  $("#DataNumber").change(function () {
    RefreshPage();
  });

  $("#SearchQuery").on("keydown", function (event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      RefreshPage();
    }
  });

  $(".pagination .page-item .page-link").on("click", function (e) {
    const pageNumber = $(this).attr("page");
    $(".activePage").attr("activePage", pageNumber);
    RefreshPage();
  });

  $(".SearchQueryBTN").on("click", function () {
    RefreshPage();
  });

  function RefreshPage() {
    let page = $(".activePage").attr("activePage");
    let search = $("#SearchQuery").val();
    let entries = $("#DataNumber").val();

    let url = `/spending_detail?${url_string}`;

    if (search != "") {
      url += `&search=${search}`;
    }

    if (page != "" && page != undefined && page != null) {
      url += `&page=${page}`;
    }

    if (entries != "" && entries != undefined && entries != null) {
      url += `&rows=${entries}`;
    }

    window.location.replace(url);
  }
});
