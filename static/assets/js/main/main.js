$(document).ready(function () {
  feather.replace();

  configureStyles(get_or_set_session("mode", "light"));
  toggleSidebar(get_or_set_session("navbar", "blokc"));

  $(".item-arrow").on("click", function () {
    const is_collapsed = $(this).attr("data-is-collapsed");

    if (is_collapsed == "false" || is_collapsed == undefined) {
      $(this).attr("data-is-collapsed", true);
      $(this).html(`<i class="arrow" data-feather="arrow-up"></i>`);
    } else {
      $(this).attr("data-is-collapsed", false);
      $(this).html(`<i class="arrow" data-feather="arrow-down"></i>`);
    }

    $(this)
      .parent()
      .parent()
      .siblings()
      .find(".item-arrow")
      .each(function () {
        const is_collapsed = $(this).attr("data-is-collapsed");
        if (is_collapsed != "" && is_collapsed != undefined) {
          $(this).html(`<i class="arrow" data-feather="arrow-down"></i>`);
        }
      });
    feather.replace();

    $(this).parent().parent().find(".submenu-list").slideToggle(200);
    $(this).parent().parent().siblings().find(".submenu-list").slideUp(200);
  });

  $("#toggle_sidebar").on("click", function () {
    const currentDisplay = $(".layout-menu").css("display");
    sessionStorage.setItem(
      "navbar",
      currentDisplay == "none" ? "block" : "none"
    );
    toggleSidebar(currentDisplay == "none" ? "block" : "none");
  });

  $(".layout-content").click(function (event) {
    const currentWidth = $(document).width();
    if (
      currentWidth <= 767 &&
      !$(event.target).closest(".layout-menu").length &&
      !$(event.target).is("#toggle_sidebar")
    ) {
      toggleSidebar("none");
    }
  });

  $("#configure_mode").on("click", function () {
    const mode =
      get_or_set_session("mode", "light") == "light" ? "dark" : "light";
    sessionStorage.setItem("mode", mode);
    configureStyles(mode);
  });

  // function to toggle between block and none to the sidebar
  function toggleSidebar(value) {
    document
      .querySelector(".layout-menu")
      .style.setProperty("display", value, "important");
  }

  function get_or_set_session(session_name, session_default) {
    // first check if the "mode" already exists
    let mode = sessionStorage.getItem(session_name);

    if (mode == null || mode == undefined) {
      mode = session_default;
      sessionStorage.setItem(session_name, mode);
    }

    return mode;
  }

  function configureStyles(mode) {
    // Applying the theme to the html
    $("#body_element").attr("data-bs-theme", mode);
  }

  const toggleFullScreen = () => {
    const elem = document.documentElement; // Get the document element

    if (!document.fullscreenElement) {
      // Check if the document is not in full screen mode
      elem.requestFullscreen(); // Set the element to full screen mode
    } else {
      if (document.exitFullscreen) {
        // Check if the exitFullscreen method is supported
        document.exitFullscreen(); // Exit full screen mode
      }
    }
  };

  document
    .querySelector("#configure_screen")
    .addEventListener("click", toggleFullScreen);
});
