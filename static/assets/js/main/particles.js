let partciles = document.querySelector(".particles .wrapper");

const partciles_width = partciles.clientWidth;
const partciles_height = partciles.clientHeight;

for (var i = 0; i < 50; i++) {
  const shapes = ["circled", "circled-outlined", "box", "box-outlined"];
  const randomIndex = Math.floor(Math.random() * shapes.length);
  const randomShape = shapes[randomIndex];

  let particle = document.createElement("div");
  particle.classList.add(randomShape);

  particle.style.position = "absolute";
  particle.style.top = partciles_width - getRandomNumber(0, partciles_width)  + "px";
  particle.style.left = partciles_height - getRandomNumber(0, partciles_height) + "px";

  partciles.appendChild(particle);
}

function getRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}
