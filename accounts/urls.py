from django.urls import path
from .auth import *
from .views import *

urlpatterns = [
    path('login' , LoginView , name='login'),
    path('register' , RegisterView , name='register'),
    path('logout' , LogoutView , name='logout'),
    path('users_list' , AccountsList , name="users_list"),
    path('profile' , Profile , name="profile"),

    # dashboard
    path('users_dashboard' , UsersDashboard , name='users_dashboard'),

    # django setting
    path('change_language/<str:lang_code>' , ChangeLanguage , name='change_language'),

    # management views
    path('create_account' , CreateAccount , name='create_account'),
    path('login_account' , LoginAccount , name='login_account'),
    path('manage_account/<str:action>' , ManageAccount , name='manage_account'),
]


