from django.utils import translation
from django.conf import settings
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import check_password
from django.contrib.auth import login,  logout
from django.views.i18n import set_language
from django.core.paginator import Paginator


# Import models
from accounts.models import *

# Import m custom packages
from my_packges.generator import *
from my_packges.random_password import *
from my_packges.logs import *
from my_packges.logs import handle_exceptions


def LogoutView(request):
    if not request.user.is_authenticated:
        return redirect('login')

    logout(request)

    return redirect('login')


def LoginView(request):
    if request.user.is_authenticated:
        return redirect('finance_dashboard')

    context = {
        'title': "Login",
        'parent': "Authentication"
    }
    return render(request, 'authentication/login.html', context)


def RegisterView(request):
    # If the user is logged in , cannot create an account
    # the user must be out of the system
    if request.user.is_authenticated:
        return redirect('finance_dashboard')

    context = {
        'title': "Register",
        'parent': "Authentication"
    }
    return render(request, 'authentication/register.html', context)


# @login_required(login_url='login')
@handle_exceptions()
def CreateAccount(request):
    email = request.POST['email']
    password = request.POST['password']
    confirm_password = request.POST['confirm_password']

    # Here we will validate those inputs
    if not validate_input(email):
        return JsonResponse(Messages.validation_message("Please enter an email"))

    if not validate_email_address(email):
        return JsonResponse(Messages.validation_message("Please enter a valid email"))

    # When the email becomes valid
    # Make it all lowercase
    email = email.lower()

    if not validate_input(password):
        return JsonResponse(Messages.validation_message("Please enter a password"))

    if not validate_input(confirm_password):
        return JsonResponse(Messages.validation_message("Please enter a confirmation password"))

    if password != confirm_password:
        return JsonResponse(Messages.validation_message("Password and confirm password do not match"))

    # Checking if this email is alrrady been used by another user/account
    if Users.objects.filter(email=email).exists():
        return JsonResponse(Messages.validation_message("This email already exists"))

    # Genrate New Username
    last_username = Users.objects.all().order_by('-username').first()

    # By default it will start from 0 , unless username was generated before
    # if so we will increment from there
    serial = 0
    if last_username is not None:
        serial = int(last_username.username[2:])

    serial += 1
    serial = str(serial).zfill(4)

    # Now everything is set up
    # Create the user
    new_account = Users(
        first_name='',
        last_name='',
        email=email,
        username=f"WT{serial}",
        is_client=True,
    )

    # Now set the password for the user
    new_account.set_password(password)

    # Save the new account
    new_account.save()

    # After email sent and user been created
    # We have to save a trial activity
    module = 'Users'
    action = f'Creating new account/user with this email {email}'
    sendTrials(request, action, module)

    return JsonResponse(
        Messages.success_message(
            "Your new account has been created, Go and login to your account")
    )


# @login_required(login_url='login')
@handle_exceptions(is_view=False)
def LoginAccount(request):
    email = request.POST['email']
    password = request.POST['password']

    # Validate those values
    if not validate_input(email):
        return JsonResponse(Messages.validation_message("Please enter your email address"))

    if not validate_email_address(email):
        return JsonResponse(Messages.validation_message("Please enter a valid email"))

    # When the email becomes valid
    # Make it all lowercase
    email = email.lower()

    if not validate_input(password):
        return JsonResponse(Messages.validation_message("Please enter your password"))

    # Check if this email is associated with an account
    user = Users.objects.filter(email=email)

    if not user.exists():
        return JsonResponse(Messages.validation_message("Invalid email and password"))

    user = user.first()

    # Now check if the password is correct
    if not check_password(password, user.password):
        return JsonResponse(Messages.validation_message("Invalid email and password"))

    # Login the user to the account and create a session
    login(request, user)

    # We have to save a trial activity
    module = 'Users'
    action = f"Logged in..."
    sendTrials(request, action, module)

    url = '/'

    if request.user.is_superuser:
        url = '/accounts/users_dashboard'

    message = {
        'message': "Login has been processed successfully",
        'url': url,
    }

    return JsonResponse(Messages.success_message(message))


@handle_exceptions()
@handle_auth()
def ManageAccount(request: HttpRequest, action: str) -> JsonResponse:
    """
    Update user's personal information.

    Args:
        request (HttpRequest): The HTTP request object.
        action (str): The specific action to perform.

    Returns:
        JsonResponse: The JSON response containing the result of the action.
    """
    if action == 'UpdateInformation':
        first_name = request.POST.get('first_name', None)
        last_name = request.POST.get('last_name', None)
        email = request.POST.get('email', None)
        phone = request.POST.get('phone_number', None)
        gender = request.POST.get('gender', None)

        if not validate_input(first_name):
            return JsonResponse(Messages.validation_message("Please enter your first name"))

        if not validate_input(last_name):
            return JsonResponse(Messages.validation_message("Please enter your last name"))

        if not validate_input(email):
            return JsonResponse(Messages.validation_message("Please enter your email address"))

        if not validate_email_address(email):
            return JsonResponse(Messages.validation_message("Please enter a valid email"))

        # When the email becomes valid
        # Make it all lowercase
        email = email.lower()

        if not validate_input(phone):
            return JsonResponse(Messages.validation_message("Please enter your phone number"))
        
        if not validate_input(gender):
            return JsonResponse(Messages.validation_message("Please select your gender"))


        Users.objects.filter(id=request.user.id).update(
            first_name=first_name,
            last_name=last_name,
            email=email,
            phone=phone,
            gender=gender
        )

        # We have to save a trial activity
        module = 'Users'
        action = f"{request.user.get_full_name()} has updated their personal information"
        sendTrials(request, action, module)

        return JsonResponse(Messages.success_message("Your information has been updated"))

    if action == 'UpdateProfile':
        image = request.FILES.get('image' , None)

        if not validate_input(image):
            return JsonResponse(Messages.validation_message("Please set your new avatar to update your profile"))
        
        """
        Validates the extension and size of an image file.

        Args:
            image: The image file to be validated.

        Returns:
            A JsonResponse with a validation message if the file does not meet the requirements.
        """
        extension = image.name.split(".")[-1].lower()
        extension_types: List[str] = ['jpg', 'jpeg', 'png']
        supported_extensions = ", ".join(extension_types)

        if extension not in extension_types:
            return JsonResponse(Messages.validation_message(f"Image we support is {supported_extensions}"))

        if image.size > 2621440:
            return JsonResponse(Messages.validation_message("Image's size must be less than or equal to 2 megabytes (MB)"))

        image.name = f"#{request.user.username}-{remove_non_ascii_2(image.name)}"
                        

        user = Users.objects.get(id=request.user.id)
        user.avatar = image
        user.save()

        # We have to save a trial activity
        module = 'Users'
        action = f"{request.user.get_full_name()} has updated their profile avatar"
        sendTrials(request, action, module)

        return JsonResponse(Messages.success_message("Your avatar has been updated"))

    if action == "ChangePassword":
        current_password = request.POST.get("current_password" , None)
        new_password = request.POST.get("new_password" , None)
        confirm_password = request.POST.get("confirm_password" , None)

        if not validate_input(current_password):
            return JsonResponse(Messages.validation_message("Please enter your current password"))
        
        if not check_password(current_password , request.user.password):
            return JsonResponse(Messages.validation_message("Your current password is invalid"))
        
        if not validate_input(new_password):
            return JsonResponse(Messages.validation_message("Please enter your new password"))
        
        if not validate_input(confirm_password):
            return JsonResponse(Messages.validation_message("Please enter confirmation password"))
        
        if new_password != confirm_password:
            return JsonResponse(Messages.validation_message("Your password are not the same"))
        
        user = request.user

        user.set_password(new_password)
        user.save()

        # We have to save a trial activity
        module = 'Users'
        action = f"{request.user.get_full_name()} has updated their password"
        sendTrials(request, action, module)

        return JsonResponse(Messages.success_message("Your password has been updated"))


@login_required(login_url='login')
@handle_exceptions()
def ChangeLanguage(request, lang_code):
    translation.activate(lang_code)
    response = HttpResponse(...)
    response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang_code)
    return redirect('finance_dashboard')

