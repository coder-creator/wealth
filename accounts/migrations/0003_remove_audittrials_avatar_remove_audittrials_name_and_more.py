# Generated by Django 4.2.1 on 2023-05-08 19:18

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_rename_expected_error_errorlogs_expected_error_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='audittrials',
            name='Avatar',
        ),
        migrations.RemoveField(
            model_name='audittrials',
            name='Name',
        ),
        migrations.RemoveField(
            model_name='audittrials',
            name='Username',
        ),
        migrations.RemoveField(
            model_name='audittrials',
            name='user_agent',
        ),
        migrations.AddField(
            model_name='audittrials',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='user_in_audit_trial', to=settings.AUTH_USER_MODEL),
        ),
    ]
