
from django.utils import translation
from django import template
register = template.Library()


@register.simple_tag
def get_active_language():
    return translation.get_language()