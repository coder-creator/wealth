from functools import reduce
from typing import Dict, Any
from django.db.models.query import QuerySet
from finance.models import category, deptors, accounts, transactions
from django import template
from django.db.models import Sum, functions, FloatField, Q, F
register = template.Library()


@register.simple_tag
def last_activities(user: Any) -> QuerySet:
    """
    Return the latest activities of a user.

    Args:
        user: An instance of the User model.

    Returns:
        A QuerySet containing the latest 10 transactions of the given user.
    """
    # Query the 'transactions' table for the latest 10 transactions of the given user
    last_10_transactions = transactions.objects.filter(
        user=user).order_by('-id')[:10]

    return last_10_transactions


@register.simple_tag
def account_balances():
    result = []

    [
        result.append({
            'name': item.name,
            'amount': float(item.get_account_balance()['balance'])
        })
        for index, item in enumerate(accounts.objects.all())
    ]

    if result:
        result.append({
            'name': "Total",
            'amount': reduce(lambda x, amount: x + amount['amount'], result, 0)
        })

    return result


@register.simple_tag
def summarize(user: Any) -> Dict[str, Any]:
    """
    Calculate the summary of a user's transactions.

    Args:
        user: The user for whom the summary is calculated.

    Returns:
        A dictionary containing the income, expense, saving, and balance values, along with their respective percentages.
    """

    # Calculate the total income, expense, and saving for the given user
    result = transactions.objects.filter(user=user).aggregate(
        normal_income=functions.Coalesce(Sum('amount', filter=Q(
            type='deposit') & ~Q(sub_type='transfer')), 0.0),
        normal_expense=functions.Coalesce(Sum('amount', filter=Q(
            type='withdraw') & ~Q(sub_type='transfer') & ~Q(sub_type='saving')), 0.0),
        account_income=functions.Coalesce(Sum('amount', filter=Q(
            type='withdraw', sub_type='transfer')), 0.0),
        saving_income=functions.Coalesce(Sum('amount', filter=Q(
            type='withdraw', sub_type='saving')), 0.0),
    )

    # Retrieve the calculated values
    normal_income: float = result['normal_income']
    normal_expense: float = result['normal_expense']
    account_income: float = result['account_income']
    saving_income: float = result['saving_income']

    # Calculate the balance
    total: float = normal_income + account_income - normal_expense - account_income - saving_income

    # Return the summary in a dictionary format
    return {
        'income': {
            'value': round(normal_income, 2)
        },
        'expense': {
            'value': round(normal_expense, 2)
        },
        'saving': {
            'value': round(saving_income, 2),
        },
        "balance": round(abs(total), 2)
    }