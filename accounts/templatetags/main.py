import json
from finance.models import category, deptors, accounts, transactions
from django import template
from django.db.models import Sum, functions, FloatField, Q
register = template.Library()


@register.simple_tag
def validEdit(type, sub_type):
    """
    Checks if a given type and sub_type combination is valid for editing.

    Args:
        type (str): The type of the edit.
        sub_type (str): The sub-type of the edit.

    Returns:
        bool: True if the combination is valid, False otherwise.
    """
    valid_combinations = {
        'deposit': ['transfer', 'normal', 'return_dept', 'take_dept'],
        'withdraw': ['normal', 'saving', 'pay_dept', 'give_dept']
    }

    return type.lower() in valid_combinations and sub_type.lower() in valid_combinations[type.lower()]


@register.simple_tag
def activateTab(type, sub_type):
    """
    Determines which tabs should be activated based on the given type and sub_type.

    Args:
        type (str): The type of the edit.
        sub_type (str): The sub-type of the edit.

    Returns:
        dict: A dictionary with boolean values indicating which tabs should be activated.
    """
    tabs = {
        'income': False,
        'expense': False,
        'saving': False,
        'transfer': False
    }

    if type.lower() == 'deposit':
        if sub_type.lower() == 'transfer':
            tabs['transfer'] = True
        elif sub_type.lower() in ['normal', 'return_dept', 'take_dept']:
            tabs['income'] = True

    if type.lower() == 'withdraw':
        if sub_type.lower() == 'saving':
            tabs['saving'] = True
        elif sub_type.lower() in ['normal', 'pay_dept', 'give_dept']:
            tabs['expense'] = True

    return tabs


@register.simple_tag
def return_menu_titles(menu):
    if menu == 'transaction':
        return ["New Transaction", "View As Excel", "Account To Account", "View As List"]

    if menu == 'reports':
        return ["Spending Report" , "Earning Report"]
    
    if menu == 'superuser_dashboard':
        return ["Users Dashboard"]
    
    return ['unknown']


@register.simple_tag
def get_users_balance(request):
    """
    Calculates the balance of a user by aggregating the income and expense amounts from the `transactions` table.

    Args:
        request: The HTTP request object containing user information.

    Returns:
        The calculated balance of the user.
    """
    result = transactions.objects.filter(user=request.user).aggregate(
        normal_income=functions.Coalesce(Sum('amount', filter=Q(
            type='deposit')), 0.0),
        normal_expense=functions.Coalesce(Sum('amount', filter=Q(
            type='withdraw') & ~Q(sub_type='transfer') & ~Q(sub_type='saving')), 0.0),
        account_income=functions.Coalesce(Sum('amount', filter=Q(
            type='withdraw') & Q(sub_type='transfer')), 0.0),
        saving_income=functions.Coalesce(Sum('amount', filter=Q(
            type='withdraw') & Q(sub_type='saving')), 0.0),
    )

    normal_income: float = result['normal_income']
    normal_expense: float = result['normal_expense']
    account_income: float = result['account_income']
    saving_income: float = result['saving_income']
    total: float = normal_income + account_income - \
        normal_expense - account_income - saving_income

    return round(abs(total), 2)


@register.simple_tag
def get_user_categories(request, type='all'):
    args = {}
    if not request.user.is_superuser:
        args['user'] = request.user

    if type != 'all':
        args['type'] = type

    return category.objects.filter(**args)


@register.simple_tag
def get_user_deptors(request):
    args = {}
    if not request.user.is_superuser:
        args['user'] = request.user

    return deptors.objects.filter(**args)


@register.simple_tag
def get_user_accounts(request):
    args = {}
    if not request.user.is_superuser:
        args['user'] = request.user

    return accounts.objects.filter(**args)


