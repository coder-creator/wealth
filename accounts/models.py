import os
from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractUser
import sys
import traceback
from django.db.models import Q
import httpagentparser
from datetime import datetime, date, timedelta, timezone


current_date = datetime.now()


class Users(AbstractUser):
    email = models.EmailField(unique=True)
    phone = models.CharField(max_length=50, null=False,
                             blank=False)
    gender = models.CharField(max_length=10, null=True, blank=True)
    avatar = models.FileField(
        upload_to="profile-images/", null=True, blank=True)
    is_client = models.BooleanField(default=False)
    is_payment_valid = models.BooleanField(default=False)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    class Meta:
        db_table = 'users'
        ordering = ('-username',)

    def beutifyData(self):
        return PreviewDate(self.date_joined, True)

    def getUserType(self) -> str:
        """
        Returns the type of user based on their attributes.

        Returns:
            str: The type of user ("Superuser", "Admin", "Client", or "Anonymous").
        """
        if self.is_superuser:
            return "Superuser"

        if self.is_admin:
            return "Admin"

        if self.is_client:
            return "Client"

        return "Anonymous"

    def uploadedfile_url(self):
        if self.avatar and hasattr(self.avatar, 'url'):
            return self.avatar.url

    def get_avatar(self) -> str:
        """
        Retrieves the avatar image of a user.

        Returns:
            str: The path of the avatar image.
        """
        if self.avatar and image_exists(self.avatar):
            return self.uploadedfile_url()

        default_image = '/static/assets/images/default-user.png'
        return default_image

    def get_modified_date(self) -> str:
        """
        # Returns a string indicating the time elapsed since the last modification of the user.
        """
        now = datetime.now(timezone.utc)
        time_difference = now - self.modified_at
        total_seconds = int(time_difference.total_seconds())

        days, remainder = divmod(total_seconds, 86400)
        hours, remainder = divmod(remainder, 3600)
        minutes, seconds = divmod(remainder, 60)

        if days > 0:
            return f"{days} {'day' if days == 1 else 'days'} ago"

        if hours > 0:
            return f"{hours} {'hour' if hours == 1 else 'hours'} ago"

        if minutes > 0:
            return f"{minutes} {'minute' if minutes == 1 else 'minutes'} ago"

        return f"{seconds} {'second' if seconds == 1 else 'seconds'} ago"

    def get_last_login(self) -> str:
        now = datetime.now(timezone.utc)
        time_difference = now - self.last_login

        # If the time difference is less than a minute
        if time_difference < timedelta(minutes=1):
            return f"{time_difference.seconds} seconds ago"

        # If the time difference is less than an hour
        elif time_difference < timedelta(hours=1):
            return f"{time_difference.seconds // 60} minutes ago"

        # If the time difference is less than a day
        elif time_difference < timedelta(days=1):
            return f"{time_difference.seconds // 3600} hours ago"

        # If the time difference is less than a month
        elif time_difference.days < 30:
            return f"{time_difference.days} days ago"

        # If the time difference is less than a year
        elif time_difference.days < 365:
            months = time_difference.days // 30
            return f"{months} months ago"

        # If the time difference is more than a year
        else:
            years = time_difference.days // 365
            return f"{years} years ago"


def image_exists(image_field):
    image_path = image_field.path
    media_root = settings.MEDIA_ROOT
    return os.path.exists(os.path.join(media_root, image_path))


def PreviewDate(date_string, is_datetime, add_time=True):
    if is_datetime:
        new_date = date_string
        if add_time:
            date_string = new_date.strftime("%a") + ', ' + new_date.strftime(
                "%b") + ' ' + str(new_date.day) + ', ' + str(new_date.year) + '  ' + new_date.strftime("%I") + ':' + new_date.strftime("%M") + ':' + new_date.strftime("%S") + ' ' + new_date.strftime("%p")
        else:
            date_string = new_date.strftime("%a") + ', ' + new_date.strftime(
                "%b") + ' ' + str(new_date.day) + ', ' + str(new_date.year)
    else:
        date_string = str(date_string)
        date_string = date_string.split('-')

        new_date = datetime(int(date_string[0]), int(
            date_string[1]), int(date_string[2]))

        date_string = new_date.strftime("%a") + ', ' + new_date.strftime(
            "%b") + ' ' + str(new_date.day) + ', ' + str(new_date.year)

    return date_string


class ErrorLogs(models.Model):
    user = models.ForeignKey(
        Users, on_delete=models.SET_NULL, related_name='user_in_error_logs', null=True, blank=True)
    expected_error = models.CharField(max_length=500)
    field_error = models.CharField(max_length=500)
    trace_back = models.TextField(max_length=500)
    line_number = models.IntegerField()
    date_recorded = models.DateTimeField(auto_now_add=True)
    browser = models.CharField(max_length=500)
    ip_address = models.CharField(max_length=500)
    os = models.TextField(max_length=500)

    class Meta:
        db_table = 'errorlogs'


class AuditTrials(models.Model):
    user = models.ForeignKey(
        Users, on_delete=models.SET_NULL, related_name='user_in_audit_trial', null=True, blank=True)
    path = models.CharField(max_length=60, null=True, blank=True)
    Actions = models.CharField(max_length=400)
    Module = models.CharField(max_length=400)
    date_of_action = models.DateTimeField(auto_now_add=True)
    operating_system = models.CharField(max_length=200)
    browser = models.CharField(max_length=200)
    ip_address = models.CharField(max_length=200)

    class Meta:
        db_table = 'audittrials'

    def reduceActions(self):
        return f"{self.Actions[:30]}..." if len(self.Actions) > 30 else self.Actions


def getCurrentDate():
    time = datetime.now(timezone.utc)
    return time


class Payments(models.Model):
    user = models.ForeignKey(
        Users, on_delete=models.DO_NOTHING, related_name='user_in_payments')
    amount = models.FloatField(default=0.0)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    cancel_duration = models.IntegerField(default=2)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'payments'
