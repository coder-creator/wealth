from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator

# Import m custom packages
from my_packges.generator import *
from my_packges.random_password import *
from my_packges.logs import *
from my_packges.logs import handle_exceptions

# Import models
from accounts.models import *

@login_required(login_url='login')
@handle_exceptions()
def UsersDashboard(request):
    if not request.user.is_superuser:
        return redirect('finance_dashboard')

    context = {
        'title': 'Users Dashboard',
        'parent': "Dashboard",
    }
    return render(request, 'dashboards/users.html', context)


@login_required(login_url='login')
@handle_exceptions()
def Profile(request):
    context = {
        'title': "Profile",
        "parent": "Accounts"
    }
    return render(request, 'accounts/profile.html', context)

# Accounts List


@login_required(login_url='login')
@handle_exceptions()
def AccountsList(request):
    if not request.user.is_superuser:
        return redirect('finance_dashboard')

    CheckSearchQuery = 'SearchQuery' in request.GET
    CheckDataNumber = 'DataNumber' in request.GET
    DataNumber = 10
    SearchQuery = ''
    UsersList = []

    if CheckDataNumber:
        DataNumber = int(request.GET['DataNumber'])

    UsersList = Users.objects.filter(is_active=True)

    if CheckSearchQuery:
        SearchQuery = request.GET['SearchQuery']
        UsersList = UsersList.filter(Q(username__icontains=SearchQuery) | Q(email__icontains=SearchQuery) | Q(
            first_name__icontains=SearchQuery) | Q(last_name__icontains=SearchQuery) | Q(phone__icontains=SearchQuery), Q(is_superuser=True) | Q(is_admin=True), is_active=True)

    paginator = Paginator(UsersList, DataNumber)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    Pages = paginator.get_elided_page_range(
        page_obj.number, on_each_side=0, on_ends=1)

    context = {
        'paginator_obj': page_obj,
        'SearchQuery': SearchQuery,
        'DataNumber': DataNumber,
        'Pages': Pages,
        'title': 'Users list',
        'parent': "Accounts"
    }

    return render(request, 'accounts/users_list.html', context)
