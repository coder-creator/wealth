from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse
from django.contrib.auth.hashers import check_password
from rest_framework.authtoken.models import Token

# Import models
from accounts.models import *

# Import m custom packages
from my_packges.generator import *
from my_packges.random_password import *
from my_packges.logs import *


@api_view(['POST'])
@handle_exceptions(is_view=False, is_web=False)
def CreateAccount(request):
    first_name = request.data['first_name']
    last_name = request.data['last_name']
    email = request.data['email']
    password = request.data['password']
    confirm_password = request.data['confirm_password']

    # Here we will validate those inputs
    if not validate_input(first_name):
        return JsonResponse(Messages.validation_message("Please enter a first name"))

    if not validate_input(last_name):
        return JsonResponse(Messages.validation_message("Please enter a last name"))

    if not validate_input(email):
        return JsonResponse(Messages.validation_message("Please enter an email"))

    if not validate_email_address(email):
        return JsonResponse(Messages.validation_message("Please enter a valid email"))

    # When the email becomes valid
    # Make it all lowercase
    email = email.lower()

    if not validate_input(password):
        return JsonResponse(Messages.validation_message("Please enter a password"))

    if not validate_input(confirm_password):
        return JsonResponse(Messages.validation_message("Please enter a confirmation password"))

    if password != confirm_password:
        return JsonResponse(Messages.validation_message("Password and confirm password do not match"))

    # Checking if this email is alrrady been used by another user/account
    if Users.objects.filter(email=email).exists():
        return JsonResponse(Messages.validation_message("This email already exists"))

    # Genrate New Username
    last_username = Users.objects.all().order_by('-username').first()

    # By default it will start from 0 , unless username was generated before
    # if so we will increment from there
    serial = 0
    if last_username is not None:
        serial = int(last_username.username[2:])

    serial += 1
    serial = str(serial).zfill(4)

    # Now everything is set up
    # Create the user
    new_account = Users(
        first_name=first_name,
        last_name=last_name,
        email=email,
        username=f"WT{serial}",
        is_client=True,
    )

    # Now set the password for the user
    # generated_password = GeneratePassword(length=10, numbers=True)
    new_account.set_password(password)

    # Save the new account
    new_account.save()

    # Send an email containng the password generated
    message = f'''
    Dear {new_account.get_full_name()},

    Welcome to Wealth Track! We are excited to have you as part of our community. \n Here are your login details:

    Username: {new_account.email}
    Password: {password}

    Please use the provided credentials to log in to our app. \nWe recommend changing your password after the initial login for security purposes.

    If you have any questions or need assistance, feel free to reach out to our support team at somalisoftwareengineer@gmail.com.

    Once again, welcome aboard! \n We look forward to helping you achieve your financial goals.

    Best regards,
    Mansuur Abdullahi
    WealthTrack
    '''

    # We comment this line
    # Becasue it seems that is not necessary to send an email for now
    # SendEmail("Welcome to WealthTrack", message, new_account.email)

    # After email sent and user been created
    # We have to save a trial activity
    module = 'Users'
    action = 'Creating new account/user'
    sendTrials(request, action, module, is_web=False)

    return JsonResponse(
        Messages.success_message(
            "Your new account has been created , and your password has been sent to your email")
    )


@api_view(['POST'])
@handle_exceptions(is_view=False, is_web=False)
def Login(request):
    email = request.data['email']
    password = request.data['password']

    # Validate those values
    if not validate_input(email):
        return JsonResponse(Messages.validation_message("Please enter your email address"))

    if not validate_email_address(email):
        return JsonResponse(Messages.validation_message("Please enter a valid email"))

    # When the email becomes valid
    # Make it all lowercase
    email = email.lower()

    if not validate_input(password):
        return JsonResponse(Messages.validation_message("Please enter your password"))

    # Check if this email is associated with an account
    user = Users.objects.filter(email=email)

    if not user.exists():
        return JsonResponse(Messages.validation_message("Invalid email and password"))

    user = user.first()

    # Now check if the password is correct
    if not check_password(password, user.password):
        return JsonResponse(Messages.validation_message("Invalid email and password"))

    # Generate token for the user
    token, created = Token.objects.get_or_create(user=user)
    token_key = token.key

    # We have to save a trial activity
    module = 'Users'
    action = f"Logged in..."
    sendTrials(request, action, module, is_web=False)

    message = {
        'token': token_key,
        'id': user.id,
        'email': user.email,
        'name': user.get_full_name()[:18] if len(user.get_full_name()) >= 18 else user.get_full_name(),
    }

    return JsonResponse(Messages.success_message(message))
