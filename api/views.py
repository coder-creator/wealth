from rest_framework.decorators import api_view
from django.http import JsonResponse
from rest_framework.authtoken.models import Token
from finance import models as finance
from django.db.models import Q
import random

# Import m custom packages
from my_packges.generator import *
from my_packges.random_password import *
from my_packges.logs import *

@api_view(['POST'])
@handle_exceptions(is_view=False, is_web=False)
def craeteCategory(request):
    action = request.data.get('action')

    if action == 'GetCategory':
        category_id = request.data.get('category_id')
        token = request.data.get('token')
        user_id = request.data.get('user_id')
        category = request.data.get('type')

        if not validate_input(category):
            return JsonResponse(Messages.validation_message("Please specify which category are you saving"))
        
        if category.lower() not in ["income" , "expense" , "saving"]:
            return JsonResponse(Messages.validation_message("Please specify a valid category")) 

        if not validate_input(token):
            return JsonResponse(Messages.validation_message("Please enter your token"))
        
        if not validate_input(user_id):
            return JsonResponse(Messages.validation_message("Please enter your user id"))
        
        # Validate the token
        if not isTokenValid(token , user_id):
            return JsonResponse(Messages.validation_message("Please your token is not valid"))
        

        if not validate_input(category_id):
            category_id = None

        category_args  = {
            'type': category,
            'user__id': user_id,
        }

        if category_id is not None:
            category_args['id'] = category_id

        categories = [ 
            {
                'id': item.id,
                'name': item.name,
                'type': item.type,
                'color': item.color,
                'icon': item.icon,
                'created': item.created_at.strftime("%Y/%m/%d %I:%M %p"),
                'updated': item.modified_at.strftime("%Y/%m/%d %I:%M %p"),
                'currency': format_currency(random.uniform(1 , 50))
            } 
            for index,item in enumerate(finance.category.objects.filter(**category_args))
        ]

        return JsonResponse(Messages.success_message(categories))


    if action == "AddCategory":
        category = request.data.get('type')
        name = request.data.get('name')
        icon = request.data.get('icon')
        color = request.data.get('color')
        token = request.data.get('token')
        user_id = request.data.get('user_id')
        # Validate inputs
        if not validate_input(category):
            return JsonResponse(Messages.validation_message("Please specify which category are you saving"))
        
        if category.lower() not in ["income" , "expense" , "saving"]:
            return JsonResponse(Messages.validation_message("Please specify a valid category")) 
        
        if not validate_input(name):
            return JsonResponse(Messages.validation_message("Please enter category's name"))
        
        if not validate_input(len(name) > 17):
            return JsonResponse(Messages.validation_message("Please category must be at least 17 characters or less than 17"))
        
        if not validate_input(icon):
            return JsonResponse(Messages.validation_message("Please enter category's icon"))
        
        if not validate_input(color):
            return JsonResponse(Messages.validation_message("Please enter category's color"))
        
        if not validate_input(token):
            return JsonResponse(Messages.validation_message("Please enter your token"))
        
        if not validate_input(user_id):
            return JsonResponse(Messages.validation_message("Please enter your user id"))
        
        # Validate the token
        if not isTokenValid(token , user_id):
            return JsonResponse(Messages.validation_message("Please your token is not valid"))
        

        # check if this name is already in the database
        if finance.category.objects.filter(name=name , type = category , user__id = user_id).exists():
            return JsonResponse(Messages.validation_message("This name already exists"))
        
        # create a new category
        new_category = finance.category(
            name = name,
            color = color,
            icon = icon,
            type = category,
            user_id = user_id
        )
        new_category.save()

        # We have to save a trial activity
        module = 'Finance'
        action = f"New category with name {name} and type {type} has been created"
        sendTrials(request, action, module, is_web=False)

        return JsonResponse(Messages.success_message("New category has been created"))


    if action == "EditCategory":
        category = request.data.get('type')
        name = request.data.get('name')
        icon = request.data.get('icon')
        color = request.data.get('color')
        token = request.data.get('token')
        user_id = request.data.get('user_id')
        category_id = request.data.get('category_id')
        # Validate inputs
        if not validate_input(category):
            return JsonResponse(Messages.validation_message("Please specify which category are you saving"))
        
        if category.lower() not in ["income" , "expense" , "saving"]:
            return JsonResponse(Messages.validation_message("Please specify a valid category")) 
        
        if not validate_input(name):
            return JsonResponse(Messages.validation_message("Please enter category's name"))
        
        if not validate_input(len(name) > 17):
            return JsonResponse(Messages.validation_message("Please category must be at least 17 characters or less than 17"))
        
        if not validate_input(icon):
            return JsonResponse(Messages.validation_message("Please enter category's icon"))
        
        if not validate_input(color):
            return JsonResponse(Messages.validation_message("Please enter category's color"))
        
        if not validate_input(token):
            return JsonResponse(Messages.validation_message("Please enter your token"))
        
        if not validate_input(user_id):
            return JsonResponse(Messages.validation_message("Please enter your user id"))
        
        if not validate_input(category_id):
            return JsonResponse(Messages.validation_message("Please enter specify category id"))
        
        # Validate the token
        if not isTokenValid(token , user_id):
            return JsonResponse(Messages.validation_message("Please your token is not valid"))
        

        # check if this name is already in the database
        if finance.category.objects.filter(~Q(id=category_id) ,name=name , type = category , user__id = user_id).exists():
            return JsonResponse(Messages.validation_message("This name already exists"))
        
        # get the instance of the category
        category_instance = finance.category.objects.get(id=category_id)
        category_instance.name = name
        category_instance.color = color
        category_instance.icon = icon
        category_instance.type = category
        category_instance.user_id = user_id
        category_instance.save()

        # We have to save a trial activity
        module = 'Finance'
        action = f"Existing category with name {name} and type {type} has been updated"
        sendTrials(request, action, module, is_web=False)

        return JsonResponse(Messages.success_message("Category has been updated"))


    
    return JsonResponse(Messages.validation_message("Invalid action makes invalid response"))

# Function to check if the token is valid and the user is also a valid user
def isTokenValid(token , user):
    get_token = Token.objects.filter(key=token)

    if not get_token.exists():
        return False
    
    get_token = get_token[0]

    if token != get_token.key:
        return False

    if get_token.user.id != int(user):
        return False
    
    return True


    

