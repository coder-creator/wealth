# Generated by Django 4.2.4 on 2023-08-06 03:31

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('finance', '0010_alter_transactions_sub_type'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='transactions',
            options={'ordering': ['id']},
        ),
        migrations.AddField(
            model_name='transactions',
            name='balance',
            field=models.FloatField(default=0.0),
        ),
        migrations.AlterField(
            model_name='transactions',
            name='month',
            field=models.CharField(default=8, max_length=50),
        ),
        migrations.AlterField(
            model_name='transactions',
            name='transaction_date',
            field=models.DateField(default=datetime.date(2023, 8, 6)),
        ),
    ]
