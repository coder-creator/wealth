from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db.models import Q
from django.core import signing
from datetime import date

# Import models
from accounts.models import *
from accounts.templatetags.main import get_user_accounts, get_users_balance
from .models import *

# Import m custom packages
from my_packges.generator import *
from my_packges.random_password import *
from my_packges.logs import *
from my_packges.logs import handle_exceptions


@login_required(login_url='login')
@handle_exceptions()
def FinanceDashboard(request):

    if request.user.is_superuser:
        return redirect('users_dashboard')

    context = {
        'title': 'Finance Dashboard',
        'parent': 'Dashboard',
    }
    return render(request, 'dashboards/main.html', context)


@login_required(login_url='login')
@handle_exceptions()
def Categories(request):

    if request.user.is_superuser:
        return redirect('users_dashboard')

    args = {}
    search = ''
    category_str = 'all'
    datanumber = 10
    # if the user is supseruser -> fetch all accounts
    # if the user is not superuser -> fetch user's accounts
    if not request.user.is_superuser:
        args['user'] = request.user

    if 'category' in request.GET:
        category_str = request.GET['category']

        if category_str != 'all':
            args['type'] = category_str

    # fetch accounts
    categories_list = category.objects.filter(**args).order_by('-created_at')

    # check if the user sent some search query
    if 'search' in request.GET:
        search = request.GET['search']

        categories_list = categories_list.filter(Q(name__icontains=search) | Q(user__username__icontains=search) | Q(
            user__first_name__icontains=search) | Q(user__last_name__icontains=search))

    if 'rows' in request.GET:
        datanumber = int(request.GET['rows'])

    paginator = Paginator(categories_list, datanumber)
    page_number = request.GET.get('page')
    paginator_obj = paginator.get_page(page_number)

    context = {
        'title': 'Categories',
        'parent': 'Essentials',

        'paginator_obj': paginator_obj,
        'datanumber': datanumber,
        'search': search,
        'category': category_str
    }
    return render(request, 'essentials/categories.html', context)


@login_required(login_url='login')
@handle_exceptions()
def Accounts(request):
    if request.user.is_superuser:
        return redirect('users_dashboard')

    args = {}
    search = ''
    datanumber = 10
    # if the user is supseruser -> fetch all accounts
    # if the user is not superuser -> fetch user's accounts
    if not request.user.is_superuser:
        args['user'] = request.user

    # fetch accounts
    accounts_list = accounts.objects.filter(**args).order_by('-created_at')

    # check if the user sent some search query
    if 'search' in request.GET:
        search = request.GET['search']

        accounts_list = accounts_list.filter(Q(name__icontains=search) | Q(user__username__icontains=search) | Q(
            user__first_name__icontains=search) | Q(user__last_name__icontains=search))

    if 'rows' in request.GET:
        datanumber = int(request.GET['rows'])

    paginator = Paginator(accounts_list, datanumber)
    page_number = request.GET.get('page')
    paginator_obj = paginator.get_page(page_number)

    context = {
        'title': 'Accounts',
        'parent': 'Essentials',

        'paginator_obj': paginator_obj,
        'datanumber': datanumber,
        'search': search,
        'users_balance': get_users_balance(request)

    }
    return render(request, 'essentials/accounts.html', context)


@login_required(login_url='login')
@handle_exceptions()
def AccountDetails(request, id):
    if request.user.is_superuser:
        return redirect('users_dashboard')

    account = accounts.objects.get(id=signing.loads(id))

    args = {
        'account': account,
        'source_account': account
    }
    search = ''
    datanumber = 10
    year = 'all'
    month = 'all'

    # if the user is supseruser -> fetch all accounts
    # if the user is not superuser -> fetch user's accounts
    if not request.user.is_superuser:
        args['user'] = request.user

    query = Q(account=account)

    # Iterate over the dictionary items
    for key, value in args.items():
        if key == 'account' or key == 'source_account':
            query |= Q(**{key: value})
        # Use AND operator for other conditions
        else:
            query &= Q(**{key: value})

    # get the years of the transactions
    years = []
    [
        years.append(item.year)
        for index, item in enumerate(transactions.objects.filter(query))
        if item.year not in years
    ]

    if 'year' in request.GET:
        year = request.GET['year']

    year_args = {}
    if year != 'all':
        year_args['year'] = year

    months = []
    [
        months.append(get_month(int(item.month)))
        for index, item in enumerate(transactions.objects.filter(query, **year_args))
        if get_month(int(item.month)) not in months
    ]

    if 'month' in request.GET:
        month = request.GET['month']

    if month != 'all':
        args['month'] = get_month(month)

    query = Q(account=account)

    # Iterate over the dictionary items
    for key, value in args.items():
        if key == 'account' or key == 'source_account':
            query |= Q(**{key: value})
        # Use AND operator for other conditions
        else:
            query &= Q(**{key: value})

    # fetch accounts
    transactions_list = transactions.get_account_transactions(
        query, request, account=account)

    if 'rows' in request.GET:
        datanumber = int(request.GET['rows'])

    paginator = Paginator(transactions_list, datanumber)
    page_number = request.GET.get('page')
    paginator_obj = paginator.get_page(page_number)

    context = {
        'title': 'Accounts',
        'parent': 'Essentials',

        'paginator_obj': paginator_obj,
        'datanumber': datanumber,
        'search': search,
        'account': account,
        'years': years,
        'months': months,
        'year': year,
        'month': month,
        'id': signing.loads(id),
        'encrypt_id': id,
        'status': account.get_account_balance(
            year if year != 'all' else None,
            get_month(month) if month != 'all' and year != 'all' else None,
        )

    }
    return render(request, 'essentials/account_details.html', context)


@login_required(login_url='login')
@handle_exceptions()
def Deptors(request):
    if request.user.is_superuser:
        return redirect('users_dashboard')

    args = {}
    search = ''
    datanumber = 10
    # if the user is supseruser -> fetch all accounts
    # if the user is not superuser -> fetch user's accounts
    if not request.user.is_superuser:
        args['user'] = request.user

    # fetch accounts
    deptors_list = deptors.objects.filter(**args).order_by('-created_at')

    # check if the user sent some search query
    if 'search' in request.GET:
        search = request.GET['search']

        deptors_list = deptors_list.filter(Q(name__icontains=search) | Q(user__username__icontains=search) | Q(
            user__first_name__icontains=search) | Q(user__last_name__icontains=search))

    if 'rows' in request.GET:
        datanumber = int(request.GET['rows'])

    paginator = Paginator(deptors_list, datanumber)
    page_number = request.GET.get('page')
    paginator_obj = paginator.get_page(page_number)

    context = {
        'title': 'Deptors',
        'parent': 'Essentials',

        'paginator_obj': paginator_obj,
        'datanumber': datanumber,
        'search': search,

    }
    return render(request, 'essentials/deptors.html', context)


@login_required(login_url='login')
@handle_exceptions()
def DeptorDetails(request, id):
    if request.user.is_superuser:
        return redirect('users_dashboard')

    deptor = deptors.objects.get(id=signing.loads(id))

    args = {
        'deptors': deptor,
    }
    search = ''
    datanumber = 10
    year = 'all'
    month = 'all'

    # if the user is supseruser -> fetch all accounts
    # if the user is not superuser -> fetch user's accounts
    if not request.user.is_superuser:
        args['user'] = request.user

    query = Q(deptors=deptor)

    # Iterate over the dictionary items
    for key, value in args.items():
        if key == 'deptors' or key == 'source_account':
            query |= Q(**{key: value})
        # Use AND operator for other conditions
        else:
            query &= Q(**{key: value})

    # get the years of the transactions
    years = []
    [
        years.append(item.year)
        for index, item in enumerate(transactions.objects.filter(query))
        if item.year not in years
    ]

    if 'year' in request.GET:
        year = request.GET['year']

    year_args = {}
    if year != 'all':
        year_args['year'] = year

    months = []
    [
        months.append(get_month(int(item.month)))
        for index, item in enumerate(transactions.objects.filter(query, **year_args))
        if get_month(int(item.month)) not in months
    ]

    if 'month' in request.GET:
        month = request.GET['month']

    if month != 'all':
        args['month'] = get_month(month)

    query = Q(deptors=deptor)

    # Iterate over the dictionary items
    for key, value in args.items():
        if key == 'deptors' or key == 'source_account':
            query |= Q(**{key: value})
        # Use AND operator for other conditions
        else:
            query &= Q(**{key: value})

    # fetch accounts
    transactions_list = transactions.get_deptor_transactions(
        query, request, account=deptor)

    if 'rows' in request.GET:
        datanumber = int(request.GET['rows'])

    paginator = Paginator(transactions_list, datanumber)
    page_number = request.GET.get('page')
    paginator_obj = paginator.get_page(page_number)

    context = {
        'title': 'Accounts',
        'parent': 'Essentials',

        'paginator_obj': paginator_obj,
        'datanumber': datanumber,
        'search': search,
        'deptor': deptor,
        'years': years,
        'months': months,
        'year': year,
        'month': month,
        'id': signing.loads(id),
        'encrypt_id': id,
        'status': deptor.deptor_balance()

    }
    return render(request, 'essentials/deptor_details.html', context)


@login_required(login_url='login')
@handle_exceptions()
def NewTransaction(request):
    if request.user.is_superuser:
        return redirect('users_dashboard')

    context = {
        'title': 'New Transaction',
        'parent': 'Transactions'

    }
    return render(request, 'transactions/new_transaction.html', context)


@login_required(login_url='login')
@handle_exceptions()
def EditTransaction(request, id):
    if request.user.is_superuser:
        return redirect('users_dashboard')

    row = signing.loads(id)

    transaction = transactions.objects.get(id=row)

    context = {
        'title': 'Edit Transaction',
        'parent': 'Transactions',
        'transaction': transaction,
        'private__id': id,
        'private__row': row

    }
    return render(request, 'transactions/edit_transaction.html', context)


@login_required(login_url='login')
@handle_exceptions()
def TransactionList(request):
    if request.user.is_superuser:
        return redirect('users_dashboard')

    args = {}
    search = ''
    datanumber = 5
    type_of_date = ''
    multiple_date = ''
    range_date = ''
    single_date = ''
    transaction_type = ''
    label = ''
    accounts = 'all'

    # if the user is supseruser -> fetch all accounts
    # if the user is not superuser -> fetch user's accounts
    if not request.user.is_superuser:
        args['user'] = request.user

    # in this section we will define and filter the data based
    # on the user's selection
    if 'type_of_date' in request.GET:
        type_of_date = request.GET['type_of_date']

    if 'multiple_date' in request.GET:
        multiple_date = request.GET['multiple_date']

        if type_of_date == 'multiple':
            args['transaction_date__in'] = [convert_string_date(
                date.strip()) for index, date in enumerate(multiple_date.split(','))]

        range_date = request.GET['range_date']

        temp = range_date.split('to')

        if type_of_date == 'range' and len(temp) == 2:
            args['transaction_date__gte'] = convert_string_date(
                temp[0].strip())
            args['transaction_date__lte'] = convert_string_date(
                temp[1].strip())

    if 'single_date' in request.GET:
        single_date = request.GET['single_date']

        if type_of_date == 'single':
            args['transaction_date'] = single_date

    if 'transaction_type' in request.GET:
        transaction_type = request.GET['transaction_type']

        args['sub_type'] = transaction_type

    if 'label' in request.GET:
        label = request.GET['label']

        args['type'] = label

    if 'accounts' in request.GET:
        accounts = request.GET['accounts']

        if accounts != 'all':
            args['account'] = accounts

    # fetch accounts
    transactions_list = transactions.objects.filter(
        **args).order_by('-id')

    # check if the user sent some search query
    if 'search' in request.GET:
        search = request.GET['search']

        transactions_list = transactions_list.filter(
            Q(category__name__icontains=search) |
            Q(deptors__name__icontains=search) |
            Q(amount__icontains=search) |
            Q(transaction_date__icontains=search)
        )

    if 'rows' in request.GET:
        datanumber = int(request.GET['rows'])

    paginator = Paginator(transactions_list, datanumber)
    page_number = request.GET.get('page')
    paginator_obj = paginator.get_page(page_number)

    context = {
        'title': 'View As List',
        'parent': 'Transactions',

        'paginator_obj': paginator_obj,
        'datanumber': datanumber,
        'search': search,

        # filters
        'type_of_date': type_of_date,
        'multiple_date': multiple_date.split(','),
        'range_date': range_date.split('to'),
        'single_date': single_date,
        'transaction_type': transaction_type,
        'accountslist': get_user_accounts(request),
        'accounts': accounts,
        'users_balance': get_users_balance(request)

    }
    return render(request, 'transactions/list.html', context)


@login_required(login_url='login')
@handle_exceptions()
def ViewAsExcel(request):
    if request.user.is_superuser:
        return redirect('users_dashboard')

    args = {}
    year = '2023'
    # if the user is supseruser -> fetch all accounts
    # if the user is not superuser -> fetch user's accounts
    if not request.user.is_superuser:
        args['user'] = request.user

    context = {
        'title': 'View As Excel',
        'parent': 'Transactions',
        'summary': transactions.generate_excel_data(year, args)

    }
    return render(request, 'transactions/view_as_excel.html', context)
