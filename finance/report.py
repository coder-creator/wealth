from urllib.parse import urlencode
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db.models import Q
from django.core import signing
from datetime import date


# Import models
from accounts.models import *
from accounts.templatetags.main import get_user_accounts, get_users_balance
from .models import *

# Import m custom packages
from my_packges.generator import *
from my_packges.random_password import *
from my_packges.logs import *
from my_packges.logs import handle_exceptions


@login_required(login_url='login')
@handle_exceptions()
def SpendingReport(request):
    if request.user.is_superuser:
        return redirect('users_dashboard')

    args = {
        'type': 'withdraw',
        'sub_type': 'normal'
    }
    g_args = {}
    search = ''
    datanumber = 5
    type_of_date = ''
    multiple_date = ''
    range_date = ''
    single_date = ''
    years = []
    months = []
    account = 'all'

    # if the user is supseruser -> fetch all accounts
    # if the user is not superuser -> fetch user's accounts
    if not request.user.is_superuser:
        args['user'] = request.user
        g_args['user'] = request.user

    
    # get all years from transactions
    all_years = []

    [
        all_years.append(year['year'])
        for index, year in enumerate(transactions.objects.filter(**args).values('year'))
        if year['year'] not in all_years
    ]

    # in this section we will define and filter the data based
    # on the user's selection
    if 'type_of_date' in request.GET:
        type_of_date = request.GET['type_of_date']

    if 'multiple_date' in request.GET:
        multiple_date = request.GET['multiple_date']

        if type_of_date == 'multiple':
            args['transaction_date__in'] = [convert_string_date(
                date.strip()) for index, date in enumerate(multiple_date.split(','))]

    if 'range_date' in request.GET:
        range_date = request.GET['range_date']

        temp = range_date.split('to')

        if type_of_date == 'range' and len(temp) == 2:
            args['transaction_date__gte'] = convert_string_date(
                temp[0].strip())
            args['transaction_date__lte'] = convert_string_date(
                temp[1].strip())

    if 'single_date' in request.GET:
        single_date = request.GET['single_date']

        if type_of_date == 'single':
            args['transaction_date'] = single_date
    
    if 'years' in request.GET:
        years = request.GET['years']
        years = [year for year in years.split(',')]

        if type_of_date == 'year_month':
            args['year__in'] = years
    
    if 'months' in request.GET:
        months = request.GET['months']
        months = [year for year in months.split(',')]

        if type_of_date == 'year_month':
            args['month__in'] = months

    if 'account' in request.GET:
        account = request.GET['account']
        if account != 'all':
            args['account_id'] = account

    # fetch accounts
    transactions_list = transactions.objects.filter(
        **args).order_by('-id')

    # check if the user sent some search query
    if 'search' in request.GET:
        search = request.GET['search']

        transactions_list = transactions_list.filter(
            Q(category__name__icontains=search))

    categories = []
    [
        categories.append(item.category.name)
        for index, item in enumerate(transactions_list)
        if item.category.name not in categories
    ]

    result = [
        {
            'category': category.objects.get(name=item, type='expense'),
            'amount': round(transactions.objects.filter(**args, category__name=item).aggregate(sum_amount=functions.Coalesce(Sum('amount'), 0.0))['sum_amount'], 2),
        }
        for index, item in enumerate(categories)
    ]

    result = sorted(result, key=lambda x: x['amount'], reverse=True)
    total_amount = round(sum(item["amount"] for item in result),2)

    if 'rows' in request.GET:
        datanumber = int(request.GET['rows'])

    paginator = Paginator(result, datanumber)
    page_number = request.GET.get('page')
    paginator_obj = paginator.get_page(page_number)

    params = request.GET.copy()
    params = remove_item_from_dict(params, ['page', 'rows'])

    # fetch accounts
    accounts_list = accounts.objects.filter(**g_args).order_by('-created_at')

    context = {
        'title': 'Spending Report',
        'parent': 'Reports',

        'paginator_obj': paginator_obj,
        'datanumber': datanumber,
        'search': search,
        'total_amount':total_amount,
        'accounts_list':accounts_list,
        'account': account,

        # filters
        'type_of_date': type_of_date,
        'multiple_date': multiple_date.split(','),
        'range_date': range_date.split('to'),
        'single_date': single_date,
        'all_years':all_years,
        'years':years,
        'months':months,

        # url
        'url': urlencode(params)

    }
    return render(request, 'reports/spending_report.html', context)


@login_required(login_url='login')
@handle_exceptions()
def SpendingDetail(request):
    if request.user.is_superuser:
        return redirect('users_dashboard')

    args = {
        'type': 'withdraw',
        'sub_type': 'normal'
    }
    search = ''
    datanumber = 5
    type_of_date = ''
    multiple_date = ''
    range_date = ''
    single_date = ''
    account = 'all'

    # if the user is supseruser -> fetch all accounts
    # if the user is not superuser -> fetch user's accounts
    if not request.user.is_superuser:
        args['user'] = request.user

    if 'category' not in request.GET:
        return redirect('spending_report')

    _category = request.GET.get('category')
    args['category__id'] = _category

    report_filter = {
        'category': category.objects.get(id=_category).name,
        'date': 'No date specified',
        'total': 0
    }

    # in this section we will define and filter the data based
    # on the user's selection
    if 'type_of_date' in request.GET:
        type_of_date = request.GET['type_of_date']

    if 'multiple_date' in request.GET:
        multiple_date = request.GET['multiple_date']
        report_filter['date'] = multiple_date

        if type_of_date == 'multiple':
            args['transaction_date__in'] = [convert_string_date(
                date.strip()) for index, date in enumerate(multiple_date.split(','))]

    if 'range_date' in request.GET:
        range_date = request.GET['range_date']
        report_filter['date'] = range_date

        temp = range_date.split('to')

        if type_of_date == 'range' and len(temp) == 2:
            args['transaction_date__gte'] = convert_string_date(
                temp[0].strip())
            args['transaction_date__lte'] = convert_string_date(
                temp[1].strip())

    if 'single_date' in request.GET:
        single_date = request.GET['single_date']
        report_filter['date'] = single_date

        if type_of_date == 'single':
            args['transaction_date'] = single_date

    if 'years' in request.GET:
        years = request.GET['years']
        years = [year for year in years.split(',')]
        report_filter['date'] = "Year && Month"
        report_filter['year'] = ",".join(years)

        if type_of_date == 'year_month':
            args['year__in'] = years
    
    if 'months' in request.GET:
        months = request.GET['months']
        months = [year for year in months.split(',')]
        report_filter['date'] = "Year && Month"
        report_filter['month'] = ",".join([get_month(int(x)) for x in months])


        if type_of_date == 'year_month':
            args['month__in'] = months

    if 'account' in request.GET:
        account = request.GET['account']
        if account != 'all':
            args['account_id'] = account
            account = accounts.objects.get(id=account)

    # fetch accounts
    transactions_list = transactions.objects.filter(
        **args).order_by('-id')

    # check if the user sent some search query
    if 'search' in request.GET:
        search = request.GET['search']

        transactions_list = transactions_list.filter(
            Q(category__name__icontains=search))

    if 'rows' in request.GET:
        datanumber = int(request.GET['rows'])

    report_filter['total'] = transactions_list.aggregate(
        total=functions.Coalesce(Sum('amount'), 0.0))['total']

    paginator = Paginator(transactions_list, datanumber)
    page_number = request.GET.get('page')
    paginator_obj = paginator.get_page(page_number)

    params = request.GET.copy()
    params = remove_item_from_dict(params, ['page', 'rows', 'search'])
    url = urlencode(params)
    params = remove_item_from_dict(params, ['category'])
    back_url = urlencode(params)

    context = {
        'title': 'Spending Report',
        'parent': 'Reports',

        'paginator_obj': paginator_obj,
        'datanumber': datanumber,
        'search': search,
        'account':account,

        # url
        'url': url,
        'back_url': back_url,
        'report_filter': report_filter
    }
    return render(request, 'reports/spending_detail.html', context)


@login_required(login_url='login')
@handle_exceptions()
def EarningReport(request):
    if request.user.is_superuser:
        return redirect('users_dashboard')

    args = {
        'type': 'deposit',
        'sub_type': 'normal'
    }
    search = ''
    datanumber = 5
    type_of_date = ''
    multiple_date = ''
    range_date = ''
    single_date = ''

    # if the user is supseruser -> fetch all accounts
    # if the user is not superuser -> fetch user's accounts
    if not request.user.is_superuser:
        args['user'] = request.user

    # in this section we will define and filter the data based
    # on the user's selection
    if 'type_of_date' in request.GET:
        type_of_date = request.GET['type_of_date']

    if 'multiple_date' in request.GET:
        multiple_date = request.GET['multiple_date']

        if type_of_date == 'multiple':
            args['transaction_date__in'] = [convert_string_date(
                date.strip()) for index, date in enumerate(multiple_date.split(','))]

    if 'range_date' in request.GET:
        range_date = request.GET['range_date']

        temp = range_date.split('to')

        if type_of_date == 'range' and len(temp) == 2:
            args['transaction_date__gte'] = convert_string_date(
                temp[0].strip())
            args['transaction_date__lte'] = convert_string_date(
                temp[1].strip())

    if 'single_date' in request.GET:
        single_date = request.GET['single_date']

        if type_of_date == 'single':
            args['transaction_date'] = single_date

    # fetch accounts
    transactions_list = transactions.objects.filter(
        **args).order_by('-id')

    # check if the user sent some search query
    if 'search' in request.GET:
        search = request.GET['search']

        transactions_list = transactions_list.filter(
            Q(category__name__icontains=search))

    categories = []
    [
        categories.append(item.category.name)
        for index, item in enumerate(transactions_list)
        if item.category.name not in categories
    ]

    result = [
        {
            'category': category.objects.get(name=item, type='income'),
            'amount': round(transactions.objects.filter(**args, category__name=item).aggregate(sum_amount=functions.Coalesce(Sum('amount'), 0.0))['sum_amount'], 2),
        }
        for index, item in enumerate(categories)
    ]

    result = sorted(result, key=lambda x: x['amount'], reverse=True)
    total_amount = sum(item["amount"] for item in result)

    if 'rows' in request.GET:
        datanumber = int(request.GET['rows'])

    paginator = Paginator(result, datanumber)
    page_number = request.GET.get('page')
    paginator_obj = paginator.get_page(page_number)

    params = request.GET.copy()
    params = remove_item_from_dict(params, ['page', 'rows'])

    context = {
        'title': 'Earning Report',
        'parent': 'Reports',

        'paginator_obj': paginator_obj,
        'datanumber': datanumber,
        'search': search,
        'total_amount':total_amount,

        # filters
        'type_of_date': type_of_date,
        'multiple_date': multiple_date.split(','),
        'range_date': range_date.split('to'),
        'single_date': single_date,

        # url
        'url': urlencode(params)

    }
    return render(request, 'reports/earning_report.html', context)


@login_required(login_url='login')
@handle_exceptions()
def EarningDetail(request):
    if request.user.is_superuser:
        return redirect('users_dashboard')

    args = {
        'type': 'deposit',
        'sub_type': 'normal'
    }
    search = ''
    datanumber = 5
    type_of_date = ''
    multiple_date = ''
    range_date = ''
    single_date = ''

    # if the user is supseruser -> fetch all accounts
    # if the user is not superuser -> fetch user's accounts
    if not request.user.is_superuser:
        args['user'] = request.user

    if 'category' not in request.GET:
        return redirect('spending_report')

    _category = request.GET.get('category')
    args['category__id'] = _category

    report_filter = {
        'category': category.objects.get(id=_category).name,
        'date': 'No date specified',
        'total': 0
    }

    # in this section we will define and filter the data based
    # on the user's selection
    if 'type_of_date' in request.GET:
        type_of_date = request.GET['type_of_date']

    if 'multiple_date' in request.GET:
        multiple_date = request.GET['multiple_date']
        report_filter['date'] = multiple_date

        if type_of_date == 'multiple':
            args['transaction_date__in'] = [convert_string_date(
                date.strip()) for index, date in enumerate(multiple_date.split(','))]

    if 'range_date' in request.GET:
        range_date = request.GET['range_date']
        report_filter['date'] = range_date

        temp = range_date.split('to')

        if type_of_date == 'range' and len(temp) == 2:
            args['transaction_date__gte'] = convert_string_date(
                temp[0].strip())
            args['transaction_date__lte'] = convert_string_date(
                temp[1].strip())

    if 'single_date' in request.GET:
        single_date = request.GET['single_date']
        report_filter['date'] = single_date

        if type_of_date == 'single':
            args['transaction_date'] = single_date

    # fetch accounts
    transactions_list = transactions.objects.filter(
        **args).order_by('-id')

    # check if the user sent some search query
    if 'search' in request.GET:
        search = request.GET['search']

        transactions_list = transactions_list.filter(
            Q(category__name__icontains=search))

    if 'rows' in request.GET:
        datanumber = int(request.GET['rows'])

    report_filter['total'] = transactions_list.aggregate(
        total=functions.Coalesce(Sum('amount'), 0.0))['total']

    paginator = Paginator(transactions_list, datanumber)
    page_number = request.GET.get('page')
    paginator_obj = paginator.get_page(page_number)

    params = request.GET.copy()
    params = remove_item_from_dict(params, ['page', 'rows', 'search'])
    url = urlencode(params)
    params = remove_item_from_dict(params, ['category'])
    back_url = urlencode(params)

    context = {
        'title': 'Earning Report',
        'parent': 'Reports',

        'paginator_obj': paginator_obj,
        'datanumber': datanumber,
        'search': search,

        # url
        'url': url,
        'back_url': back_url,
        'report_filter': report_filter
    }
    return render(request, 'reports/earning_detail.html', context)
