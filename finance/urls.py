from django.urls import path
from .views import *
from .essentails import *
from .dashboard import *
from .report import *



urlpatterns = [
    # dashboards
    path('', FinanceDashboard, name="finance_dashboard"),

    # essentials
    path('accounts', Accounts, name="accounts"),
    path('account_details/<str:id>' , AccountDetails , name='account_details'),
    path('deptors', Deptors, name="deptors"),
    path('deptor_details/<str:id>' , DeptorDetails , name='deptor_details'),
    path('categories', Categories, name="categories"),
    path('view_as_list' , TransactionList , name='view_as_list'),
    # path('view_as_excel' , ViewAsExcel , name='view_as_excel'),
    path('new_transaction' , NewTransaction , name='new_transaction'),
    path('edit_transaction/<str:id>' , EditTransaction , name='edit_transaction'),

    # reports
    path('spending_report' , SpendingReport , name='spending_report'),
    path('spending_detail' , SpendingDetail , name='spending_detail'),
    path('earning_report' , EarningReport , name='earning_report'),
    path('earning_detail' , EarningDetail , name='earning_detail'),

    # management
    path('manage_essentials/<str:action>', ManageEssentails),
    path('manage_dashboards/<str:action>', ManageDashboard),
]
