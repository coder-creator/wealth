from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.core import signing

# Import models
from accounts.models import *
from .models import *

# Import m custom packages
from my_packges.generator import *
from my_packges.random_password import *
from my_packges.logs import *
from my_packges.logs import handle_exceptions


@handle_exceptions(is_view=False)
@handle_auth()
def ManageEssentails(request, action):
    if request.method == "POST":

        # Accounts
        if action == "CreateAccount":
            account_name = request.POST.get('account_name')

            if not validate_input(account_name):
                return JsonResponse(Messages.validation_message("Please enter an account name"))

            # check if this accout name already exists
            if accounts.objects.filter(name=account_name, user=request.user).exists():
                return JsonResponse(Messages.validation_message("Already an account with this name exists"))

            # now save the account to the database
            new_account = accounts(name=account_name, user=request.user)
            new_account.save()

            # We have to save a trial activity
            module = 'Accounts'
            action = f'{request.user} has created new account with this name {account_name}'
            sendTrials(request, action, module)

            return JsonResponse(
                Messages.success_message(
                    "Your new account has been created")
            )

        if action == "GetAccount":
            row = request.POST.get('row')

            if not validate_input(row):
                return JsonResponse(Messages.validation_message("Please select an account to perform editing"))

            # get the account by the row id provided
            account = accounts.objects.get(id=row)

            return JsonResponse(Messages.success_message({'id': account.id, 'name': account.name}))

        if action == "EditAccount":
            account_name = request.POST.get('account_name')
            account_id = request.POST.get('account_id')

            if not validate_input(account_id):
                return JsonResponse(Messages.validation_message("Please select an account to perform editing"))

            if not validate_input(account_name):
                return JsonResponse(Messages.validation_message("Please enter an account name"))

            # check if this accout name already exists
            if accounts.objects.filter(~Q(id=account_id), name=account_name, user=request.user).exists():
                return JsonResponse(Messages.validation_message("Already an account with this name exists"))

            # now save the account to the database
            accounts.objects.filter(id=account_id).update(name=account_name)

            # We have to save a trial activity
            module = 'Accounts'
            action = f'{request.user} has updated an account with this name {account_name}'
            sendTrials(request, action, module)

            return JsonResponse(
                Messages.success_message(
                    "Your account has been updated")
            )

        # Deptors
        if action == "CreateDeptor":
            deptor_name = request.POST.get('deptor_name')
            deptor_phone = request.POST.get('deptor_phone')

            if not validate_input(deptor_name):
                return JsonResponse(Messages.validation_message("Please enter a deptor name"))
            
            if not validate_input(deptor_name):
                deptor_phone = None

            # check if this accout name already exists
            if deptors.objects.filter(name=deptor_name, user=request.user).exists():
                return JsonResponse(Messages.validation_message("Already a deptor with this name exists"))

            # now save the deptor to the database
            new_deptor = deptors(name=deptor_name, phone=deptor_phone  ,user=request.user)
            new_deptor.save()

            # We have to save a trial activity
            module = 'Deptors'
            action = f'{request.user} has created new deptor with this name {deptor_name}'
            sendTrials(request, action, module)

            return JsonResponse(
                Messages.success_message(
                    "Your new deptor has been created")
            )

        if action == "GetDeptor":
            row = request.POST.get('row')

            if not validate_input(row):
                return JsonResponse(Messages.validation_message("Please select a deptor to perform editing"))

            # get the deptor by the row id provided
            deptor = deptors.objects.get(id=row)

            return JsonResponse(Messages.success_message({'id': deptor.id, 'name': deptor.name , 'phone': deptor.phone}))

        if action == "EditDeptor":
            deptor_name = request.POST.get('deptor_name')
            deptor_id = request.POST.get('deptor_id')
            deptor_phone = request.POST.get('deptor_phone')
            
            if not validate_input(deptor_name):
                deptor_phone = None

            if not validate_input(deptor_id):
                return JsonResponse(Messages.validation_message("Please select a deptor to perform editing"))

            if not validate_input(deptor_name):
                return JsonResponse(Messages.validation_message("Please enter a deptor name"))

            # check if this accout name already exists
            if deptors.objects.filter(~Q(id=deptor_id), name=deptor_name, user=request.user).exists():
                return JsonResponse(Messages.validation_message("Already a deptor with this name exists"))

            # now save the deptor to the database
            deptors.objects.filter(id=deptor_id).update(name=deptor_name , phone=deptor_phone)

            # We have to save a trial activity
            module = 'Deptors'
            action = f'{request.user} has updated an deptor with this name {deptor_name}'
            sendTrials(request, action, module)

            return JsonResponse(
                Messages.success_message(
                    "Your deptor has been updated")
            )

        # Categories
        if action == "CreateCategory":
            category_type = request.POST.get('category_type')
            category_name = request.POST.get('category_name')

            if not validate_input(category_type):
                return JsonResponse(Messages.validation_message("Please select a category type"))

            if category_type not in ["income", "expense", "saving"]:
                return JsonResponse(Messages.validation_message("This category not known at WealthTrack"))

            if not validate_input(category_name):
                return JsonResponse(Messages.validation_message("Please enter a category name"))

            # check if this accout name already exists
            if category.objects.filter(name=category_name, user=request.user, type=category_type).exists():
                return JsonResponse(Messages.validation_message("Already a category with this name exists"))

            # now save the deptor to the database
            new_category = category(
                name=category_name, type=category_type, user=request.user)
            new_category.save()

            # We have to save a trial activity
            module = 'Categories'
            action = f'{request.user} has created new category with this name {category_name}'
            sendTrials(request, action, module)

            return JsonResponse(
                Messages.success_message(
                    "Your new category has been created")
            )

        if action == "GetCategory":
            row = request.POST.get('row')

            if not validate_input(row):
                return JsonResponse(Messages.validation_message("Please select a category to perform editing"))

            # get the deptor by the row id provided
            category_instance = category.objects.get(id=row)

            return JsonResponse(Messages.success_message({'id': category_instance.id, 'name': category_instance.name, 'type': category_instance.type}))

        if action == "EditCategory":
            category_type = request.POST.get('category_type')
            category_name = request.POST.get('category_name')
            category_id = request.POST.get('category_id')

            if not validate_input(category_id):
                return JsonResponse(Messages.validation_message("Please select a deptor to perform editing"))

            if not validate_input(category_type):
                return JsonResponse(Messages.validation_message("Please select a category type"))

            if category_type not in ["income", "expense", "saving"]:
                return JsonResponse(Messages.validation_message("This category not known at WealthTrack"))

            if not validate_input(category_name):
                return JsonResponse(Messages.validation_message("Please enter a deptor name"))

            # check if this accout name already exists
            if category.objects.filter(~Q(id=category_id), name=category_name, type=category_type, user=request.user).exists():
                return JsonResponse(Messages.validation_message("Already a deptor with this name exists"))

            # now save the deptor to the database
            category.objects.filter(id=category_id).update(
                name=category_name, type=category_type)

            # We have to save a trial activity
            module = 'Categories'
            action = f'{request.user} has updated a category with this name {category_name}'
            sendTrials(request, action, module)

            return JsonResponse(
                Messages.success_message(
                    "Your category has been updated")
            )

        if action == "GetCategories":
            type = request.POST.get('type')

            if not validate_input(type):
                return JsonResponse(Messages.validation_message("Please select the transaction type"))

            args = {
                'type': type,
            }

            if not request.user.is_superuser:
                args['user'] = request.user

            result = [
                {
                    'id': item.id,
                    'name': item.name
                }
                for index, item in enumerate(category.objects.filter(**args))
            ]

            return JsonResponse(Messages.success_message(result))

        # Transaction
        # -- UPDATE transactions set sub_type = 'return_dept' where sub_type = 'return_dept';
        # -- UPDATE transactions set sub_type = 'give_dept' where sub_type = 'give_dept';
        # -- UPDATE transactions set sub_type = 'take_dept' where sub_type = 'take_dept';
        # -- UPDATE transactions set sub_type = 'pay_dept' where sub_type = 'pay_dept';

        if action == 'SubmitTransaction':
            transaction_type = request.POST.get('transaction_type')
            transaction_source = request.POST.get('transaction_source')
            transaction_category = request.POST.get('transaction_category')
            transaction_deptor = request.POST.get('transaction_deptor')
            transaction_account = request.POST.get('transaction_account')
            to_account = request.POST.get('to_account')
            transaction_amount = request.POST.get('transaction_amount')
            transaction_date = request.POST.get('transaction_date')

            deptor_fields = ['return_dept',
                             'take_dept', 'give_dept', 'pay_dept']

            if not validate_input(transaction_type) and transaction_type not in ['saving', 'expense', 'income', 'transfer']:
                return JsonResponse(Messages.validation_message("Unknown transaction"))

            if transaction_type in ['income', 'expense'] and not validate_input(transaction_source):
                return JsonResponse(Messages.validation_message("Please select transaction source"))

            if not validate_input(transaction_category):
                if transaction_type in ['income', 'expense'] and transaction_source == 'category':
                    return JsonResponse(Messages.validation_message("Please select transaction category"))
                if transaction_type == 'saving':
                    return JsonResponse(Messages.validation_message("Please select transaction category"))

            if transaction_type in ['income', 'expense'] and transaction_source in deptor_fields and not validate_input(transaction_deptor):
                return JsonResponse(Messages.validation_message("Please select transaction deptor"))

            if not validate_input(transaction_account):
                return JsonResponse(Messages.validation_message(f"Please select transaction account"))

            if not validate_input(to_account) and transaction_type in ['transfer', 'saving']:
                return JsonResponse(Messages.validation_message(f"Please select transaction to_account"))

            if to_account == transaction_account and transaction_type in ['transfer', 'saving']:
                return JsonResponse(Messages.validation_message(f"From account and To account must be not the same"))
            

            if not validate_input(transaction_amount):
                return JsonResponse(Messages.validation_message("Enter transaction amount"))

            transaction_amount = float(transaction_amount)

            if transaction_amount <= 0:
                return JsonResponse(Messages.validation_message("Transaction must be greater than zero"))

            if not validate_input(transaction_date):
                return JsonResponse(Messages.validation_message("Enter transaction date"))

            if len(transaction_date.split('-')) < 2 and len(transaction_date) > 2:
                return JsonResponse(Messages.validation_message("Enter valid transaction date"))

            # Get account transaction instance
            transaction_account = accounts.objects.get(id=transaction_account)

            # Checking if the transaction is an expense and the balance of the selected account is not enough
            if transaction_type != 'income' and transaction_amount > transaction_account.get_account_balance()['balance']:
                return JsonResponse(Messages.validation_message(f"Your account '{transaction_account.name}' balance is not enough"))

            # Transaction args for the query
            args = {
                'user': request.user,
                'amount': transaction_amount,
                'transaction_date': transaction_date,
                'year': transaction_date.split('-')[0],
                'month': str(int(transaction_date.split('-')[1])),
                'account': transaction_account
            }

            if transaction_type == 'income':
                args['type'] = 'deposit'
            else:
                args['type'] = 'withdraw'

            if transaction_type in ['saving', 'transfer']:
                args['sub_type'] = transaction_type
            else:
                args['sub_type'] = transaction_source if transaction_source != 'category' else 'normal'

            if transaction_type == 'saving':
                args['category_id'] = transaction_category

            if transaction_type in ['income', 'expense'] and transaction_source == 'category':
                args['category_id'] = transaction_category

            if transaction_type in ['transfer', 'saving']:
                args['source_account_id'] = to_account

            # Also, I want to check about deptors
            # Before any checking, get the instance of the deptor
            if transaction_type in ['income', 'expense'] and transaction_source in deptor_fields:
                transaction_deptor = deptors.objects.get(
                    id=transaction_deptor, user=request.user)
                args['deptors'] = transaction_deptor

                # 1. When returning dept
                #    - If dept has been taken
                #    - If the amount returning is greater than the amount taken
                if transaction_source == 'return_dept':
                    # Get the deptor's balance
                    deptor_balance = transaction_deptor.deptor_balance()['balance']

                    if deptor_balance == 0.0:
                        return JsonResponse(Messages.validation_message(f"{transaction_deptor.name} has not taken any money from you, there is nothing to return"))

                    if transaction_amount > deptor_balance:
                        return JsonResponse(Messages.validation_message(f"The amount you should return from {transaction_deptor.name} is ${abs(deptor_balance)}, please check your amount"))

                # 2. When paying back dept
                #    - If dept has been taken
                #    - If the amount returning is greater than the amount taken
                if transaction_source == 'pay_dept':
                    # Get the deptor's balance
                    deptor_balance = transaction_deptor.deptor_balance()['balance']

                    if deptor_balance == 0.0:
                        return JsonResponse(Messages.validation_message(f"You didn't take money from {transaction_deptor.name}, there is nothing to payback"))

                    if transaction_amount > abs(deptor_balance):
                        return JsonResponse(Messages.validation_message(f"The amount you should payback to {transaction_deptor.name} is ${abs(deptor_balance)}, please check your amount"))

            transaction_query = transactions(**args)
            transaction_query.save()

            return JsonResponse(Messages.success_message("Your transaction has been saved"))

        if action == 'EditTransaction':
            transaction_type = request.POST.get('transaction_type')
            transaction_source = request.POST.get('transaction_source')
            transaction_category = request.POST.get('transaction_category')
            transaction_deptor = request.POST.get('transaction_deptor')
            transaction_account = request.POST.get('transaction_account')
            to_account = request.POST.get('to_account')
            transaction_amount = request.POST.get('transaction_amount')
            transaction_date = request.POST.get('transaction_date')
            row = request.POST.get('row')

            if not validate_input(row):
                return JsonResponse(Messages.validation_message("Unknown transaction action"))

            deptor_fields = ['return_dept',
                             'take_dept', 'give_dept', 'pay_dept']

            if not validate_input(transaction_type) and transaction_type not in ['saving', 'expense', 'income', 'transfer']:
                return JsonResponse(Messages.validation_message("Unknown transaction"))

            if transaction_type in ['income', 'expense'] and not validate_input(transaction_source):
                return JsonResponse(Messages.validation_message("Please select transaction source"))

            if not validate_input(transaction_category):
                if transaction_type in ['income', 'expense'] and transaction_source == 'category':
                    return JsonResponse(Messages.validation_message("Please select transaction category"))
                if transaction_type == 'saving':
                    return JsonResponse(Messages.validation_message("Please select transaction category"))

            if transaction_type in ['income', 'expense'] and transaction_source in deptor_fields and not validate_input(transaction_deptor):
                return JsonResponse(Messages.validation_message("Please select transaction deptor"))

            if not validate_input(transaction_account):
                return JsonResponse(Messages.validation_message(f"Please select transaction account"))

            if not validate_input(to_account) and transaction_type in ['transfer', 'saving']:
                return JsonResponse(Messages.validation_message(f"Please select transaction to_account"))

            if not validate_input(transaction_amount):
                return JsonResponse(Messages.validation_message("Enter transaction amount"))

            transaction_amount = float(transaction_amount)

            if transaction_amount <= 0:
                return JsonResponse(Messages.validation_message("Transaction must be greater than zero"))

            if not validate_input(transaction_date):
                return JsonResponse(Messages.validation_message("Enter transaction date"))

            if len(transaction_date.split('-')) < 2 and len(transaction_date) > 2:
                return JsonResponse(Messages.validation_message("Enter valid transaction date"))

            # Get account transaction instance
            transaction_account = accounts.objects.get(id=transaction_account)
            transaction = transactions.objects.get(id=row)

            # Checking if the transaction is an expense and the balance of the selected account is not enough
            # Also, it will be applied when giving_dept and paying back dept
            if transaction_type != 'income' and transaction_amount > transaction_account.get_account_balance()['balance'] + transaction.amount:
                return JsonResponse(Messages.validation_message(f"Your account '{transaction_account.name}' balance is not enough"))

            # Transaction args for the query
            args = {
                'user': request.user,
                'amount': transaction_amount,
                'transaction_date': transaction_date,
                'year': transaction_date.split('-')[0],
                'month': str(int(transaction_date.split('-')[1])),
                'account': transaction_account
            }

            if transaction_type == 'income':
                args['type'] = 'deposit'
            else:
                args['type'] = 'withdraw'

            if transaction_type in ['saving', 'transfer']:
                args['sub_type'] = transaction_type
            else:
                args['sub_type'] = transaction_source if transaction_source != 'category' else 'normal'

            if transaction_type == 'saving':
                args['category_id'] = transaction_category

            if transaction_type in ['income', 'expense'] and transaction_source == 'category':
                args['category_id'] = transaction_category

            if transaction_type in ['transfer', 'saving']:
                args['source_account_id'] = to_account

            # Also, I want to check about deptors
            # Before any checking, get the instance of the deptor
            if transaction_type in ['income', 'expense'] and transaction_source in deptor_fields:
                transaction_deptor = deptors.objects.get(
                    id=transaction_deptor, user=request.user)
                args['deptors'] = transaction_deptor

                # 1. When returning dept
                #    - If dept has been taken
                #    - If the amount returning is greater than the amount taken
                if transaction_source == 'return_dept':
                    # Get the deptor's balance
                    deptor_balance = transaction_deptor.deptor_balance()['balance']

                    if deptor_balance == 0.0:
                        return JsonResponse(Messages.validation_message(f"{transaction_deptor.name} has not taken any money from you, there is nothing to return"))

                    if transaction_amount > deptor_balance:
                        return JsonResponse(Messages.validation_message(f"The amount you should return to {transaction_deptor.name} is ${abs(deptor_balance)}, please check your amount"))

                # 2. When paying back dept
                #    - If dept has been taken
                #    - If the amount returning is greater than the amount taken
                if transaction_source == 'pay_dept':
                    # Get the deptor's balance
                    deptor_balance = transaction_deptor.deptor_balance()['balance']

                    if deptor_balance == 0.0:
                        return JsonResponse(Messages.validation_message(f"You didn't take money from {transaction_deptor.name}, there is nothing to payback"))

                    if transaction_amount > abs(deptor_balance):
                        return JsonResponse(Messages.validation_message(f"The amount you should payback to {transaction_deptor.name} is ${abs(deptor_balance)}, please check your amount"))

            transaction_query = transactions.objects.filter(
                id=row).update(**args)

            return JsonResponse(Messages.success_message("Your transaction has been updated"))
