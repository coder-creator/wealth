from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.core import signing
from django.db.models import Sum, FloatField, functions, Q

# Import models
from accounts.models import *
from . import models as finance_models

# Import m custom packages
from my_packges.generator import *
from my_packges.random_password import *
from my_packges.logs import *


@login_required(login_url='login')
@handle_exceptions(is_view=False)
def ManageDashboard(request, action):

    args = {}
    if not request.user.is_superuser:
        args['user'] = request.user

    if request.method == 'POST':
        if action == 'TransactionYears':
            data = []
            [
                data.append(item['year'])
                for index, item in enumerate(finance_models.transactions.objects.filter(**args).values('year').distinct())
                if item['year'] not in data
            ]
            return JsonResponse({"message": data})

        if action == 'TransactionMonths':
            year = request.POST.get('year', None)
            year_args = {}

            if validate_input(year):
                year_args['year'] = year

            data = []
            d = []
            for index, item in enumerate(finance_models.transactions.objects.filter(**args ,**year_args)):

                if item.month not in d:
                    d.append(item.month)
                    data.append(
                        {'month': item.month, 'name': get_month(int(item.month))}) and d.append(item.month)
            return JsonResponse({"message": data})

        if action == "TransactionChart":
            year = request.POST.get('year', None)
            month = request.POST.get('month', None)

            chart_args = {}

            if not validate_input(year):
                year = current_date.year

            print(year)

            # if validate_input(month):
            #     chart_args['month'] = month

            month_names = ["January", "February", "March", "April", "May", "June",
                   "July", "August", "September", "October", "November", "December"]
            days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
            
            if month == 'all':
                keys = {
                    item: round(finance_models.transactions.objects.filter(type='withdraw', sub_type='normal',
                        year=year, month=str(get_month(item))).aggregate(total=functions.Coalesce(Sum('amount'), 0.0))['total'], 2)
                    for index,item in enumerate(month_names)
                }
            else:
                keys = {
                    item: round(finance_models.transactions.objects.filter(type='withdraw', sub_type='normal',
                        year=year, month=month, transaction_date__day=item).aggregate(total=functions.Coalesce(Sum('amount'), 0.0))['total'], 2)
                    for index,item in enumerate(days)
                }

            # if month == 'all':
            #     for index, item in enumerate(finance_models.transactions.objects.filter(year=year).values('month').distinct()):
            #         keys[get_month(int(item['month']))] = round(finance_models.transactions.objects.filter(
            #             year=year, month=item['month'],).aggregate(total=functions.Coalesce(Sum('amount'), 0.0))['total'], 2)

            # else:
            #     for index, item in enumerate(finance_models.transactions.objects.filter(year=year, month=month).values('transaction_date__day').distinct()):
            #         keys[item['transaction_date__day']] = round(finance_models.transactions.objects.filter(
            #             year=year, month=month, transaction_date__day=item['transaction_date__day']).aggregate(total=functions.Coalesce(Sum('amount'), 0.0))['total'], 2)

            return JsonResponse({"message": keys})

        if action == "ExpenseChart":
            year = request.POST.get('year', None)
            month = request.POST.get('month', None)

            chart_args = {}

            if validate_input(year):
                chart_args['year'] = year

            if validate_input(month) and month != 'all':
                chart_args['month'] = month

            keys = finance_models.transactions.objects.filter(~Q(category=None),**args, **chart_args, type='withdraw').values(
                'category__name').annotate(total_amount=functions.Coalesce(Sum('amount'), 0.0, output_field=FloatField())).order_by('-total_amount')[:10]
            


            data = [
                {
                    'value': round(item['total_amount'],2),
                    'name': item['category__name']
                }

                for item in keys
            ]

            return JsonResponse({"message": data})

        if action == "IncomeChart":
            year = request.POST.get('year', None)
            month = request.POST.get('month', None)

            chart_args = {}

            if validate_input(year):
                chart_args['year'] = year

            if validate_input(month) and month != 'all':
                chart_args['month'] = month

            keys = finance_models.transactions.objects.filter(~Q(category=None),**args, **chart_args, type='deposit').values(
                'category__name').annotate(total_amount=functions.Coalesce(Sum('amount'), 0.0, output_field=FloatField())).order_by('-total_amount')[:5]


            data = [
                {
                    'value': round(item['total_amount'],2),
                    'name': item['category__name']
                }

                for item in keys
            ]

            return JsonResponse({"message": data})
