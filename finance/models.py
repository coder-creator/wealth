from django.db import models
from django.db.models import Sum, functions, FloatField, Exists, OuterRef, Q
from accounts.models import Users
from datetime import date
from django.apps import apps
from django.core import signing


class category(models.Model):
    name = models.CharField(max_length=200)
    type = models.CharField(max_length=200)
    user = models.ForeignKey(Users, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'category'


class accounts(models.Model):
    name = models.CharField(max_length=500)
    user = models.ForeignKey(Users, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'accounts'

    def encrypt_id(self):
        return signing.dumps(self.id)

    # getting the account's balance
    def get_account_balance(self, year=None, month=None) -> dict:
        """
        Calculates the account balance for a given year and month.

        Args:
            year (int, optional): The year for which the balance is calculated.
            month (int, optional): The month for which the balance is calculated.

        Returns:
            dict: A dictionary containing the balance, total deposits, and total withdrawals for the specified period.
        """

        args = {}

        if year is not None:
            args['year'] = year

        if month is not None:
            args['month'] = month

        normal_deposit = transactions.objects.filter(~Q(sub_type="transfer") , **args, type='deposit', account=self.id).aggregate(
            deposit=functions.Coalesce(Sum('amount'), 0, output_field=FloatField()))['deposit']

        normal_withdraw = transactions.objects.filter(~Q(sub_type="transfer"), **args, type='withdraw', account=self.id).aggregate(
            withdraw=functions.Coalesce(Sum('amount'), 0, output_field=FloatField()))['withdraw']


        account_deposit = transactions.objects.filter(~Q(account=self.id), **args, type='withdraw', sub_type="transfer", source_account=self.id).aggregate(
            deposit=functions.Coalesce(Sum('amount'), 0, output_field=FloatField()))['deposit']

        account_withdraw = transactions.objects.filter(~Q(source_account=self.id), **args, type='withdraw', sub_type="transfer", account=self.id).aggregate(
            withdraw=functions.Coalesce(Sum('amount'), 0, output_field=FloatField()))['withdraw']
        

        saving_deposit = transactions.objects.filter(~Q(account=self.id), **args, type='withdraw', sub_type="saving", source_account=self.id).aggregate(
            deposit=functions.Coalesce(Sum('amount'), 0, output_field=FloatField()))['deposit']

        saving_withdraw = transactions.objects.filter(~Q(source_account=self.id), **args, type='withdraw', sub_type="saving", account=self.id).aggregate(
            withdraw=functions.Coalesce(Sum('amount'), 0, output_field=FloatField()))['withdraw']

        balance = (normal_deposit + account_deposit + saving_deposit) - \
            (normal_withdraw + account_withdraw+saving_withdraw)

        return {
            'balance': round(balance, 2),
            'deposit': round(normal_deposit + account_deposit+saving_deposit, 2),
            'withdraw': round(normal_withdraw+account_withdraw+saving_withdraw, 2)
        }


class deptors(models.Model):
    name = models.CharField(max_length=500)
    phone = models.CharField(max_length=500 , blank=True, null=True)
    user = models.ForeignKey(Users, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'deptors'

    def get_phone(self):
        return "N/A" if self.phone in [None,''] else self.phone

    def encrypt_id(self):
        return signing.dumps(self.id)

    # create a static method that calculates the deptor's balance
    # this function will take one param called type ( return_dept , payback_dept )
    def deptor_balance(self):
        # We will use those shortcut letters
        # G -> gave
        # R -> Retun
        # T -> Take
        # P -> Payback
        G = transactions.objects.filter(user=self.user, deptors=self.id, type='withdraw', sub_type='give_dept').aggregate(
            G=functions.Coalesce(Sum('amount'), 0, output_field=FloatField()))['G']
        P = transactions.objects.filter(user=self.user, deptors=self.id, type='withdraw', sub_type='pay_dept').aggregate(
            P=functions.Coalesce(Sum('amount'), 0, output_field=FloatField()))['P']
        R = transactions.objects.filter(user=self.user, deptors=self.id, type='deposit', sub_type='return_dept').aggregate(
            R=functions.Coalesce(Sum('amount'), 0, output_field=FloatField()))['R']
        T = transactions.objects.filter(user=self.user, deptors=self.id, type='deposit', sub_type='take_dept').aggregate(
            T=functions.Coalesce(Sum('amount'), 0, output_field=FloatField()))['T']
        

        # now we calculate the deptor's balance
        balance = (R + T) - (G + P)
        icon = '<i class="feather-icon-sm text-info fw-bold" data-feather="check"></i>'

        if balance > 0:
            icon = '<i class="feather-icon-sm text-success fw-bold" data-feather="arrow-right"></i>'

        if balance < 0:
            icon = '<i class="feather-icon-sm text-danger fw-bold" data-feather="arrow-down-right"></i>'

        return {
            'G': float(G),
            'R': float(R),
            'T': float(T),
            'P': float(P),
            'balance': float(balance),
            'icon': icon
        }


class transactions(models.Model):
    amount = models.FloatField(default=0.0)
    balance = models.FloatField(default=0.0)
    type = models.CharField(max_length=10)
    sub_type = models.CharField(max_length=20)
    deptors = models.ForeignKey(
        deptors, on_delete=models.DO_NOTHING, null=True, blank=True)
    account = models.ForeignKey(
        accounts, on_delete=models.DO_NOTHING, null=True, blank=True, related_name="from_account")
    category = models.ForeignKey(
        category, on_delete=models.DO_NOTHING, null=True, blank=True)
    source_account = models.ForeignKey(
        accounts, on_delete=models.DO_NOTHING, null=True, blank=True, related_name="to_account")
    year = models.CharField(max_length=50, default=date.today().year)
    month = models.CharField(max_length=50, default=date.today().month)
    user = models.ForeignKey(Users, on_delete=models.DO_NOTHING)
    transaction_date = models.DateField(default=date.today())
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'transactions'
        ordering = ['id']

    def transaction_type(self):
        type = self.sub_type

        if self.sub_type not in ['saving', 'transfer']:
            type = 'income' if self.type == 'deposit' else 'expense'

        return type

    def transaction_source(self):
        type = self.sub_type

        if self.sub_type == 'normal':
            type = 'category'

        return type

    def encrypt_id(self):
        return signing.dumps(self.id)

    # getting sub_type in a formatted string
    def get_sub_type(self):
        return self.sub_type.replace('_', ' ').capitalize()
        

    # getting date with this format YYY-mmm-ddd
    def get_transaction_date(self):
        return self.transaction_date.strftime('%Y-%m-%d')

    # getting the amount with round two digits after .
    def get_amount(self):
        return round(self.amount, 2)

    def get_balance(self):
        return round(self.balance, 2)

    # get the source of the transaction
    def get_source(self):
        if self.source_account is not None:
            source = f"{self.source_account.name}"

            if self.sub_type == 'saving':
                source += f" ({self.category.name})"

            return source

        if self.category is not None:
            return self.category.name

        if self.deptors is not None:
            return self.deptors.name

        return ''

    @classmethod
    def summarize(cls, user, year):
        months_numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        result = []

        for month in months_numbers:
            # get the income of the current month
            deposit = cls.objects.filter(type='deposit', user=user, year=year, month=str(month)).aggregate(
                deposit=functions.Coalesce(Sum('amount'), 0, output_field=FloatField()))['deposit']

            # get the expense of the current month
            withdraw = transactions.objects.filter(type='withdraw', user=user, year=year, month=str(month)).order_by(
                'created_at').aggregate(withdraw=functions.Coalesce(Sum('amount'), 0, output_field=FloatField()))['withdraw']

            # subtract the income from expense
            # and append the result to the result variable
            balance = deposit - withdraw
            result.append(round(balance, 2))

        # after that append the sum of all months to the list
        result.append(sum(result))

        return result

    @classmethod
    def summarize2(cls, user, year, type):
        months_numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        result = []

        category = apps.get_model(app_label='finance', model_name='category')

        # get categories where captured in transactions only
        categories = category.objects.filter(
            Exists(transactions.objects.filter(
                ~Q(category=None), category=OuterRef('pk'))),
            type=type
        ).order_by('id')

        for index, category in enumerate(categories):
            temp = []
            for month in months_numbers:
                # get the income/expense of the current month by the current category
                income_expense = cls.objects.filter(category=category,  type='deposit' if type == 'income' else 'withdraw', user=user, year=year, month=str(month)).aggregate(
                    income_expense=functions.Coalesce(Sum('amount'), 0, output_field=FloatField()))['income_expense']

                temp.append(round(income_expense, 2))

            temp.append(sum(temp))
            result.append(temp)

        sum_of_all = list(map(lambda x: sum(x), zip(*result)))

        return {
            'categories': categories,
            'result': result,
            'sum_of_all': sum_of_all
        }

    @classmethod
    def get_account_transactions(cls, args, request , account=None):
        transactions = cls.objects.filter(args).reverse()

        result = []

        if 'search' in request.GET:
            search = request.GET['search']
            transactions = transactions.filter(Q(type__icontains=search) | Q(sub_type__icontains=search) | Q(account__name__icontains=search) | Q(user__username__icontains=search) | Q(
                user__first_name__icontains=search) | Q(user__last_name__icontains=search))

        balance = 0
        for index, item in enumerate(transactions):
            temp = balance
            balance = balance + item.amount if item.type == 'deposit' else balance - item.amount
            
            temp_type = item.type
            temp_source = item.get_source()

            if item.sub_type in ['transfer', 'saving']:
                temp_type = "deposit" if item.source_account.id == account.id else "withdraw"
                temp_source = f"{item.account.name}" if item.source_account.id == account.id else f"{item.source_account.name}"
            
            result.append({
                'id': item.id,
                'transaction_date': item.get_transaction_date(),
                'type': temp_type,
                'sub_type': item.get_sub_type(),
                'source': temp_source,
                'direction': "trending-up" if balance > temp else "trending-down",
                'color': "primary" if balance > temp else "danger",
                'amount': item.get_amount(),
                'encrypt_id': item.encrypt_id()
            })

        return result

    @classmethod
    def get_deptor_transactions(cls, args, request , account=None):
        transactions = cls.objects.filter(args).reverse()

        result = []

        if 'search' in request.GET:
            search = request.GET['search']
            transactions = transactions.filter(Q(type__icontains=search) | Q(sub_type__icontains=search) | Q(account__name__icontains=search) | Q(user__username__icontains=search) | Q(
                user__first_name__icontains=search) | Q(user__last_name__icontains=search))

        balance = 0
        for index, item in enumerate(transactions):
            temp = balance
            balance = balance + item.amount if item.type == 'deposit' else balance - item.amount
            
            temp_type = item.type
            temp_source = item.get_source()

            if item.sub_type in ['transfer', 'saving']:
                temp_type = "deposit" if item.source_account.id == account.id else "withdraw"
                temp_source = f"{item.account.name}" if item.source_account.id == account.id else f"{item.source_account.name}"
            
            result.append({
                'id': item.id,
                'transaction_date': item.get_transaction_date(),
                'type': temp_type,
                'sub_type': item.get_sub_type(),
                'source': temp_source,
                'direction': "trending-up" if balance > temp else "trending-down",
                'color': "primary" if balance > temp else "danger",
                'amount': item.get_amount(),
                'encrypt_id': item.encrypt_id()
            })

        return result


    @classmethod
    def generate_excel_data(cls, year,args):
        '''
        - getting the year
        - get all incomes and expense
        '''

        income_result = []
        expense_result = []

        months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

        income_categories = []
        expense_categories = []

        [
            income_categories.append(item.category.id)
            for index, item in enumerate(cls.objects.filter(
                year=year, type="deposit", sub_type="normal",**args))

            if item.category.id not in income_categories
        ]

        for x, categ in enumerate(income_categories):
            category__instance = category.objects.get(id=categ)
            temp = {
                'category': category__instance.name
            }

            for month in months:
                temp[get_month(month)] = transactions.objects.filter(year=year, month=month, type="deposit", sub_type="normal", category=category__instance.id,**args).aggregate(
                    total=functions.Round(
                        functions.Coalesce(Sum('amount'), 0.0), 2)
                )['total']

            temp['total'] = transactions.objects.filter(year=year, type="deposit", sub_type="normal", category=category__instance.id,**args).aggregate(
                total=functions.Round(functions.Coalesce(Sum('amount'), 0.0), 2))['total']

            income_result.append(temp)

        temp = {
            'category': "Total"
        }
        for month in months:
            temp[get_month(month)] = transactions.objects.filter(year=year,  month=month, type="deposit", sub_type="normal",**args).aggregate(
                total=functions.Round(functions.Coalesce(Sum('amount'), 0.0), 2))['total']

        temp['total'] = transactions.objects.filter(year=year, type="deposit", sub_type="normal",**args).aggregate(
            total=functions.Round(functions.Coalesce(Sum('amount'), 0.0), 2))['total']
        income_result.append(temp)

        [
            expense_categories.append(item.category.id)
            for index, item in enumerate(cls.objects.filter(
                year=year, type="withdraw", sub_type="normal",**args))

            if item.category.id not in expense_categories
        ]

        for x, categ in enumerate(expense_categories):
            category__instance = category.objects.get(id=categ)
            temp = {
                'category': category__instance.name
            }

            for month in months:
                temp[get_month(month)] = transactions.objects.filter(year=year, month=month, type="withdraw", sub_type="normal", category=category__instance.id,**args).aggregate(
                    total=functions.Round(
                        functions.Coalesce(Sum('amount'), 0.0), 2)
                )['total']

            temp['total'] = transactions.objects.filter(year=year, type="withdraw", sub_type="normal", category=category__instance.id,**args).aggregate(
                total=functions.Round(functions.Coalesce(Sum('amount'), 0.0), 2))['total']

            expense_result.append(temp)

        temp = {
            'category': "Total"
        }
        for month in months:
            temp[get_month(month)] = transactions.objects.filter(year=year,  month=month, type="withdraw", sub_type="normal",**args).aggregate(
                total=functions.Round(functions.Coalesce(Sum('amount'), 0.0), 2))['total']

        temp['total'] = transactions.objects.filter(year=year, type="withdraw", sub_type="normal",**args).aggregate(
            total=functions.Round(functions.Coalesce(Sum('amount'), 0.0), 2))['total']
        expense_result.append(temp)

        return {
            "incomes": income_result,
            "expenses": expense_result
        }


def get_month(value):
    months = {
        1: 'January',
        2: 'February',
        3: 'March',
        4: 'April',
        5: 'May',
        6: 'June',
        7: 'July',
        8: 'August',
        9: 'September',
        10: 'October',
        11: 'November',
        12: 'December'
    }

    if isinstance(value, int):
        return months[value]
    else:
        for month_num, month_name in months.items():
            if value.lower() in month_name.lower():
                return month_num
