# middleware.py

from django.shortcuts import redirect
from django.contrib import auth

class AutoLogoutMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated:
            current_session_timeout = request.session.get_expiry_age()

            # Check if the session timeout is less than 15 minutes
            if current_session_timeout < 900:
                # Logout the user
                auth.logout(request)
                return redirect('login_account')  # Replace 'logout_redirect_url' with the actual URL name or path

        response = self.get_response(request)
        return response