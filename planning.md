# WealthTrack Application

> App kaan waxaan ugu talo galay in aad ku maamulan karto kharashaatkaada maalinlaha ah , billaha ah iyo sanad laha, sidoo kale aad dagsan karo misaaniyad adiga kuu gaar ah kaas oo kaa caawin doonaa inaad markasta ogaato kharashyada aad u baahan tahay inaad bixiso iyo ku lagaa rabo inaad save agreesato.


## Muxuu ka koobnaan doonaa

1. Dakhliyada ( Incomes ).
2. Khraashaad ( Expenses ).
3. Keyd ( Saving ).
4. Miisaaniyad ( Budget ).
5. Warbixinno ( Reports ).
6. Users.
7. Payments.
8. Categories
9. Accounts -> Waa meesha lagu keyin doono lacagahaaga


#### Account Table
1. ID : Number
2. Name : String
3. Created Date : DateTime
4. Modified Date : DateTime
5. User : Foreign Key To Users Table

#### Category Table
1. ID : Number
2. Name : String
3. Created Date : DateTime
4. Modified Date : DateTime
5. Type : String (Income || Expenses || Saving )
6. color : String -> Icon Color
7. User : Foreign Key To Users Table

#### Loan Table
1. ID : Number
2. Name : String
3. Created Date : DateTime
4. Modified Date : DateTime
5. User : Foreign Key To Users Table

#### Transaction Table
1. ID : Number
2. User : Foreign Key To Users Table
3. Amount : Float
4. Type : String ( Deposit , Withdraw )
5. Account : Foreign Key To Account Table ( NULL True)
6. Loan : Foreign Key To Loan Table ( NULL True)
7. Category : Foreign Key To Category Table
8. Created Date : DateTime
9. Modified Date : DateTime

#### Payments Table
> In users table add (is_payment_valid) to track the user's payments
1. ID : Number
2. User : Foreign Key To Users Table
3. Start Date : DateTime
4. End Date : DateTime
5. Created Date : DateTime
6. Modified Date : DateTime


#### Dashboard
> These will be in **Boxes**
1. Your incomes
2. Your expenses
3. Your Saving
4. Last net or balance

> These will in **Charts**
1. Transaction
    - Daily
    - Monthly
    - Yearly